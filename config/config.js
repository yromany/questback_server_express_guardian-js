'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    glob = require('glob'),
    JiraApi = require('jira').JiraApi,
    ConfModel = require('../app/models/configuration.server.model.js'),
    Configuration = require('mongoose').model('Configuration');

var conf = {};
var privateConf = {};

function _setPrivatConf() {
    privateConf.jira = new JiraApi('https', conf.jiraURL, '443', conf.jiraGlobalUserName, conf.jiraGlobalUserPass, '2', false, false);
    privateConf.jiraCervus = new JiraApi('https', conf.jiraURL, '443', conf.jiraProjectUserName, conf.jiraProjectUserPass, '2', false, false);
    privateConf.jiraJQLURL = 'https://' + conf.jiraURL + '/issues/?jql='; // jql url
    privateConf.jiraGlobalUserPass = conf.jiraGlobalUserPass;
    privateConf.jenkins2password = conf.jenkins2password;
    privateConf.artifactoryPassword = conf.artifactoryPassword;
}

/**
 * Load app configurations
 */
module.exports = _.extend(
    require('./env/all'),
    require('./env/' + process.env.NODE_ENV) || {}
);

module.exports.initializeSystemConfig = function () {
    Configuration.findOne(function (err, obj) {
        if (obj === null) {
            conf = new Configuration();
            conf.save();
        }
        else {
            conf = obj;
        }
        _setPrivatConf();
    });
};

module.exports.conf = function () {
    var obj =  (JSON.parse(JSON.stringify(conf)));
    delete obj.jiraGlobalUserPass;
    delete obj.jiraProjectUserPass;
    delete obj.jenkins2password;
    delete obj.artifactoryPassword;
    return obj;
};

module.exports.setConf = function (newConf) {
    conf = newConf;
    _setPrivatConf();
};

module.exports.privateConf = privateConf;

/**
 * Get files by glob patterns
 */
module.exports.getGlobbedFiles = function (globPatterns, removeRoot) {
    // For context switching
    var _this = this;

    // URL paths regex
    var urlRegex = new RegExp('^(?:[a-z]+:)?\/\/', 'i');

    // The output array
    var output = [];

    // If glob pattern is array so we use each pattern in a recursive way, otherwise we use glob
    if (_.isArray(globPatterns)) {
        globPatterns.forEach(function (globPattern) {
            output = _.union(output, _this.getGlobbedFiles(globPattern, removeRoot));
        });
    } else if (_.isString(globPatterns)) {
        if (urlRegex.test(globPatterns)) {
            output.push(globPatterns);
        } else {
            glob(globPatterns, {
                sync: true
            }, function (err, files) {
                if (removeRoot) {
                    files = files.map(function (file) {
                        return file.replace(removeRoot, '');
                    });
                }

                output = _.union(output, files);
            });
        }
    }

    return output;
};

/**
 * Get the modules JavaScript files
 */
module.exports.getJavaScriptAssets = function (includeTests) {
    var output = this.getGlobbedFiles(this.assets.lib.js.concat(this.assets.js), 'public/');

    // To include tests
    if (includeTests) {
        output = _.union(output, this.getGlobbedFiles(this.assets.tests));
    }
    return output;
};

/**
 * Get the modules CSS files
 */
module.exports.getCSSAssets = function () {
    var output = this.getGlobbedFiles(this.assets.lib.css.concat(this.assets.css), 'public/');
    return output;
};
