'use strict';

var passport = require('passport'),
	User = require('mongoose').model('User'),
	path = require('path'),
	config = require('./config');

module.exports = function() {
  
	// Serialize sessions
	passport.serializeUser(function(user, done) {
		done(null, user.email);
	});

	// Deserialize sessions
	passport.deserializeUser(function(email, done) {
		User.findOne({
			email: email
		},function(err, user) {
			done(err, user);
		});
	});

	// Initialize strategies
	config.getGlobbedFiles('./config/strategies/**/*.js').forEach(function(strategy) {
		require(path.resolve(strategy))();
	});
};