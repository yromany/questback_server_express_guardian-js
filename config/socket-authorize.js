'use strict';

// export function for socket authorization
module.exports = function (socket, next, cookie, store) {
    var data = socket.request;

    if (!data.headers.cookie) {
        next(new Error('not authorized'));
    }

    cookie(data, {}, function (parseErr) {

        if (parseErr) {
            return next(new Error('Error parsing cookies.'));
        }

        var sidCookie = data.headers.cookie;

        if (typeof sidCookie === 'undefined')
            return next(new Error('Error parsing cookies.'));

        if (sidCookie.match(/connect.sid=s%3A(.+?)\./) === null)
            return next(new Error('Error parsing cookies.'));

        sidCookie = sidCookie.match(/connect.sid=s%3A(.+?)\./)[1];

        store.get(sidCookie, function (err, session) {
            if (err || !session || typeof session.passport.user === 'undefined') {
                next(new Error('Not logged in.'));
            } else {
                data.session = session;
                next();
            }
        });

    });

};
