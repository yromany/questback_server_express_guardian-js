'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport'),
    LdapStrategy = require('passport-ldapauth').Strategy,
    User = require('mongoose').model('User'),
    Build = require('mongoose').model('Build');

var OPTS = {
    server: {
        url: 'ldap://qbcgn-ad1.questback.cologne:389',
        adminDn: 'ldapabfrage@questback.cologne',
        searchBase: 'dc=questback,dc=cologne',
        adminPassword: 'wwldap123',
        searchFilter: '(sAMAccountName={{username}})',
        searchAttributes: ['uSNCreated', 'sAMAccountName', 'displayName', 'mail', 'givenName']
    }
};

function copyFile(source, target, cb) {
    var fs = require('fs');

    var cbCalled = false;

    function done(err) {
        if (!cbCalled) {
            cb(err);
            cbCalled = true;
        }
    }

    var rd = fs.createReadStream(source);
    rd.on('error', done);

    var wr = fs.createWriteStream(target);
    wr.on('error', done);
    wr.on('close', function (ex) {
        done();
    });
    rd.pipe(wr);
}

module.exports = function () {
    passport.use(new LdapStrategy(OPTS, function (user, done) {

        var name = user.displayName;
        var accountName = user.sAMAccountName;
        var email = user.mail.toLowerCase();
        var displayName = user.givenName;

        User.findOne({email: email}, function (err, dbUser) {
            if (null === dbUser) {
                var newUser = new User();
                newUser.name = name;
                newUser.userName = accountName;
                newUser.email = email;
                newUser.displayName = displayName;
                newUser.avatar = 'default.png';
                newUser.save(function (err, user) {
                    if (err) {
                        console.error(err);
                        return done(err);
                    } else {
                        copyFile(__dirname + '/../../public/modules/users/img/avatars/default.png', __dirname + '/../../public/modules/users/img/avatars/' + accountName + '.png', function (averr) {
                            if (averr) {
                                console.error('\x1b[31m', 'Error saving user avatar ' + averr);
                                return done(averr);
                            } else {
                                var build = new Build();
                                build.userName = accountName;
                                build.save(function (err, cb) {
                                    return done(null, user);
                                });
                            }
                        });
                    }
                });
            } else {
                return done(null, dbUser);
            }
        });
    }));
};
