'use strict';

module.exports = {
    db: process.env.DOCKER_MONGOURL || 'mongodb://localhost/guardian',
    assets: {
        lib: {
            css: [
                'public/lib/bootstrap/dist/css/bootstrap.min.css',
                'public/lib/components-font-awesome/css/fontawesome-all.min.css',
                'public/lib/ionicons/css/ionicons.min.css',
                'public/lib/jquery-ui/themes/base/sortable.css',
                'public/lib/bootstrap-daterangepicker/daterangepicker-bs3.css',
                'public/lib/ionrangeslider/css/ion.rangeSlider.css',
                'public/lib/ionrangeslider/css/ion.rangeSlider.skinNice.css',
                'public/lib/angular-xeditable/dist/css/xeditable.css',
                'public/lib/angular-ui-select/dist/select.min.css',
                'public/lib/angular-growl-v2/build/angular-growl.min.css'
            ],
            js: [
                'public/lib/jquery/dist/jquery.min.js',
                'public/lib/jquery-ui/jquery-ui.min.js',
                'public/lib/angular/angular.min.js',
                'public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js',
                'public/lib/angular-resource/angular-resource.min.js',
                'public/lib/angular-sanitize/angular-sanitize.min.js',
                'public/lib/angular-ui-sortable/sortable.min.js',
                'public/lib/angular-ui-router/release/angular-ui-router.min.js',
                'public/js/angular-moment.min.js', // we need to change this to the hack version
                'public/lib/angular-xeditable/dist/js/xeditable.min.js',
                'public/lib/socket.io-client/socket.io.js',
                'public/lib/angular-socket-io/socket.min.js',
                'public/lib/slimScroll/jquery.slimscroll.min.js',
                'public/lib/moment/min/moment.min.js',
                'public/lib/bootstrap-daterangepicker/daterangepicker.js',
                'public/lib/ionrangeslider/js/ion.rangeSlider.min.js',
                'public/lib/highcharts/highcharts.js',
                'public/lib/highcharts-ng/dist/highcharts-ng.min.js',
                'public/lib/ace-builds/src-min/ace.js',
                'public/lib/ace-builds/src-min/mode-gherkin.js',
                'public/lib/angular-ui-select/dist/select.min.js',
                'public/lib/angular-bootstrap-multiselect/dist/angular-bootstrap-multiselect.min.js',
                'public/lib/angular-growl-v2/build/angular-growl.min.js'
            ]
        },
        css: 'public/dist/application.min.css',
        js: 'public/dist/application.min.js'
    },
    mailer: {
        from: process.env.MAILER_FROM || 'guardian.js@questback.com',
        options: {
            service: process.env.MAILER_SERVICE_PROVIDER || 'Gmail',
            auth: {
                user: process.env.MAILER_EMAIL_ID || 'questback.qa@gmail.com',
                pass: process.env.MAILER_PASSWORD || 'questback!'
            }
        }
    }
};
