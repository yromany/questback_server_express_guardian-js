'use strict';

module.exports = {
    app: {
        title: 'Guardian.JS | the QA dashboard',
        description: 'The QA dashboard',
        keywords: 'questback, quality, assurance'
    },
    port: process.env.PORT || 80,
    templateEngine: 'swig',
    sessionSecret: 'QAINDAHOUSE',
    sessionCollection: 'sessions',
    assets: {
        lib: {
            css: [
                'public/lib/bootstrap/dist/css/bootstrap.css',
                'public/lib/components-font-awesome/css/fontawesome-all.css',
                'public/lib/ionicons/css/ionicons.css',
                'public/lib/jquery-ui/themes/base/sortable.css',
                'public/lib/bootstrap-daterangepicker/daterangepicker-bs3.css',
                'public/lib/ionrangeslider/css/ion.rangeSlider.css',
                'public/lib/ionrangeslider/css/ion.rangeSlider.skinNice.css',
                'public/lib/angular-xeditable/dist/css/xeditable.css',
                'public/lib/angular-ui-select/dist/select.css',
                'public/lib/angular-growl-v2/build/angular-growl.css'
            ],
            js: [
                'public/lib/jquery/dist/jquery.js',
                'public/lib/jquery-ui/jquery-ui.js',
                'public/lib/angular/angular.js',
                'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
                'public/lib/angular-resource/angular-resource.js',
                'public/lib/angular-sanitize/angular-sanitize.js',
                'public/lib/angular-ui-sortable/sortable.js',
                'public/lib/angular-ui-router/release/angular-ui-router.js',
                'public/js/angular-moment.js', // we need to change this to the hack version
                'public/lib/angular-xeditable/dist/js/xeditable.js',
                'public/lib/socket.io-client/socket.io.js',
                'public/lib/angular-socket-io/socket.js',
                'public/lib/slimScroll/jquery.slimscroll.js',
                'public/lib/moment/moment.js',
                'public/lib/bootstrap-daterangepicker/daterangepicker.js',
                'public/lib/ionrangeslider/js/ion.rangeSlider.js',
                'public/lib/highcharts/highcharts.js',
                'public/lib/highcharts-ng/dist/highcharts-ng.js',
                'public/lib/ace-builds/src/ace.js',
                'public/lib/ace-builds/src/mode-gherkin.js',
                'public/lib/angular-ui-select/dist/select.js',
                'public/lib/angular-bootstrap-multiselect/dist/angular-bootstrap-multiselect.js',
                'public/lib/angular-growl-v2/build/angular-growl.js',
                '//www.google.com/jsapi'
            ]
        },
        css: [
            'public/modules/**/css/*.css'
        ],
        js: [
            'public/config.js',
            'public/application.js',
            'public/modules/*/*.js',
            'public/modules/*/*[!tests]*/*.js',
            'public/js/app.js'
        ],
        tests: [
            'public/lib/angular-mocks/angular-mocks.js',
            'public/modules/*/tests/*.js'
        ]
    }
};
