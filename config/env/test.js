'use strict';

module.exports = {
	db: 'mongodb://localhost/guardian-test',
	port: 3001,
	app: {
		title: 'Guardian.JS - Test Environment'
	},
  mailer: {
    from: process.env.MAILER_FROM || 'guardian.js@questback.com',
    options: {
      service: process.env.MAILER_SERVICE_PROVIDER || 'Gmail',
      auth: {
        user: process.env.MAILER_EMAIL_ID || 'questback.qa@gmail.com',
        pass: process.env.MAILER_PASSWORD || 'questback!'
      }
    }
  }
};