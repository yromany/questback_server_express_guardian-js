'use strict';
// Init the application configuration module for AngularJS application
var ApplicationConfiguration = function () {
    // Init module configuration options
    var applicationModuleName = 'guardianjs';
    var applicationModuleVendorDependencies = [
        'ngResource',
        'ui.router',
        'ui.select',
        'xeditable',
        'btford.socket-io',
        'ui.bootstrap',
        'ngSanitize',
        'angularMoment',
        'ui.sortable',
        'highcharts-ng',
        'btorfs.multiselect',
        'angular-growl'
      ];
    // Add a new vertical module
    var registerModule = function (moduleName, dependencies) {
      // Create angular module
      angular.module(moduleName, dependencies || []);
      // Add the module to the AngularJS configuration file
      angular.module(applicationModuleName).requires.push(moduleName);
    };
    return {
      applicationModuleName: applicationModuleName,
      applicationModuleVendorDependencies: applicationModuleVendorDependencies,
      registerModule: registerModule
    };
  }();'use strict';
//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);
angular.module(ApplicationConfiguration.applicationModuleName).config([
  '$locationProvider',
  'growlProvider',
  function ($locationProvider, growlProvider) {
    // Setting HTML5 Location Mode
    $locationProvider.hashPrefix('!');
    // Set default timeout for Growl
    growlProvider.globalTimeToLive(60000);
  }
]).run(function (editableOptions) {
  editableOptions.theme = 'bs3';
});
//Then define the init function for starting up the application
angular.element(document).ready(function () {
  //Fixing facebook bug with redirect
  if (window.location.hash === '#_=_')
    window.location.hash = '#!';
  //Then init the app
  angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('changelog');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('chat');'use strict';
// Use application configuration module to register a new module
ApplicationConfiguration.registerModule('configuration');'use strict';
// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('coverage');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('deployment');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('feature');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('jenkins');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('jira');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('university');'use strict';
// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('users');'use strict';
angular.module('changelog').controller('ChangelogController', [
  '$scope',
  '$http',
  function ($scope, $http) {
    $scope.product = {};
    $scope.versions = [];
    $scope.changelogs = [];
    $scope.getVersions = function () {
      $scope.changelogs = [];
      $http.get('artifacts/versions?product=' + $scope.product).success(function (data) {
        $scope.versions = data;
      });
    };
    $scope.getChangelog = function (version) {
      $http.get('changelogs/' + $scope.product + '/' + version).success(function (data) {
        $scope.changelogs = data;
      });
    };
  }
]);'use strict';
angular.module('chat').controller('ChatController', [
  '$scope',
  'Authentication',
  'socket',
  '$http',
  function ($scope, Authentication, socket, $http) {
    $scope.account = Authentication.account;
    $scope.chatStarted = false;
    $scope.configLimit = false;
    $scope.messages = [];
    $scope.chatLimit = $scope.account.chat.chatLimit;
    $scope.chatFrom = $scope.account.chat.chatFrom;
    // initialize chat
    var initChat = function () {
      $scope.message = '';
      $http.get('chat/messages').success(function (data) {
        $scope.messages = data;
        $scope.chatStarted = true;
      });
    };
    $scope.setLimit = function (limit) {
      $scope.chatStarted = false;
      $scope.chatLimit = limit;
      var data = { 'limit': $scope.chatLimit };
      $http.put('settings/chat/limit', data).success(function (data) {
        initChat();
        $scope.configLimit = false;
      });
    };
    $scope.setFrom = function (from) {
      $scope.chatStarted = false;
      $scope.chatFrom = from;
      var data = { 'from': $scope.chatFrom };
      $http.put('settings/chat/from', data).success(function (data) {
        initChat();
      });
    };
    $scope.init = function () {
      initChat();
    };
    $scope.init();
    socket.on('got:message', function (data) {
      $scope.messages.unshift({
        user: data.user,
        message: data.message,
        when: data.when
      });
    });
    $scope.sendMessage = function () {
      if ($scope.message.trim() !== '') {
        if ($scope.message === '/limit') {
          $scope.configLimit = true;
        } else {
          socket.emit('send:message', {
            user: $scope.account,
            message: $scope.message
          });
        }
        $scope.lastMessage = $scope.message;
        $scope.message = '';
      }
    };
  }
]);(function () {
  'use strict';
  var main = 'smile', smilies = [
      'biggrin',
      'confused',
      'cool',
      'cry',
      'eek',
      'evil',
      'like',
      'lol',
      'love',
      'mad',
      'mrgreen',
      'neutral',
      'question',
      'razz',
      'redface',
      'rolleyes',
      'sad',
      'smile',
      'surprised',
      'thumbdown',
      'thumbup',
      'twisted',
      'wink'
    ], shorts = {
      ':D': 'biggrin',
      ':-D': 'biggrin',
      ':S': 'confused',
      ':-S': 'confused',
      ';(': 'cry',
      ';-(': 'cry',
      'OO': 'eek',
      '<3': 'like',
      '&lt;3': 'like',
      '^^': 'lol',
      ':|': 'neutral',
      ':-|': 'neutral',
      ':P': 'razz',
      ':-P': 'razz',
      ':(': 'sad',
      ':-(': 'sad',
      ':)': 'smile',
      ':-)': 'smile',
      ':O': 'surprised',
      ':-O': 'surprised',
      ';)': 'wink',
      ';-)': 'wink'
    }, regex = new RegExp(':(' + smilies.join('|') + '):', 'g'), template = '<i class="smiley-$1" title="$1"></i>', escapeRegExp = function (str) {
      return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
    }, regExpForShort = function (str) {
      if (/^[a-z]+$/i.test(str)) {
        // use word boundaries if emoji is letters only
        return new RegExp('\\b' + str + '\\b', 'gi');
      } else {
        return new RegExp(escapeRegExp(str), 'gi');
      }
    }, templateForSmiley = function (str) {
      return template.replace('$1', str);
    }, apply = function (input) {
      if (!input)
        return '';
      var output = input.replace(regex, template);
      for (var sm in shorts) {
        if (shorts.hasOwnProperty(sm)) {
          output = output.replace(regExpForShort(sm), templateForSmiley(shorts[sm]));
        }
      }
      return output;
    };
  angular.module('chat').filter('smilies', function () {
    return apply;
  }).directive('smilies', function () {
    return {
      restrict: 'A',
      scope: { source: '=smilies' },
      link: function ($scope, el) {
        el.html(apply($scope.source));
      }
    };
  }).directive('smiliesSelector', [
    '$timeout',
    function ($timeout) {
      var templateUrl;
      try {
        angular.module('ui.bootstrap.popover');
        templateUrl = 'template/smilies/button-a.html';
      } catch (e) {
        console.error('No Popover module found');
        return {};
      }
      return {
        restrict: 'A',
        templateUrl: templateUrl,
        scope: {
          source: '=smiliesSelector',
          placement: '@smiliesPlacement',
          title: '@smiliesTitle'
        },
        link: function ($scope, el) {
          $scope.smilies = smilies;
          $scope.append = function (smiley) {
            if ($scope.source === undefined) {
              $scope.source = '';
            }
            $scope.source += ' :' + smiley + ': ';
            $timeout(function () {
              el.children('i').click();  // close the popover
            });
          };
        }
      };
    }
  ]).directive('focusOnChange', function () {
    return {
      restrict: 'A',
      link: function ($scope, el, attrs) {
        $scope.$watch(attrs.focusOnChange, function () {
          el[0].focus();
        });
      }
    };
  }).directive('dateRangePicker', function () {
    return {
      link: function (scope, element, attrs, form) {
        element.daterangepicker({
          opens: 'right',
          ranges: {
            'Today': [
              window.moment(),
              window.moment()
            ],
            'Last Day': [
              window.moment().subtract(1, 'days'),
              window.moment()
            ],
            'Last 7 Days': [
              window.moment().subtract(6, 'days'),
              window.moment()
            ],
            'Last 30 Days': [
              window.moment().subtract(29, 'days'),
              window.moment()
            ],
            'Last Quarter': [
              window.moment().subtract(3, 'month').startOf('month'),
              window.moment()
            ],
            'Last Year': [
              window.moment().subtract(1, 'year').startOf('month'),
              window.moment()
            ]
          },
          startDate: window.moment().subtract(6, 'days'),
          endDate: window.moment()
        }, function (start, end) {
          scope.setFrom(start);
          angular.element('#chatrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        });
      }
    };
  }).directive('ngEnter', function () {
    return function (scope, element, attrs) {
      element.bind('keydown keypress', function (event) {
        if (event.which === 13) {
          scope.$apply(function () {
            scope.$eval(attrs.ngEnter);
          });
          event.preventDefault();
        }
      });
    };
  }).directive('chatLimitRange', function () {
    return {
      link: function (scope, element, attrs, form) {
        element.ionRangeSlider({
          from: scope.chatLimit,
          onFinish: function (obj) {
            scope.setLimit(obj.fromNumber);
          }
        });
      }
    };
  }).directive('chatRange', function () {
    return {
      link: function (scope, element, attrs, form) {
        element.html(window.moment(scope.chatFrom).format('MMMM D, YYYY') + ' - ' + window.moment().format('MMMM D, YYYY'));
      }
    };
  }).run([
    '$templateCache',
    function ($templateCache) {
      // use ng-init because popover-template only accept a variable
      $templateCache.put('template/smilies/button-a.html', '<i class="smiley-' + main + ' smilies-selector" ' + 'ng-init="smiliesTemplate = \'template/smilies/popover-a.html\'" ' + 'popover-template="smiliesTemplate" ' + 'popover-placement="{{!placement && \'left\' || placement}}" ' + 'popover-title="{{title}}"</i>');
      $templateCache.put('template/smilies/popover-a.html', '<div ng-model="smilies" class="smilies-selector-content">' + '<i class="smiley-{{smiley}}" ng-repeat="smiley in smilies" ng-click="append(smiley)"></i>' + '</div>');
      $templateCache.put('template/smilies/popover-b.html', '<div class="popover" tabindex="-1">' + '<div class="arrow"></div>' + '<h3 class="popover-title" ng-bind-html="title" ng-show="title"></h3>' + '<div class="popover-content">' + $templateCache.get('template/smilies/popover-a.html') + '</div>' + '</div>');
    }
  ]);
}());'use strict';
angular.module('configuration').controller('ConfigurationController', [
  '$scope',
  '$http',
  'System',
  '$location',
  'Authentication',
  function ($scope, $http, System, $location, Authentication) {
    $scope.account = Authentication.account;
    var helper = JSON.parse(JSON.stringify(System.conf()));
    delete helper._id;
    delete helper.__v;
    helper.jiraProjectUserPass = '*****';
    helper.jiraGlobalUserPass = '*****';
    helper.jenkins2password = '*****';
    helper.artifactoryPassword = '*****';
    $scope.conf = helper;
    $scope.btnStatus = 'disabled';
    $scope.btnStatusLabel = 'Update feature definition';
    $scope.btnCoverageStatus = 'disabled';
    $scope.statusResult = 'Hey ho! Please check the version status first.';
    $scope.coverageStatusResult = 'Hey ho! Please select the coverage file.';
    $scope.coverage = '';
    $scope.maintenance = { message: '' };
    //########### COVERAGE ##############
    // handle on load coverage file
    $scope.handler = function (e, files) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $scope.coverage = reader.result;
        $scope.btnCoverageStatus = '';
        $scope.coverageStatusResult = 'Ok. Lets update the coverage.';
        $scope.$apply();
      };
      reader.readAsText(files[0]);
    };
    // update coverage collection
    $scope.updateCoverage = function () {
      $scope.coverageStatusResult = 'Updating coverage...';
      return $http.post('/configuration/coverage/update', { coverage: $scope.coverage }).success(function (data) {
        $scope.coverageStatusResult = 'Yep. Everything should be up to date now.';
      });
    };
    //########### FEATURES ##############
    // check for version status
    $scope.checkFeatureStatus = function () {
      $scope.statusResult = 'Checking status...';
      $scope.btnStatus = 'disabled';
      return $http.get('/configuration/features/status').success(function (data) {
        $scope.statusResult = data;
        if (data.indexOf('no changes found') === -1) {
          $scope.btnStatus = '';
          if (data.indexOf('check out') > 0) {
            $scope.btnStatusLabel = 'Checkout webdriver repository';
          }
        } else {
          $scope.btnStatusLabel = 'Update feature definition';
        }
      });
    };
    // update features
    $scope.updateFeatures = function () {
      $scope.statusResult = 'Updating features...';
      $scope.btnStatus = 'disabled';
      return $http.post('/configuration/features/update').success(function (data) {
        $scope.statusResult = 'Horray! Feature files are up to date.';
        $scope.btnStatusLabel = 'Update feature definition';
      });
    };
    //########### GLOBAL CONFIG ##############
    // update a single gloabal variable
    $scope.updateConfig = function (item, value) {
      var data = {};
      data[item] = value;
      return $http.post('/configuration/system/update', {
        conf: data,
        id: System.conf()._id
      });
    };
    //########### DEPLOYMENT HOSTS ##############
    // get the list of deployment hosts
    var getHosts = function () {
      $http.get('/configuration/hosts').success(function (data) {
        $scope.hosts = data;
      });
    };
    // create a new deployment host
    $scope.newDeployHost = function () {
      $http.post('/configuration/hosts').success(function (data) {
        getHosts();
      });
    };
    // update a deployment host
    $scope.updateHost = function (host) {
      $http.put('/configuration/hosts/' + host._id, host).success(function (data) {
      });
    };
    // delete a deployment host
    $scope.deleteHost = function (host) {
      $http.delete('/configuration/hosts/' + host._id).success(function (data) {
        getHosts();
      });
    };
    //########### USER MANAGEMENT ##############
    // get the list of deployment hosts
    var getUsers = function () {
      $http.get('/configuration/users').success(function (data) {
        $scope.users = data;
      });
    };
    // update a user
    $scope.updateUser = function (user) {
      $http.put('/configuration/users/' + user._id, user).success(function (data) {
      });
    };
    // delete a user
    $scope.deleteUser = function (user) {
      $http.delete('/configuration/users/' + user._id).success(function (data) {
        getUsers();
      });
    };
    // send maintenance message
    $scope.sendMaintenanceMessage = function () {
      var data = { message: $scope.maintenance.message };
      if (data.message !== '') {
        $http.post('/configuration/maintenance', data).success(function (data) {
        });
      } else
        return 'Maintenance message with no message? :-/';
    };
  }
]);'use strict';
angular.module('configuration').controller('MaintenanceController', [
  '$scope',
  'message',
  '$modalInstance',
  '$http',
  function ($scope, message, $modalInstance, $http) {
    $scope.message = message;
    $scope.ok = function () {
      $http.put('/settings/announcement/read').success(function (data) {
        $modalInstance.close();
      });
    };
  }
]);'use strict';
angular.module('configuration').directive('fileChange', [
  '$parse',
  function ($parse) {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function ($scope, element, attrs, ngModel) {
        var attrHandler = $parse(attrs.fileChange);
        var handler = function (e) {
          $scope.$apply(function () {
            attrHandler($scope, {
              $event: e,
              files: e.target.files
            });
          });
        };
        element[0].addEventListener('change', handler, false);
      }
    };
  }
]);'use strict';
// Authentication service for user variables
angular.module('configuration').service('System', function () {
  var conf = window.conf;
  this.conf = function () {
    return conf;
  };
  this.setConf = function (newConfig) {
    conf = newConfig;
  };
});'use strict';
// Setting up route
angular.module('core').config([
  '$stateProvider',
  '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {
    // Redirect to home view when route not found
    $urlRouterProvider.otherwise('/home');
    // Home state routing
    $stateProvider.state('index', {
      views: {
        'header': { templateUrl: 'modules/core/views/header.client.view.html' },
        'wrapper': { templateUrl: 'modules/core/views/wrapper.client.view.html' }
      }
    }).state('index.home', {
      url: '/home',
      views: {
        'content': { templateUrl: 'modules/core/views/home.client.view.html' },
        'chat@content': {
          templateUrl: 'modules/chat/views/chat.client.view.html',
          controller: 'ChatController'
        },
        'todo@content': {
          templateUrl: 'modules/jira/views/todo.client.view.html',
          controller: 'TodoController'
        },
        'stream@content': {
          templateUrl: 'modules/jira/views/stream.client.view.html',
          controller: 'StreamController'
        },
        'webdriver@content': {
          templateUrl: 'modules/jenkins/views/webdriver.client.view.html',
          controller: 'TestReportController'
        },
        'build@content': {
          templateUrl: 'modules/jenkins/views/build.client.view.html',
          controller: 'BuildController'
        }
      }
    }).state('index.features', {
      resolve: {
        features: function ($http) {
          return $http.get('/features');
        }
      },
      url: '/features',
      views: {
        'content': {
          templateUrl: 'modules/feature/views/feature.client.view.html',
          controller: function ($scope, features, Editor) {
            Editor.init();
            $scope.filteredSearch = [features.data];
            $scope.features = [features.data];
            $scope.featureLoaded = true;
            $scope.tree_loading = false;
          }
        }
      }
    }).state('index.university', {
      url: '/university',
      views: { 'content': { templateUrl: 'modules/university/views/university.client.view.html' } }
    }).state('index.jira', {
      url: '/jira',
      views: { 'content': { templateUrl: 'modules/jira/views/boxes.client.view.html' } }
    }).state('index.config', {
      resolve: {
        hosts: function ($http) {
          return $http.get('/configuration/hosts');
        },
        users: function ($http) {
          return $http.get('/configuration/users');
        }
      },
      url: '/config',
      views: {
        'content': {
          templateUrl: 'modules/configuration/views/configuration.client.view.html',
          controller: function ($scope, hosts, users) {
            $scope.hosts = hosts.data;
            $scope.users = users.data;
          }
        }
      }
    }).state('index.artifact', {
      url: '/artifact',
      views: { 'content': { templateUrl: 'modules/deployment/views/artifacts.client.view.html' } }
    }).state('index.deployment', {
      resolve: {
        hosts: function ($http) {
          return $http.get('/configuration/hosts');
        }
      },
      url: '/deployment',
      views: {
        'content': {
          templateUrl: 'modules/deployment/views/deployment.client.view.html',
          controller: function ($scope, hosts) {
            $scope.hosts = hosts.data;
          }
        }
      }
    }).state('index.changelog', {
      url: '/changelog',
      views: { 'content': { templateUrl: 'modules/changelog/views/changelog.client.view.html' } }
    }).state('index.coverage', {
      resolve: {
        coverage: function ($http) {
          return $http.get('/coverage');
        }
      },
      url: '/coverage',
      views: {
        'content': {
          templateUrl: 'modules/coverage/views/coverage.client.view.html',
          controller: function ($scope, coverage) {
            $scope.coverage = coverage.data.coverage;
          }
        }
      }
    });
  }
]);'use strict';
angular.module('core').controller('HeaderController', [
  '$scope',
  'Authentication',
  '$http',
  'System',
  function ($scope, Authentication, $http, System) {
    $scope.conf = System.conf();
    $scope.account = Authentication.account;
    var initJiras = function () {
      $http.get('jira/tasks').success(function (data) {
        $scope.tasks = data;
      });
    };
    $scope.init = function () {
      initJiras();
    };
    $scope.init();
  }
]);'use strict';
angular.module('core').controller('HomeController', [
  '$scope',
  'Authentication',
  '$splash',
  '$http',
  function ($scope, Authentication, $splash, $http) {
    $scope.account = Authentication.account;
    $scope.widgets = [];
    $scope.widgetSections = $scope.account.widgetSections;
    $scope.layoutConfig = false;
    // user widgets positioning
    $scope.widgets = $scope.account.widgets;
    // widgets positioning is a user specific setting
    $scope.init = function () {
      if ($scope.widgetSections > 2) {
        $scope.widgetsClasses = [
          'col-lg-6',
          'col-lg-3',
          'col-lg-3'
        ];
        $scope.widgetsLayout = 'layout-aaa';
      } else {
        $scope.widgetsClasses = [
          'col-lg-7',
          'col-lg-5'
        ];
        $scope.widgetsLayout = 'layout-ab';
      }
    };
    $scope.init();
    // widgets options
    $scope.widgetOptions = {
      placeholder: 'sort-highlight',
      connectWith: '.connectedSortable',
      forcePlaceholderSize: true,
      zIndex: 999999,
      update: function (e, ui) {
        $http.put('settings/widgets', $scope.widgets).success(function (data) {
        });
      }
    };
    // first login handler
    if ($scope.account.firstLogin === true) {
      $splash.open({}, {
        templateUrl: 'modules/users/views/settings/first-login.client.view.html',
        windowTemplateUrl: 'splash/index.html'
      });
    }
    $scope.layout = function () {
      $scope.layoutConfig = true;
    };
    $scope.changeLayout = function (layout) {
      if (layout !== $scope.widgetsLayout) {
        if (layout === 'layout-ab') {
          angular.forEach($scope.widgets[2], function (ob) {
            $scope.widgets[1].unshift(ob);
          });
          $scope.widgets[2] = [];
          $scope.widgetSections = 2;
        } else {
          $scope.widgetSections = 3;
        }
        var widgetSections = { 'sections': $scope.widgetSections };
        $http.put('settings/widgets/sections', widgetSections).success(function (data) {
          $http.put('settings/widgets', $scope.widgets).success(function (data) {
            window.location.reload();
          });
        });
      }
      $scope.layoutConfig = false;
    };
  }
]);'use strict';
angular.module('core').controller('SideBarController', [
  '$scope',
  '$location',
  'socket',
  'Authentication',
  'System',
  '$splash',
  'growl',
  '$modal',
  '$http',
  function ($scope, $location, socket, Authentication, System, $splash, growl, $modal, $http) {
    $scope.account = Authentication.account;
    $scope.maintenanceTemplate = 'template/configuration/maintenance.html';
    $scope.avatarLastState = window.moment().unix();
    socket.on('user:init', function (data) {
      $scope.user = data.user;
      $scope.users = data.users;
    });
    socket.on('user:refresh', function (data) {
      $scope.users = data.users;
    });
    socket.on('user:left', function (data) {
      $scope.users = data.users;
    });
    socket.on('config:refresh', function (data) {
      System.setConf(data.conf);
    });
    $scope.toggleStatus = function (user) {
      socket.emit('user:togglestatus', { user: user });
    };
    socket.on('user:update', function (data) {
      $scope.user = data.user;
      $scope.users = data.users;
    });
    socket.on('user:avatarUpdate', function () {
      $scope.avatarLastState = window.moment().unix();
    });
    $scope.changeAvatar = function () {
      $splash.open({}, { templateUrl: 'modules/users/views/settings/first-login.client.view.html' });
    };
    socket.on('artifact:refresh', function (data) {
      growl.info('Artifact <strong>' + data.name + '</strong> has been checked in.', {});
    });
    var openMaintenanceModal = function (message) {
      $modal.open({
        templateUrl: $scope.maintenanceTemplate,
        controller: 'MaintenanceController',
        size: 'lg',
        resolve: {
          message: function () {
            return message;
          }
        }
      });
    };
    if (!$scope.account.announcementRead) {
      $http.get('/maintenance/message').success(function (data) {
        openMaintenanceModal(data.message);
      });
    }
    socket.on('maintenance:new', function (data) {
      openMaintenanceModal(data.message);
    });
    socket.on('jenkins:failed', function (data) {
      growl.error('Job <strong> <a href="' + data.url + '" target="_blank">' + data.job + '</strong></a> has failed.', { ttl: -1 });
    });
    socket.on('jenkins:fixed', function (data) {
      growl.success('Job <strong> <a href="' + data.url + ' " target="_blank">' + data.job + '</strong></a> is fixed.', { ttl: -1 });
    });
  }
]).run([
  '$templateCache',
  function ($templateCache) {
    // template for maintenance message
    $templateCache.put('template/configuration/maintenance.html', '<div class="modal-header"><h3 class="modal-title">Maintenance Announcement</h3></div>' + '<div class="modal-body">' + '<pre>{{ message }}</pre>' + '</div>' + '<div class="modal-footer">' + '<button class="btn btn-primary" data-ng-click="ok()">Ok, understood!</button>' + '</div>' + '</div>');
  }
]);'use strict';
angular.module('core').directive('sidebarToggle', [
  '$http',
  function ($http) {
    return {
      link: function (scope, element, attrs) {
        // Enable sidebar toggle
        element.click(function (e) {
          e.preventDefault();
          // If window is small enough, enable sidebar push menu
          if (angular.element(window).width() <= 992) {
            angular.element('.row-offcanvas').toggleClass('active');
            angular.element('.left-side').removeClass('collapse-left');
            angular.element('.right-side').removeClass('strech');
            angular.element('.row-offcanvas').toggleClass('relative');
          } else {
            angular.element('.left-side').toggleClass('collapse-left');
            angular.element('.right-side').toggleClass('strech');
          }
        });
      }
    };
  }
]).directive('slimScroll', function () {
  return {
    link: function (scope, element, attrs) {
      element.slimscroll({
        height: attrs.h,
        alwaysVisible: false,
        size: attrs.s
      }).css('width', '100%');
    }
  };
}).directive('sidebarTree', function () {
  return {
    link: function (scope, element, attrs) {
      var btn = element.children('a').first();
      var menu = element.children('.treeview-menu').first();
      var isActive = element.hasClass('active');
      // initialize already active menus
      if (isActive) {
        menu.show();
        btn.children('.fa-angle-left').first().removeClass('fa-angle-left').addClass('fa-angle-down');
      }
      // Slide open or close the menu on link click
      btn.click(function (e) {
        e.preventDefault();
        if (isActive) {
          // Slide up to close menu
          menu.slideUp('fast');
          isActive = false;
          btn.children('.fa-angle-down').first().removeClass('fa-angle-down').addClass('fa-angle-left');
          btn.parent('li').removeClass('active');
        } else {
          // Slide down to open menu
          menu.slideDown('fast');
          isActive = true;
          btn.children('.fa-angle-left').first().removeClass('fa-angle-left').addClass('fa-angle-down');
          btn.parent('li').addClass('active');
        }
      });
      /* Add margins to submenu elements to give it a tree look */
      menu.find('li > a').each(function () {
        var pad = parseInt(window.jQuery(this).css('margin-left')) + 10;
        window.jQuery(this).css({ 'margin-left': pad + 'px' });
      });
    }
  };
});'use strict';
angular.module('core').factory('socket', [
  'socketFactory',
  function (socketFactory) {
    return socketFactory();
  }
]);'use strict';
angular.module('core').service('$splash', [
  '$modal',
  '$rootScope',
  function ($modal, $rootScope) {
    return {
      open: function (attrs, opts) {
        var scope = $rootScope.$new();
        angular.extend(scope, attrs);
        opts = angular.extend(opts || {}, {
          backdrop: false,
          scope: scope,
          windowTemplateUrl: 'modules/core/views/splash.client.view.html'
        });
        return $modal.open(opts);
      }
    };
  }
]);'use strict';
angular.module('coverage').controller('CoverageController', [
  '$scope',
  function ($scope) {
    var initCoverage = function () {
      var data = google.visualization.arrayToDataTable($scope.coverage), tree = new google.visualization.TreeMap(document.getElementById('coverage'));
      tree.draw(data, {
        minColor: '#f56954',
        midColor: '#ffcc73',
        maxColor: '#1acd70',
        headerHeight: 15,
        fontColor: 'black',
        showScale: true,
        maxColorValue: 100,
        useWeightedAverageForAggregation: true
      });
    };
    $scope.init = function () {
      initCoverage();
    };
    $scope.init();
  }
]);'use strict';
angular.module('coverage').directive('coverage', [function () {
    return {
      restrict: 'E',
      template: '<div></div>',
      replace: true,
      scope: { data: '=' },
      link: function (scope, element, attrs) {
        var data = google.visualization.arrayToDataTable(scope.data), tree = new google.visualization.TreeMap(document.getElementById('coverage'));
        tree.draw(data, {
          minColor: '#ff0000',
          midColor: '#FF7722',
          maxColor: '#00ff00',
          headerHeight: 15,
          fontColor: 'black',
          showScale: true,
          maxColorValue: 100,
          useWeightedAverageForAggregation: true
        });
      }
    };
  }]);'use strict';
angular.module('deployment').controller('ChangeSetController', [
  '$scope',
  'artifactName',
  'artifact',
  'version',
  'host',
  'installations',
  'max_artifacts',
  'all_artifacts',
  '$http',
  'Authentication',
  'growl',
  '$splash',
  function ($scope, artifactName, artifact, version, host, installations, max_artifacts, all_artifacts, $http, Authentication, growl, $splash) {
    $scope.version = version;
    $scope.artifact = {};
    $scope.artifactPage = artifact;
    $scope.index = 0;
    $scope.artifactCount = 0;
    $scope.artifactsCommits = [];
    $scope.hasprev = true;
    $scope.hasnext = true;
    $scope.account = Authentication.account;
    var count = function () {
      $scope.artifactCount = 0;
      $scope.artifactsCommits = [];
      if ($scope.artifact.artifacts[$scope.index].changesets.length > 0) {
        $scope.artifact.artifacts[$scope.index].changesets.forEach(function (changeset, changesetIndex) {
          $scope.artifactCount = $scope.artifactCount + changeset.commitCount;
          Array.prototype.push.apply($scope.artifactsCommits, changeset.commits);
        });
      }
      $scope.hasnext = typeof $scope.artifact.artifacts[$scope.index + 1] !== 'undefined';
      $scope.hasprev = typeof $scope.artifact.artifacts[$scope.index - 1] !== 'undefined';
    };
    $scope.next = function () {
      $scope.index++;
      count();
    };
    $scope.prev = function () {
      $scope.index--;
      count();
    };
    // return
    $scope.updateJiraStatus = function () {
      var data = { 'commits': $scope.artifactsCommits };
      $http.post('/jira/issue/status', data).success(function (cb) {
        $scope.artifactsCommits = cb;
      }).error(function (error) {
        growl.error(error);
      });
    };
    // approve artifact
    $scope.approve = function (artifact) {
      $scope.artifactPage.searched = false;
      var artifactPageIndex = $scope.artifactPage.artifacts.findIndex(function (x) {
          return x.name === artifact.name;
        });
      var data = { 'name': artifact.name };
      $http.put('/artifact/approve', data).success(function (data) {
        artifact.approved = true;
        artifact.approvedBy = $scope.account.displayName;
        if (typeof artifactPageIndex !== 'undefined') {
          $scope.artifactPage.artifacts[artifactPageIndex].approved = true;
          $scope.artifactPage.artifacts[artifactPageIndex].approvedBy = $scope.account.displayName;
          $scope.artifactPage.searched = true;
        }
      });
    };
    // start artifact deployment
    $scope.deploy = function (artifactName) {
      var answer = window.confirm('Sure?');
      if (answer) {
        $splash.open({
          artifact: $scope.artifact.artifacts[$scope.index],
          host: host,
          installations: installations
        }, { templateUrl: 'modules/deployment/views/deployment-splash.client.view.html' });
      }
    };
    $scope.init = function () {
      $http.get('artifacts?start=0&version=' + $scope.version + '&max=0&all=' + all_artifacts).success(function (data) {
        $scope.artifact.artifacts = data.artifacts;
        $scope.artifactCount = data.total;
        $scope.index = $scope.artifact.artifacts.findIndex(function (x) {
          return x.name === artifactName;
        });
        count();
      });
    };
    $scope.init();
  }
]);'use strict';
angular.module('deployment').controller('CommentController', [
  '$scope',
  'instanceArtifact',
  '$modalInstance',
  '$http',
  function ($scope, instanceArtifact, $modalInstance, $http) {
    $scope.artifact = instanceArtifact;
    $scope.comment = $scope.artifact.comment;
    $scope.ok = function () {
      var data = {
          'name': $scope.artifact.name,
          'comment': $scope.comment
        };
      $http.put('/artifact/comment', data).success(function (data) {
        $scope.artifact.comment = $scope.comment;
        $modalInstance.close();
      });
    };
  }
]);'use strict';
angular.module('deployment').controller('DeploymentStatusController', [
  '$scope',
  '$http',
  '$modal',
  function ($scope, $http, $modal) {
    $scope.distributionPercentage = 0;
    $scope.distributionStatus = 'Initializing...';
    $scope.distributionType = 'default';
    $scope.distributionBarStatus = 'progress-striped active';
    $scope.healselfTemplate = 'template/deployment/healself.html';
    var getBuildUploadStatus = {
        'id': 1,
        'service': 'status',
        'method': 'getBuildUploadStatus',
        'params': [$scope.artifact.name]
      };
    var getDeployStatus = function (installation) {
      return {
        'id': 1,
        'service': 'status',
        'method': 'getDeployStatus',
        'params': [installation]
      };
    };
    var isDeploying = function (installation) {
      return {
        'id': 1,
        'service': 'status',
        'method': 'isDeploying',
        'params': [installation]
      };
    };
    var deployBuild = function (installation) {
      return {
        'id': 1,
        'service': 'deploy',
        'method': 'deployBuild',
        'params': [
          installation,
          $scope.artifact.name,
          true
        ]
      };
    };
    /**
         * Wait for deployment to be completed
         */
    function waitForDeploymentCompleted(installation, index) {
      var progress = setInterval(function () {
          $http.post($scope.host.hostURL, getDeployStatus(installation.name)).success(function (data) {
            if (data.result[0] === -1) {
              clearInterval(progress);
              if (!$scope.installations[index].deploymentFailed) {
                $scope.installations[index].deploymentPercentage = 100;
                $scope.installations[index].deploymentStatus = 'Build was successfully deployed';
                $scope.installations[index].deploymentType = 'success';
                $scope.installations[index].deploymentBarStatus = '';
                $scope.installations[index].deploymentFinnished = true;
              }
            } else {
              $scope.installations[index].deploymentPercentage = data.result.progress;
              $scope.installations[index].deploymentStatus = data.result.message;
            }
          });
        }, 800);
    }
    /**
         * Initialize build deployment
         */
    function deploy() {
      $scope.installations.forEach(function (installation, index) {
        $scope.installations[index].deploymentStarted = true;
        $scope.installations[index].deploymentPercentage = 0;
        $scope.installations[index].deploymentStatus = 'Initializing...';
        $scope.installations[index].deploymentType = 'default';
        $scope.installations[index].deploymentBarStatus = 'progress-striped active';
        $http.post($scope.host.hostURL, isDeploying(installation.name)).success(function (data) {
          if (data.result === false) {
            $http.post($scope.host.hostURL, deployBuild(installation.name)).success(function (data) {
              if (typeof data.error !== 'undefined') {
                $scope.installations[index].deploymentPercentage = 100;
                $scope.installations[index].deploymentStatus = data.error.message;
                $scope.installations[index].deploymentType = 'danger';
                $scope.installations[index].deploymentBarStatus = '';
                $scope.installations[index].deploymentFailed = true;
                $scope.installations[index].deploymentFinnished = true;
              }
            });
            waitForDeploymentCompleted(installation, index);
          } else {
            $scope.installations[index].deploymentPercentage = 100;
            $scope.installations[index].deploymentStatus = 'Deployment not possible. Already deploying';
            $scope.installations[index].deploymentType = 'warning';
            $scope.installations[index].deploymentBarStatus = '';
            $scope.installations[index].deploymentFailed = true;
            $scope.installations[index].deploymentFinnished = true;
          }
        });
      });
    }
    /**
         * Initialize upload progress bar
         */
    function initUploadProgressBar() {
      var progress = setInterval(function () {
          $http.post($scope.host.hostURL, getBuildUploadStatus).success(function (data) {
            $scope.distributionPercentage = data.result.progress;
            $scope.distributionStatus = data.result.message;
            if (data.result[0] === -1) {
              clearInterval(progress);
              $scope.distributionPercentage = 100;
              $scope.distributionStatus = 'Build was successfully distributed';
              $scope.distributionType = 'success';
              $scope.distributionBarStatus = '';
              deploy();
            }
          });
        }, 800);
    }
    /**
         * Wait for upload process to start
         */
    function waitForUploadProcess() {
      //the artifact needs to be distributed (we want to check the upload status)
      var timeout = 0;
      var waitForUpload = setInterval(function () {
          //we use this timeout to be sure that the upload process has started
          if (timeout >= 10) {
            alert('Something went wrong when getting the distribution process');
            $scope.distributionPercentage = 100;
            $scope.distributionStatus = 'Build could not be distributed';
            $scope.distributionType = 'danger';
            $scope.distributionBarStatus = '';
            clearInterval(waitForUpload);
          } else {
            timeout = timeout + 1;
          }
          /**
                 * wait until upload process is started
                 */
          $http.post($scope.host.hostURL, getBuildUploadStatus).success(function (data) {
            if (data.result[0] !== -1) {
              // OK we have an upload process
              clearInterval(waitForUpload);
              initUploadProgressBar();
            }
          });
        }, 1000);
    }
    $scope.init = function () {
      $scope.installations.forEach(function (installation, index) {
        $scope.installations[index].deploymentStarted = true;
        $scope.installations[index].deploymentPercentage = 0;
        $scope.installations[index].deploymentStatus = 'Initializing...';
        $scope.installations[index].deploymentType = 'default';
        $scope.installations[index].deploymentBarStatus = 'progress-striped active';
        $scope.installations[index].deploymentFinnished = false;
        $scope.installations[index].deploymentFailed = false;
      });
      var data = {
          'id': 1,
          'service': 'deploy',
          'method': 'buildExists',
          'params': [$scope.artifact.name]
        };
      $http.post($scope.host.hostURL, data).success(function (data) {
        if (typeof data.error !== 'undefined') {
          initUploadProgressBar();
        } else {
          if (data.result === true) {
            initUploadProgressBar();
          } else {
            var body = { 'artifact': $scope.artifact.artifactory };
            $http.put($scope.host.hostURL + '/?service=deploy&build=' + $scope.artifact.name + '&artifactory=true', body);
            waitForUploadProcess();
          }
        }
      });
    };
    $scope.init();
    // healself modal
    $scope.healself = function (installation) {
      $modal.open({
        templateUrl: $scope.healselfTemplate,
        controller: 'HealselfController',
        size: 'lg',
        resolve: {
          healself: function ($http) {
            var getHealselfLog = {
                'id': 1,
                'service': 'status',
                'method': 'getHealselfLog',
                'params': [
                  installation,
                  true
                ]
              };
            return $http.post($scope.host.hostURL, getHealselfLog);
          },
          installation: function () {
            return installation;
          }
        }
      });
    };
  }
]).run([
  '$templateCache',
  function ($templateCache) {
    // template for comment modal
    $templateCache.put('template/deployment/healself.html', '<div class="modal-header"><h3 class="modal-title">Healself log</h3></div>' + '<div class="modal-body" slim-scroll h="600px" s="5px">' + '<p class="lead">{{ installation }}</p>' + '<pre data-ng-bind-html="healself"></pre>' + '</div>' + '<div class="modal-footer">' + '<button class="btn btn-primary" data-ng-click="$close()">Ok</button>' + '</div>' + '</div>');
  }
]);'use strict';
angular.module('deployment').controller('DeploymentController', [
  '$scope',
  '$http',
  '$modal',
  'Authentication',
  '$splash',
  'socket',
  'growl',
  function ($scope, $http, $modal, Authentication, $splash, socket, growl) {
    $scope.account = Authentication.account;
    $scope.selectedHost = null;
    $scope.installations = [];
    $scope.selectedInstallations = [];
    $scope.selectedVersions = [];
    $scope.selectedVersionsModel = [];
    $scope.artifact = {};
    $scope.selectedInstallationsDetails = [];
    $scope.modulesTemplate = 'template/deployment/modules.html';
    $scope.customModulesTemplate = 'template/deployment/custom.html';
    $scope.changesetsTemplate = 'template/deployment/changeset.html';
    $scope.commentTemplate = 'template/deployment/comment.html';
    $scope.currentArtifactPage = 0;
    $scope.favoriteInst = $scope.account.favoriteInst;
    $scope.myFavoriteInst = [];
    $scope.allVersionsShowed = false;
    // for artifact management
    $scope.product = {};
    $scope.versions = [];
    var max_artifacts = 5, all_artifacts = false;
    // help function to remove element from array
    function remove(arr, what) {
      var found = arr.indexOf(what);
      while (found !== -1) {
        arr.splice(found, 1);
        found = arr.indexOf(what);
      }
    }
    // initialize deployment page
    var initDeployment = function () {
      $http.get('user/favorite/installations').success(function (data) {
        $scope.myFavoriteInst = data;
      });
    };
    $scope.init = function () {
      initDeployment();
    };
    $scope.init();
    // search for artifacts
    $scope.searchArtifacts = function () {
      if ($scope.selectedVersionsModel.length > 0) {
        $scope.artifact.searched = false;
        var start = 0;
        if ($scope.currentArtifactPage > 0) {
          start = max_artifacts * $scope.currentArtifactPage - max_artifacts;
        }
        $http.get('artifacts?start=' + start + '&version=' + $scope.selectedVersionsModel + '&max=' + max_artifacts + '&all=' + all_artifacts).success(function (data) {
          $scope.artifact.artifacts = data.artifacts;
          $scope.artifact.total = data.total;
          $scope.artifact.searched = true;
        });
      }
    };
    // handle selection of installations
    $scope.selectInstallations = function () {
      $scope.selectedInstallationsDetails = [];
      $scope.selectedVersions = [];
      $scope.selectedVersionsModel = [];
      $scope.artifact = {};
      $scope.artifact.searched = false;
      $scope.currentArtifactPage = 0;
      $scope.allVersionsShowed = false;
      angular.forEach($scope.selectedInstallations, function (value, key) {
        $scope.selectedInstallationsDetails[key] = { name: value };
        if (window.$.inArray(value, $scope.myFavoriteInst) > -1) {
          $scope.selectedInstallationsDetails[key].favorite = 'favorite';
        }
      });
      var count = 0;
      angular.forEach($scope.selectedInstallations, function (value, key) {
        var data = {
            id: 1,
            service: 'installations',
            method: 'getInstallationInfo',
            params: [value]
          };
        $http.post($scope.selectedHost.hostURL, data).success(function (cb) {
          count++;
          if (typeof cb.error !== 'undefined') {
            growl.error(cb.error.message, {});
            if ($scope.selectedVersions.indexOf('DEV') === -1) {
              $scope.selectedVersions.push('DEV');
              $scope.selectedVersionsModel.push('DEV');
            }
          } else {
            $scope.selectedInstallationsDetails[key].info = cb.result;
            if ($scope.selectedVersions.indexOf(cb.result['EFS Version']) === -1) {
              $scope.selectedVersions.push(cb.result['EFS Version'].toUpperCase());
              $scope.selectedVersionsModel.push(cb.result['EFS Version'].toUpperCase());
            }
          }
          if (count === $scope.selectedInstallations.length) {
            $scope.searchArtifacts();
          }
        });
      });
    };
    // get installations from given given host
    $scope.getInstallations = function () {
      $scope.installations = [];
      $scope.selectedInstallations = [];
      $scope.selectedInstallationsDetails = [];
      $scope.selectedVersions = [];
      $scope.selectedVersionsModel = [];
      var data = {
          'id': 1,
          'service': 'status',
          'method': 'getInstallations',
          'params': []
        };
      $http.post($scope.selectedHost.hostURL, data).success(function (cb) {
        cb.result.sort();
        var installations = cb.result;
        if ($scope.favoriteInst) {
          installations.forEach(function (inst) {
            if (window.$.inArray(inst, $scope.myFavoriteInst) > -1) {
              $scope.installations.push(inst);
            }
          });
        } else
          $scope.installations = cb.result;
      });
    };
    // handle artifact page navigation
    $scope.pageArtifactChanged = function () {
      $scope.artifact.searched = false;
      var start = max_artifacts * $scope.currentArtifactPage - max_artifacts;
      $http.get('artifacts?start=' + start + '&version=' + $scope.selectedVersionsModel + '&max=' + max_artifacts + '&all=' + all_artifacts).success(function (data) {
        $scope.artifact.artifacts = data.artifacts;
        $scope.artifact.searched = true;
      });
    };
    // changesets modal
    $scope.open = function (artifactName) {
      $modal.open({
        templateUrl: 'modules/deployment/views/deployment-changeset.client.view.html',
        controller: 'ChangeSetController',
        size: 'lg',
        resolve: {
          artifactName: function () {
            return artifactName;
          },
          artifact: function () {
            return $scope.artifact;
          },
          version: function () {
            return $scope.selectedVersionsModel;
          },
          host: function () {
            return $scope.selectedHost;
          },
          installations: function () {
            return $scope.selectedInstallationsDetails;
          },
          max_artifacts: function () {
            return max_artifacts;
          },
          all_artifacts: function () {
            return all_artifacts;
          }
        }
      });
    };
    // comments modal
    $scope.comment = function (artifact) {
      $modal.open({
        templateUrl: $scope.commentTemplate,
        controller: 'CommentController',
        resolve: {
          instanceArtifact: function () {
            return artifact;
          }
        }
      });
    };
    // toggle show only favorite installations
    $scope.toggleFavoriteInst = function () {
      var data = { 'favoriteInst': $scope.favoriteInst };
      $http.put('/settings/favorite/inst', data).success(function (data) {
        if ($scope.selectedHost)
          $scope.getInstallations();
      });
    };
    // add or remove installation to favorite list
    $scope.handleFavoriteInstallation = function (installation) {
      var data = { 'installation': installation.name };
      if (installation.favorite === 'favorite') {
        $http.delete('/user/favorite/installation/' + installation.name, data).success(function (data) {
          installation.favorite = '';
          remove($scope.myFavoriteInst, installation.name);
        });
      } else {
        $http.post('/user/favorite/installation', data).success(function (data) {
          installation.favorite = 'favorite';
          $scope.myFavoriteInst.push(installation.name);
        });
      }
    };
    // approve artifact
    $scope.approve = function (artifact) {
      $scope.artifact.searched = false;
      var data = { 'name': artifact.name };
      $http.put('/artifact/approve', data).success(function (data) {
        artifact.approved = true;
        artifact.approvedBy = $scope.account.displayName;
        $scope.artifact.searched = true;
      });
    };
    // decline artifact
    $scope.decline = function (artifact) {
      $scope.artifact.searched = false;
      var data = { 'name': artifact.name };
      $http.put('/artifact/decline', data).success(function (data) {
        artifact.declined = true;
        artifact.declinedBy = $scope.account.displayName;
        $scope.artifact.searched = true;
      });
    };
    // revoke artifact decline
    $scope.revoke = function (artifact) {
      $scope.artifact.searched = false;
      var data = { 'name': artifact.name };
      $http.put('/artifact/revoke', data).success(function (data) {
        artifact.declined = false;
        artifact.declinedBy = '';
        $scope.artifact.searched = true;
      });
    };
    // start artifact deployment
    $scope.deploy = function (artifact) {
      var answer = window.confirm('Sure?');
      if (answer) {
        $splash.open({
          artifact: artifact,
          host: $scope.selectedHost,
          installations: $scope.selectedInstallationsDetails
        }, { templateUrl: 'modules/deployment/views/deployment-splash.client.view.html' });
      }
    };
    // start artifact deployment
    $scope.promote = function (artifact) {
      var answer = window.confirm('Sure?');
      if (answer) {
        $splash.open({
          artifactToPromote: artifact,
          artifact: $scope.artifact,
          currentArtifactPage: $scope.currentArtifactPage,
          selectedVersions: $scope.selectedVersions
        }, { templateUrl: 'modules/deployment/views/promotion-splash.client.view.html' });
      }
    };
    // show all versions (only available 4 QA)
    $scope.showAllVersions = function () {
      $scope.artifact.searched = false;
      $http.get('artifacts/versions?product=EFS').success(function (data) {
        $scope.selectedVersions = [];
        $scope.selectedVersionsModel = [];
        data.forEach(function (version) {
          $scope.selectedVersions.push(version);
          $scope.selectedVersionsModel.push(version);
          $scope.allVersionsShowed = true;
        });
        $scope.searchArtifacts();
      });
    };
    // reload artifact list when new artifact has been checked in
    socket.on('artifact:refresh', function (data) {
      $scope.searchArtifacts();
    });
    $scope.getVersions = function () {
      max_artifacts = 12;
      all_artifacts = true;
      $scope.selectedVersionsModel = [];
      $http.get('artifacts/versions?product=' + $scope.product).success(function (data) {
        $scope.versions = data;
      });
    };
    $scope.deleteVersions = function () {
      var answer = window.confirm('This will purge the selected versions from system. Sure?');
      if (answer) {
        $http.delete('artifacts?product=' + $scope.product + '&version=' + $scope.selectedVersionsModel).success(function (data) {
          $scope.selectedVersionsModel = [];
          $scope.product = {};
          $scope.versions = [];
        });
      }
    };
  }
]).run([
  '$templateCache',
  function ($templateCache) {
    // template for modules popover
    $templateCache.put('template/deployment/modules.html', '<span class="modules-label label-danger" ng-repeat="module in installation.info[\'Modules\']">{{ module }} </span>');
  }
]).run([
  '$templateCache',
  function ($templateCache) {
    // template for custom modules popover
    $templateCache.put('template/deployment/custom.html', '<span class="modules-label label-warning" ng-repeat="module in installation.info[\'Custom Modules\']">{{ module }} </span>');
  }
]).run([
  '$templateCache',
  function ($templateCache) {
    // template for comment modal
    $templateCache.put('template/deployment/comment.html', '<div class="modal-header"><h3 class="modal-title">Comment artifact {{ artifact.name }}</h3></div>' + '<div class="modal-body">' + '<textarea rows="6" style="width: 100%" data-ng-model="comment"></textarea>' + '</div>' + '<div class="modal-footer">' + '<button class="btn btn-primary" data-ng-click="ok()">Ok</button>' + '</div>' + '</div>');
  }
]);'use strict';
angular.module('deployment').controller('HealselfController', [
  '$scope',
  'healself',
  'installation',
  function ($scope, healself, installation) {
    $scope.healself = healself.data.result;
    $scope.installation = installation;
  }
]);'use strict';
angular.module('deployment').controller('PromotionStatusController', [
  '$scope',
  '$http',
  'Authentication',
  function ($scope, $http, Authentication) {
    $scope.promotionHost = 'http://old-servicegw.tka.globalpark.com:84';
    //TODO muss eine config variable sein
    $scope.distributionPercentage = 0;
    $scope.distributionStatus = 'Initializing...';
    $scope.distributionType = 'default';
    $scope.distributionBarStatus = 'progress-striped active';
    $scope.distributionFailed = false;
    $scope.promotionStarted = false;
    $scope.alreadyPromoting = false;
    $scope.account = Authentication.account;
    // handle artifact page navigation
    $scope.searchArtifacts = function () {
      $scope.artifact.searched = false;
      var start = 0;
      if ($scope.currentArtifactPage > 0) {
        start = 5 * $scope.currentArtifactPage - 5;
      }
      $http.get('artifacts?start=' + start + '&version=' + $scope.selectedVersions + '&max=5').success(function (data) {
        $scope.artifact.artifacts = data.artifacts;
        $scope.artifact.searched = true;
      });
    };
    var getPromotionStatus = {
        'id': 1,
        'service': 'status',
        'method': 'getPromotionStatus',
        'params': ['EFS']
      };
    /**
         * Wait for deployment to be completed
         */
    function waitForPromotionCompleted() {
      setTimeout(function () {
      }, 2000);
      // wait 2 seconds before start
      var progress = setInterval(function () {
          $http.post($scope.promotionHost, getPromotionStatus).success(function (data) {
            if (data.result[0] === -1) {
              clearInterval(progress);
              if (!$scope.distributionFailed) {
                $scope.distributionPercentage = 100;
                $scope.distributionStatus = 'Artifact was successfully promoted';
                $scope.distributionType = 'success';
                $scope.distributionBarStatus = '';
                $scope.artifactToPromote.promoted = true;
                $scope.artifactToPromote.promotedBy = $scope.account.displayName;
                $scope.searchArtifacts();
              }
            } else {
              if (typeof data.error !== 'undefined') {
                $scope.distributionPercentage = 100;
                $scope.distributionStatus = data.error.message;
                $scope.distributionType = 'danger';
                $scope.distributionBarStatus = '';
              } else {
                $scope.distributionPercentage = data.result.progress;
                $scope.distributionStatus = data.result.message;
              }
            }
          });
        }, 800);
    }
    $scope.init = function () {
      var promotion = {
          'product': $scope.artifactToPromote.product,
          'version': $scope.artifactToPromote.version,
          'name': $scope.artifactToPromote.name,
          'artifactory': $scope.artifactToPromote.artifactory
        };
      $http.post($scope.promotionHost, getPromotionStatus).success(function (data) {
        if (data.result[0] !== -1) {
          $scope.alreadyPromoting = true;
          $scope.searchArtifacts();
        } else {
          $scope.promotionStarted = true;
          $http.put('/artifact/promote', promotion).success(function (data) {
            if (data.result === false) {
              $scope.distributionFailed = true;
              $scope.distributionPercentage = 100;
              $scope.distributionStatus = data.error.message;
              $scope.distributionType = 'danger';
              $scope.distributionBarStatus = '';
            }
          });
          waitForPromotionCompleted();
        }
      });
    };
    $scope.init();
  }
]);'use strict';
angular.module('deployment').directive('checklistModel', [
  '$parse',
  '$compile',
  function ($parse, $compile) {
    // contains
    function contains(arr, item, comparator) {
      if (angular.isArray(arr)) {
        for (var i = arr.length; i--;) {
          if (comparator(arr[i], item)) {
            return true;
          }
        }
      }
      return false;
    }
    // add
    function add(arr, item, comparator) {
      arr = angular.isArray(arr) ? arr : [];
      if (!contains(arr, item, comparator)) {
        arr.push(item);
      }
      return arr;
    }
    // remove
    function remove(arr, item, comparator) {
      if (angular.isArray(arr)) {
        for (var i = arr.length; i--;) {
          if (comparator(arr[i], item)) {
            arr.splice(i, 1);
            break;
          }
        }
      }
      return arr;
    }
    // http://stackoverflow.com/a/19228302/1458162
    function postLinkFn(scope, elem, attrs) {
      // exclude recursion, but still keep the model
      var checklistModel = attrs.checklistModel;
      attrs.$set('checklistModel', null);
      // compile with `ng-model` pointing to `checked`
      $compile(elem)(scope);
      attrs.$set('checklistModel', checklistModel);
      // getter / setter for original model
      var getter = $parse(checklistModel);
      var setter = getter.assign;
      var checklistChange = $parse(attrs.checklistChange);
      var checklistBeforeChange = $parse(attrs.checklistBeforeChange);
      // value added to list
      var value = attrs.checklistValue ? $parse(attrs.checklistValue)(scope.$parent) : attrs.value;
      var comparator = angular.equals;
      if (attrs.hasOwnProperty('checklistComparator')) {
        if (attrs.checklistComparator[0] === '.') {
          var comparatorExpression = attrs.checklistComparator.substring(1);
          comparator = function (a, b) {
            return a[comparatorExpression] === b[comparatorExpression];
          };
        } else {
          comparator = $parse(attrs.checklistComparator)(scope.$parent);
        }
      }
      // watch UI checked change
      scope.$watch(attrs.ngModel, function (newValue, oldValue) {
        if (newValue === oldValue) {
          return;
        }
        if (checklistBeforeChange && checklistBeforeChange(scope) === false) {
          scope[attrs.ngModel] = contains(getter(scope.$parent), value, comparator);
          return;
        }
        setValueInChecklistModel(value, newValue);
        if (checklistChange) {
          checklistChange(scope);
        }
      });
      function setValueInChecklistModel(value, checked) {
        var current = getter(scope.$parent);
        if (angular.isFunction(setter)) {
          if (checked === true) {
            setter(scope.$parent, add(current, value, comparator));
          } else {
            setter(scope.$parent, remove(current, value, comparator));
          }
        }
      }
      // declare one function to be used for both $watch functions
      function setChecked(newArr, oldArr) {
        if (checklistBeforeChange && checklistBeforeChange(scope) === false) {
          setValueInChecklistModel(value, scope[attrs.ngModel]);
          return;
        }
        scope[attrs.ngModel] = contains(newArr, value, comparator);
      }
      // watch original model change
      // use the faster $watchCollection method if it's available
      if (angular.isFunction(scope.$parent.$watchCollection)) {
        scope.$parent.$watchCollection(checklistModel, setChecked);
      } else {
        scope.$parent.$watch(checklistModel, setChecked, true);
      }
    }
    return {
      restrict: 'A',
      priority: 1000,
      terminal: true,
      scope: true,
      compile: function (tElement, tAttrs) {
        if ((tElement[0].tagName !== 'INPUT' || tAttrs.type !== 'checkbox') && tElement[0].tagName !== 'MD-CHECKBOX' && !tAttrs.btnCheckbox) {
          throw 'checklist-model should be applied to `input[type="checkbox"]` or `md-checkbox`.';
        }
        if (!tAttrs.checklistValue && !tAttrs.value) {
          throw 'You should provide `value` or `checklist-value`.';
        }
        // by default ngModel is 'checked', so we set it if not specified
        if (!tAttrs.ngModel) {
          // local scope var storing individual checkbox model
          tAttrs.$set('ngModel', 'checked');
        }
        return postLinkFn;
      }
    };
  }
]);'use strict';
function replaceAll(txt, replace, with_this) {
  return txt.replace(new RegExp(replace, 'g'), with_this);
}
angular.module('deployment').filter('nodomain', [function () {
    return function (input) {
      input = input.replace('.qb-feedback.com', '');
      input = input.replace('.3uu.de', '');
      return input;
    };
  }]).filter('trust', [
  '$sce',
  function ($sce) {
    return function (htmlCode) {
      return $sce.trustAsHtml(htmlCode);
    };
  }
]);'use strict';
angular.module('feature').controller('FeatureController', [
  '$scope',
  'Editor',
  '$http',
  '$anchorScroll',
  function ($scope, Editor, $http, $anchorScroll) {
    var tree;
    $scope.ftree = tree = {};
    $scope.last_selected_feature = Editor.getLastSelected();
    $scope.browsers = [
      {
        label: 'Chrome (latest)',
        name: 'chrome_cloud',
        os: 'Windows 8.1'
      },
      {
        label: 'Firefox (latest)',
        name: 'firefox_cloud',
        os: 'Windows 8.1'
      },
      {
        label: 'Chrome (latest)',
        name: 'chrome_cloud',
        os: 'Windows 7'
      },
      {
        label: 'Firefox (latest)',
        name: 'firefox_cloud',
        os: 'Windows 7'
      }
    ];
    $scope.installations = [
      {
        label: 'http://dev-aat-tka.qb-feedback.com',
        environment: 'Testkanal'
      },
      {
        label: 'http://dev-integration.qb-feedback.com',
        environment: 'Automation Farm'
      }
    ];
    $scope.pie = {
      options: {
        chart: {
          type: 'pie',
          height: 150,
          margin: [
            0,
            0,
            0,
            0
          ]
        },
        plotOptions: {
          pie: {
            size: '80%',
            dataLabels: { enabled: false },
            showInLegend: true
          }
        },
        legend: {
          enabled: false,
          layout: 'vertical',
          align: 'left',
          verticalAlign: 'middle'
        }
      },
      series: [{
          data: [
            [
              'Firefox',
              45
            ],
            [
              'IE',
              26.8
            ],
            [
              'Chrome',
              12.8
            ],
            [
              'Safari',
              8.5
            ],
            [
              'Opera',
              6.2
            ]
          ]
        }],
      title: {
        text: 'Feature statistics',
        style: { display: 'none' }
      },
      loading: false
    };
    $scope.column = {
      options: {
        chart: {
          type: 'column',
          height: 150,
          margin: [
            0,
            0,
            0,
            0
          ]
        },
        plotOptions: {
          pie: {
            size: '80%',
            dataLabels: { enabled: false }
          }
        },
        legend: { enabled: false }
      },
      series: [{
          data: [
            [
              'Firefox',
              45
            ],
            [
              'IE',
              26.8
            ],
            [
              'Chrome',
              12.8
            ],
            [
              'Safari',
              8.5
            ],
            [
              'Opera',
              6.2
            ]
          ]
        }],
      title: {
        text: 'Feature statistics',
        style: { display: 'none' }
      },
      loading: false
    };
    $scope.browser = $scope.browsers[0];
    $scope.installation = $scope.installations[0];
    var search = function (features, found) {
      $scope.folder_loaded = false;
      if (features.data.type === 'folder') {
        if (features.children.length > 0) {
          angular.forEach(features.children, function (value, key) {
            if (value.data.type === 'folder') {
              if (value.children.length > 0) {
                search(value, found);
              }
            } else {
              if (value.label.match($scope.featureSearch))
                found.push(value);
            }
          });
        }
      }
      return found;
    };
    $scope.$watch('featureSearch', function () {
      if ($scope.featureSearch) {
        $scope.feature_loaded = false;
        var found = [];
        $scope.filteredSearch = $scope.features;
        $scope.filteredSearch = search($scope.features[0], found);
      } else {
        $scope.filteredSearch = $scope.features;
      }
    });
    $scope.features_handler = function (branch) {
      Editor.setLastSelected(branch.label);
      if (branch.data.type === 'folder') {
        $scope.feature_loaded = false;
        $scope.folder_loaded = true;
        if (branch.selected) {
          $scope.child_features = [];
          var getChildren = function (branch) {
            for (var index in branch.children) {
              if (branch.children[index].label.split('.').pop() === 'feature') {
                //branch.children[index].group = branch.data.label;
                $scope.child_features.push(branch.children[index]);
              }
              getChildren(branch.children[index]);
            }
          };
          getChildren(branch);
        }
      } else {
        $scope.feature_loaded = true;
        $scope.folder_loaded = false;
        var path = { path: branch.data.path };
        $http.post('/feature', path).success(function (response) {
          var editor = Editor.getEditor() === null ? Editor.createEditor() : Editor.getEditor();
          editor.getSession().setValue(response + '\n\n\n');
          $anchorScroll();
        }).error(function (response) {
          $scope.error = response;
        });
      }
    };
  }
]);'use strict';
angular.module('feature').directive('featureTree', [
  '$timeout',
  function ($timeout) {
    return {
      restrict: 'E',
      template: '<ul class="nav nav-list nav-pills nav-stacked abn-tree">\n  <li ng-repeat="row in tree_rows | filter:{visible:true} track by row.branch.uid" ng-animate="\'abn-tree-animate\'" ng-class="\'level-\' + {{ row.level }} + (row.branch.selected ? \' active\':\'\')" class="abn-tree-row {{row.branch.data.type}}">\n    <a ng-click="user_clicks_branch(row.branch)">\n      <i ng-class="row.tree_icon" ng-click="row.branch.expanded = !row.branch.expanded" class="indented tree-icon"> </i>\n      <span class="indented tree-label">{{ row.label }} </span>\n    </a>\n  </li>\n</ul>',
      replace: true,
      scope: {
        treeData: '=',
        onSelect: '&',
        initialSelection: '@',
        treeControl: '='
      },
      link: function (scope, element, attrs) {
        var error, expand_all_parents, expand_level, for_all_ancestors, for_each_branch, get_parent, n, on_treeData_change, select_branch, selected_branch, tree, feature_count;
        error = function (s) {
          console.log('ERROR:' + s);
          debugger;
          return void 0;
        };
        if (attrs.iconExpand == null) {
          attrs.iconExpand = 'icon-plus  glyphicon glyphicon-plus  fa fa-plus';
        }
        if (attrs.iconCollapse == null) {
          attrs.iconCollapse = 'icon-minus glyphicon glyphicon-minus fa fa-minus';
        }
        if (attrs.iconLeaf == null) {
          attrs.iconLeaf = 'icon-file  glyphicon glyphicon-file  fa fa-file';
        }
        if (attrs.expandLevel == null) {
          attrs.expandLevel = '3';
        }
        expand_level = parseInt(attrs.expandLevel, 10);
        if (!scope.treeData) {
          alert('no treeData defined for the tree!');
          return;
        }
        if (scope.treeData.length == null) {
          if (treeData.label != null) {
            scope.treeData = [treeData];
          } else {
            alert('treeData should be an array of root branches');
            return;
          }
        }
        for_each_branch = function (f) {
          var do_f, root_branch, _i, _len, _ref, _results;
          do_f = function (branch, level) {
            var child, _i, _len, _ref, _results;
            f(branch, level);
            if (branch.children != null) {
              _ref = branch.children;
              _results = [];
              for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                child = _ref[_i];
                _results.push(do_f(child, level + 1));
              }
              return _results;
            }
          };
          _ref = scope.treeData;
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            root_branch = _ref[_i];
            _results.push(do_f(root_branch, 1));
          }
          return _results;
        };
        selected_branch = null;
        select_branch = function (branch) {
          if (!branch) {
            if (selected_branch != null) {
              selected_branch.selected = false;
            }
            selected_branch = null;
            return;
          }
          if (branch !== selected_branch) {
            if (selected_branch != null) {
              selected_branch.selected = false;
            }
            branch.selected = true;
            selected_branch = branch;
            expand_all_parents(branch);
            if (branch.onSelect != null) {
              return $timeout(function () {
                return branch.onSelect(branch);
              });
            } else {
              if (scope.onSelect != null) {
                return $timeout(function () {
                  return scope.onSelect({ branch: branch });
                });
              }
            }
          }
        };
        scope.user_clicks_branch = function (branch) {
          if (branch !== selected_branch) {
            return select_branch(branch);
          }
        };
        get_parent = function (child) {
          var parent;
          parent = void 0;
          if (child.parent_uid) {
            for_each_branch(function (b) {
              if (b.uid === child.parent_uid) {
                return parent = b;
              }
            });
          }
          return parent;
        };
        for_all_ancestors = function (child, fn) {
          var parent;
          parent = get_parent(child);
          if (parent != null) {
            fn(parent);
            return for_all_ancestors(parent, fn);
          }
        };
        expand_all_parents = function (child) {
          return for_all_ancestors(child, function (b) {
            return b.expanded = true;
          });
        };
        scope.tree_rows = [];
        on_treeData_change = function () {
          var add_branch_to_list, root_branch, _i, _len, _ref, _results;
          for_each_branch(function (b, level) {
            if (!b.uid) {
              return b.uid = '' + Math.random();
            }
          });
          for_each_branch(function (b) {
            var child, _i, _len, _ref, _results;
            if (angular.isArray(b.children)) {
              _ref = b.children;
              _results = [];
              for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                child = _ref[_i];
                _results.push(child.parent_uid = b.uid);
              }
              return _results;
            }
          });
          scope.tree_rows = [];
          for_each_branch(function (branch) {
            var child, f;
            if (branch.children) {
              if (branch.children.length > 0) {
                f = function (e) {
                  if (typeof e === 'string') {
                    return {
                      label: e,
                      children: []
                    };
                  } else {
                    return e;
                  }
                };
                return branch.children = function () {
                  var _i, _len, _ref, _results;
                  _ref = branch.children;
                  _results = [];
                  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    child = _ref[_i];
                    _results.push(f(child));
                  }
                  return _results;
                }();
              }
            } else {
              return branch.children = [];
            }
          });
          add_branch_to_list = function (level, branch, visible) {
            var type, child, child_visible, tree_icon, _i, _len, _ref, _results;
            if (branch.expanded == null) {
              branch.expanded = false;
            }
            if (!branch.children || branch.children.length === 0) {
              tree_icon = attrs.iconLeaf;
            } else {
              if (branch.expanded) {
                tree_icon = attrs.iconCollapse;
              } else {
                tree_icon = attrs.iconExpand;
              }
            }
            scope.tree_rows.push({
              level: level,
              branch: branch,
              label: branch.label,
              tree_icon: tree_icon,
              visible: visible
            });
            if (branch.children != null) {
              _ref = branch.children;
              _results = [];
              for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                child = _ref[_i];
                child_visible = visible && branch.expanded;
                _results.push(add_branch_to_list(level + 1, child, child_visible));
              }
              return _results;
            }
          };
          _ref = scope.treeData;
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            root_branch = _ref[_i];
            _results.push(add_branch_to_list(1, root_branch, true));
          }
          return _results;
        };
        scope.$watch('treeData', on_treeData_change, true);
        if (attrs.initialSelection != null) {
          feature_count = 0;
          for_each_branch(function (b) {
            if (b.data.type === 'feature')
              feature_count++;
          });
          for_each_branch(function (b) {
            if (b.label === attrs.initialSelection) {
              return $timeout(function () {
                return select_branch(b);
              });
            }
          });
        }
        n = scope.treeData.length;
        for_each_branch(function (b, level) {
          b.level = level;
          return b.expanded = b.level < expand_level;
        });
        if (scope.treeControl != null) {
          if (angular.isObject(scope.treeControl)) {
            tree = scope.treeControl;
            tree.expand_all = function () {
              return for_each_branch(function (b, level) {
                return b.expanded = true;
              });
            };
            tree.collapse_all = function () {
              return for_each_branch(function (b, level) {
                return b.expanded = false;
              });
            };
            tree.get_first_branch = function () {
              n = scope.treeData.length;
              if (n > 0) {
                return scope.treeData[0];
              }
            };
            tree.select_first_branch = function () {
              var b;
              b = tree.get_first_branch();
              return tree.select_branch(b);
            };
            tree.get_selected_branch = function () {
              return selected_branch;
            };
            tree.get_parent_branch = function (b) {
              return get_parent(b);
            };
            tree.select_branch = function (b) {
              select_branch(b);
              return b;
            };
            tree.get_children = function (b) {
              return b.children;
            };
            tree.select_parent_branch = function (b) {
              var p;
              if (b == null) {
                b = tree.get_selected_branch();
              }
              if (b != null) {
                p = tree.get_parent_branch(b);
                if (p != null) {
                  tree.select_branch(p);
                  return p;
                }
              }
            };
            tree.add_branch = function (parent, new_branch) {
              if (parent != null) {
                parent.children.push(new_branch);
                parent.expanded = true;
              } else {
                scope.treeData.push(new_branch);
              }
              return new_branch;
            };
            tree.add_root_branch = function (new_branch) {
              tree.add_branch(null, new_branch);
              return new_branch;
            };
            tree.expand_branch = function (b) {
              if (b == null) {
                b = tree.get_selected_branch();
              }
              if (b != null) {
                b.expanded = true;
                return b;
              }
            };
            tree.collapse_branch = function (b) {
              if (b == null) {
                b = selected_branch;
              }
              if (b != null) {
                b.expanded = false;
                return b;
              }
            };
            tree.get_siblings = function (b) {
              var p, siblings;
              if (b == null) {
                b = selected_branch;
              }
              if (b != null) {
                p = tree.get_parent_branch(b);
                if (p) {
                  siblings = p.children;
                } else {
                  siblings = scope.treeData;
                }
                return siblings;
              }
            };
            tree.get_next_sibling = function (b) {
              var i, siblings;
              if (b == null) {
                b = selected_branch;
              }
              if (b != null) {
                siblings = tree.get_siblings(b);
                n = siblings.length;
                i = siblings.indexOf(b);
                if (i < n) {
                  return siblings[i + 1];
                }
              }
            };
            tree.get_prev_sibling = function (b) {
              var i, siblings;
              if (b == null) {
                b = selected_branch;
              }
              siblings = tree.get_siblings(b);
              n = siblings.length;
              i = siblings.indexOf(b);
              if (i > 0) {
                return siblings[i - 1];
              }
            };
            tree.select_next_sibling = function (b) {
              var next;
              if (b == null) {
                b = selected_branch;
              }
              if (b != null) {
                next = tree.get_next_sibling(b);
                if (next != null) {
                  return tree.select_branch(next);
                }
              }
            };
            tree.select_prev_sibling = function (b) {
              var prev;
              if (b == null) {
                b = selected_branch;
              }
              if (b != null) {
                prev = tree.get_prev_sibling(b);
                if (prev != null) {
                  return tree.select_branch(prev);
                }
              }
            };
            tree.get_first_child = function (b) {
              var _ref;
              if (b == null) {
                b = selected_branch;
              }
              if (b != null) {
                if (((_ref = b.children) != null ? _ref.length : void 0) > 0) {
                  return b.children[0];
                }
              }
            };
            tree.get_closest_ancestor_next_sibling = function (b) {
              var next, parent;
              next = tree.get_next_sibling(b);
              if (next != null) {
                return next;
              } else {
                parent = tree.get_parent_branch(b);
                return tree.get_closest_ancestor_next_sibling(parent);
              }
            };
            tree.get_next_branch = function (b) {
              var next;
              if (b == null) {
                b = selected_branch;
              }
              if (b != null) {
                next = tree.get_first_child(b);
                if (next != null) {
                  return next;
                } else {
                  next = tree.get_closest_ancestor_next_sibling(b);
                  return next;
                }
              }
            };
            tree.select_next_branch = function (b) {
              var next;
              if (b == null) {
                b = selected_branch;
              }
              if (b != null) {
                next = tree.get_next_branch(b);
                if (next != null) {
                  tree.select_branch(next);
                  return next;
                }
              }
            };
            tree.last_descendant = function (b) {
              var last_child;
              if (b == null) {
                debugger;
              }
              n = b.children.length;
              if (n === 0) {
                return b;
              } else {
                last_child = b.children[n - 1];
                return tree.last_descendant(last_child);
              }
            };
            tree.get_prev_branch = function (b) {
              var parent, prev_sibling;
              if (b == null) {
                b = selected_branch;
              }
              if (b != null) {
                prev_sibling = tree.get_prev_sibling(b);
                if (prev_sibling != null) {
                  return tree.last_descendant(prev_sibling);
                } else {
                  parent = tree.get_parent_branch(b);
                  return parent;
                }
              }
            };
            tree.get_features_count = function () {
              return feature_count;
            };
            tree.set_features_count = function (count) {
              feature_count = count;
            };
            return tree.select_prev_branch = function (b) {
              var prev;
              if (b == null) {
                b = selected_branch;
              }
              if (b != null) {
                prev = tree.get_prev_branch(b);
                if (prev != null) {
                  tree.select_branch(prev);
                  return prev;
                }
              }
            };
          }
        }
      }
    };
  }
]);'use strict';
angular.module('feature').service('Editor', function () {
  var editor = null;
  var last_selected_feature = '';
  this.createEditor = function () {
    editor = window.ace.edit('web-editor');
    editor.renderer.setShowGutter(false);
    editor.renderer.setShowPrintMargin(false);
    editor.setTheme('ace/theme/monokai');
    editor.getSession().setMode('ace/mode/gherkin');
    editor.getSession().setTabSize(2);
    editor.setReadOnly(true);
    editor.setOptions({ maxLines: 1000 });
    return editor;
  };
  this.init = function () {
    editor = null;
  };
  this.getEditor = function () {
    return editor;
  };
  this.getLastSelected = function () {
    return last_selected_feature;
  };
  this.setLastSelected = function (feature) {
    last_selected_feature = feature;
  };
});'use strict';
angular.module('core').controller('BuildController', [
  '$scope',
  '$http',
  function ($scope, $http) {
    $scope.build = {};
    $scope.build.title = 'Commit Stage';
    $scope.build.version = 'DEV';
    // initialize build status
    var initBuilds = function () {
      $scope.buildsStarted = false;
      $http.get('jenkins/builds').success(function (data) {
        $scope.buildStatus = data;
        $scope.buildsStarted = true;
      });
    };
    // get test report from a given jenkins job
    var initTestReport = function () {
      $scope.testReportStarted = false;
      $http.get('jenkins/testreport?job=EFS_DEV_AcceptanceStage_WebDriver').success(function (data) {
        $scope.testreport = data;
        $scope.testReportStarted = true;
      });
    };
    $scope.init = function () {
      initBuilds();
      initTestReport();
    };
    $scope.init();
    // draggable build options
    $scope.buildStatusOptions = {
      stop: function (e, ui) {
        for (var index in $scope.buildStatus) {
          $scope.buildStatus[index].position = index;
        }
        $http.put('jenkins/build/position', { builds: $scope.buildStatus });
      }
    };
    // add build start
    $scope.addBuildStatus = function () {
      $scope.addingBuildStatus = true;
      $http.get('jenkins/build/stages').success(function (data) {
        $scope.BuildStages = data;
      });
    };
    // add build confirm
    $scope.addBuildStatusConfirm = function (status) {
      status.stage = status.title.replace(/ /g, '');
      $http.post('jenkins/build', { status: status }).success(function (data) {
        $scope.addingBuildStatus = false;
        initBuilds();
      });
    };
    // remove build status
    $scope.deleteBuildStatus = function (id) {
      $http.delete('jenkins/build/' + id).success(function (data) {
        initBuilds();
      });
    };
  }
]).controller('TestReportController', [
  '$scope',
  '$http',
  function ($scope, $http) {
    // get test report from a given jenkins job
    var initTestReport = function () {
      $scope.testReportStarted = false;
      $http.get('jenkins/testreport?job=EFS_DEV_AcceptanceStage_WebDriver').success(function (data) {
        $scope.testReport = {
          options: {
            credits: { enabled: false },
            chart: { type: 'pie' },
            plotOptions: {
              pie: {
                colors: [
                  '#1acd70',
                  '#ff876c'
                ],
                dataLabels: { format: '<strong>{point.percentage:.0f} </strong> %' },
                showInLegend: true
              }
            },
            legend: {
              enabled: true,
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          },
          series: [{
              data: [
                [
                  'Passed',
                  data.passCount
                ],
                {
                  name: 'Failed',
                  y: data.failCount,
                  sliced: true,
                  selected: true
                }
              ]
            }],
          title: { style: { display: 'none' } },
          loading: false
        };
        $scope.testReportStarted = true;
      });
    };
    $scope.init = function () {
      initTestReport();
    };
    $scope.init();
  }
]);'use strict';
angular.module('jira').controller('JiraConfigController', [
  '$scope',
  'Authentication',
  '$http',
  function ($scope, Authentication, $http) {
    $scope.jira = {};
    $scope.jira.task = {};
    $scope.account = Authentication.account;
    var types = [
        'warning',
        'info',
        'success',
        'danger'
      ];
    // initialize build status
    var initJiras = function () {
      $http.get('jira/block/config').success(function (data) {
        if (data.length === 4) {
          angular.forEach(data, function (value, key) {
            $scope.jira[value.type] = value;
            $scope.jira[value.type].loaded = true;
          });
        } else {
          angular.forEach(types, function (value, key) {
            $scope.jira[value] = {};
            $scope.jira[value].title = 'Not defined';
            $scope.jira[value].jql = 'Not defined';
            $scope.jira[value].loaded = true;
          });
          angular.forEach(data, function (value, key) {
            $scope.jira[value.type] = value;
            $scope.jira[value.type].loaded = true;
          });
        }
      });
      $http.get('jira/task/config').success(function (data) {
        $scope.jira.task.jql = 'Not defined';
        if (data.jql) {
          $scope.jira.task.jql = data.jql;
        }
        $scope.jira.task.loaded = true;
      });
    };
    $scope.init = function () {
      initJiras();
    };
    $scope.init();
    $scope.updateBox = function (type) {
      $scope.jira[type].loaded = false;
      if ($scope.jira[type].jql.length > 0) {
        $http.put('jira/block/update', {
          jira: $scope.jira[type],
          type: type
        }).success(function (data) {
          $scope.jira[type].loaded = true;
        }).error(function (data) {
          $scope.jira[type].loaded = true;
          alert(data);
        });
      } else {
        alert('No JQL? Who are you? A tester?');
        $scope.jira[type].loaded = true;
      }
    };
    $scope.updateTask = function (type) {
      $scope.jira.task.loaded = false;
      if ($scope.jira.task.jql.length > 0) {
        $http.put('jira/task/update', { jql: $scope.jira.task.jql }).success(function (data) {
          $scope.jira.task.loaded = true;
        }).error(function (data) {
          $scope.jira.task.loaded = true;
          alert(data);
        });
      } else {
        alert('No JQL? Who are you? A tester?');
        $scope.jira.task.loaded = true;
      }
    };
    $scope.setDefaultBoxes = function () {
      $scope.jira.warning.title = 'Open Support Tickets';
      $scope.jira.warning.jql = 'project = "K@nban" AND status != Closed AND "Support Request" = yes and assignee = gp-schnitte';
      $scope.jira.success.title = 'QA Closed Tickets in This Year';
      $scope.jira.success.jql = 'category = "QA Germany" AND status = Closed AND assignee in membersOf("QA Germany") and updated <= endOfYear() AND updated >= startOfYear()';
      $scope.jira.info.title = 'Kanban Ready For Test';
      $scope.jira.info.jql = 'project = KANBAN AND status = Resolved AND assignee in membersOf("QA Germany")';
      $scope.jira.danger.title = 'Critical Bugs';
      $scope.jira.danger.jql = 'project = "K@nban" AND status != Closed AND priority = "Priority 1"';
      angular.forEach(types, function (value, key) {
        $scope.updateBox(value);
      });
    };
    $scope.setDefaultTask = function () {
      $scope.jira.task.jql = 'category = "QA Germany" AND status != Closed AND assignee = ' + $scope.account.userName;
      $scope.updateTask();
    };
  }
]);'use strict';
angular.module('jira').controller('JiraHomeController', [
  '$scope',
  '$http',
  'socket',
  function ($scope, $http, socket) {
    var types = [
        'warning',
        'info',
        'success',
        'danger'
      ];
    $scope.jira = {};
    // initialize jira blocks
    var initJiras = function () {
      angular.forEach(types, function (value, key) {
        $scope.jira[value] = {};
        $scope.jira[value].title = 'Loading...';
        $scope.jira[value].count = 'Loading...';
      });
      $http.get('jira/blocks').success(function (data) {
        if (data.length === 4) {
          angular.forEach(data, function (value, key) {
            $scope.jira[value.type] = value;
            $scope.jira[value.type].loaded = true;
          });
        } else {
          angular.forEach(types, function (value, key) {
            $scope.jira[value] = {};
            $scope.jira[value].title = 'Not defined';
            $scope.jira[value].count = 'Not defined';
            $scope.jira[value].loaded = true;
          });
          angular.forEach(data, function (value, key) {
            $scope.jira[value.type] = value;
            $scope.jira[value.type].loaded = true;
          });
        }
      });
    };
    $scope.init = function () {
      initJiras();
    };
    $scope.init();
    socket.on('config:refresh', function (data) {
      initJiras();
    });
  }
]);'use strict';
angular.module('core').controller('StreamController', [
  '$scope',
  '$http',
  'socket',
  function ($scope, $http, socket) {
    $scope.streamStarted = false;
    $scope.stream = {};
    $scope.currentStreamMax = 5;
    $scope.showContent = true;
    $scope.showLessButton = false;
    $scope.showMoreButton = false;
    $scope.streamConfig = false;
    $scope.availableProjects = [];
    $scope.myProjects = {};
    $scope.myProjects.projects = [];
    $scope.streamConfigStart = function () {
      $scope.streamConfig = !$scope.streamConfig;
    };
    $scope.streamConfigSave = function () {
      console.log($scope.showContent);
      $scope.streamConfig = false;
      $http.put('jira/stream/update', {
        projects: $scope.myProjects.projects,
        limit: $scope.currentStreamMax,
        showContent: $scope.showContent
      }).success(function (data) {
        initStream();
      });
    };
    // initialize activity stream
    var initStream = function () {
      $scope.streamStarted = false;
      $scope.showLessButton = false;
      $scope.showMoreButton = false;
      $scope.stream = [];
      $http.get('jira/stream/config').success(function (data) {
        $scope.availableProjects = data.availableprojects;
        $scope.myProjects.projects = data.myprojects;
        $scope.currentStreamMax = data.mylimit;
        $scope.showContent = data.showcontent;
        $http.get('jira/stream?max=' + $scope.currentStreamMax + '&projects=' + $scope.myProjects.projects).success(function (data) {
          $scope.stream = data;
          if ($scope.stream.length > 0) {
            $scope.showMoreButton = true;
          }
          if ($scope.stream.length > 5) {
            $scope.showLessButton = true;
          }
          $scope.streamStarted = true;
        });
      });
    };
    $scope.init = function () {
      initStream();
    };
    $scope.init();
    $scope.streamMore = function () {
      $scope.streamStarted = false;
      $scope.currentStreamMax = $scope.currentStreamMax + 5;
      $http.get('jira/stream?max=' + $scope.currentStreamMax + '&projects=' + $scope.myProjects.projects).success(function (data) {
        $scope.stream = data;
        $scope.streamStarted = true;
        $scope.showLessButton = true;
      });
    };
    $scope.streamLess = function () {
      $scope.streamStarted = false;
      $scope.currentStreamMax = $scope.currentStreamMax - 5;
      $http.get('jira/stream?max=' + $scope.currentStreamMax + '&projects=' + $scope.myProjects.projects).success(function (data) {
        $scope.stream = data;
        $scope.streamStarted = true;
        if ($scope.currentStreamMax === 5) {
          $scope.showLessButton = false;
        }
      });
    };
    socket.on('stream:refresh', function (data) {
      initStream();
    });
    socket.on('config:refresh', function (data) {
      initStream();
    });
  }
]);'use strict';
angular.module('core').controller('TodoController', [
  '$scope',
  '$http',
  'System',
  'socket',
  function ($scope, $http, System, socket) {
    $scope.conf = System.conf();
    $scope.todosStarted = false;
    $scope.todos = {};
    $scope.perPageTodos = 6;
    $scope.totalTodos = 0;
    $scope.currentTodoPage = 0;
    // initialize qa todo list
    var initTodos = function () {
      $scope.todosStarted = false;
      $scope.currentTodoPage = 0;
      $http.get('jira/todos?start=0&max=6').success(function (data) {
        $scope.todos = data.issues;
        $scope.totalTodos = data.total;
        $scope.todosStarted = true;
      });
    };
    $scope.init = function () {
      initTodos();
    };
    $scope.init();
    $scope.pageTodoChanged = function () {
      $scope.todosStarted = false;
      var start = $scope.perPageTodos * $scope.currentTodoPage - $scope.perPageTodos;
      $http.get('jira/todos?start=' + start + '&max=6').success(function (data) {
        $scope.todos = data.issues;
        $scope.todosStarted = true;
      });
    };
    socket.on('config:refresh', function (data) {
      initTodos();
    });
  }
]);'use strict';
angular.module('jira').directive('todoText', function () {
  return { template: '{{todo.fields.summary.substring(0,70)}}' };
});'use strict';
angular.module('university').controller('UniversityController', [
  '$scope',
  function ($scope) {
  }
]);'use strict';
// Config HTTP Error Handling
angular.module('users').config([
  '$httpProvider',
  function ($httpProvider) {
    // Set the httpProvider "not authorized" interceptor
    $httpProvider.interceptors.push([
      '$q',
      '$location',
      function ($q, $location) {
        return {
          responseError: function (rejection) {
            switch (rejection.status) {
            case 401:
              // Redirect to signin page
              window.location = '/';
              break;
            case 403:
              // Add unauthorized behaviour 
              break;
            }
            return $q.reject(rejection);
          }
        };
      }
    ]);
  }
]);'use strict';
// Setting up route
angular.module('users').config([
  '$stateProvider',
  function ($stateProvider) {
    // Users state routing
    $stateProvider.state('index.profile', {
      url: '/settings/profile',
      views: { 'content': { templateUrl: 'modules/users/views/settings/edit-profile.client.view.html' } }
    }).state('index.first-login', {
      url: '/settings/first-login',
      views: { 'content': { templateUrl: 'modules/users/views/settings/first-login.client.view.html' } }
    });
  }
]);'use strict';
angular.module('users').controller('AuthenticationController', [
  '$scope',
  '$http',
  '$modal',
  function ($scope, $http, $modal) {
    $scope.failedJobsTemplate = 'template/jenkins/failedjobs.html';
    $scope.signin = function () {
      $http.post('/auth/signin', $scope.credentials).success(function (response) {
        $scope.credentials = null;
        // just check if there are failing jenkins jobs if the user is a QA member
        if (!response.isQA) {
          window.location = '/';
        } else {
          $http.get('/jenkins/build/failed').success(function (data) {
            if (data.length > 0) {
              $modal.open({
                templateUrl: $scope.failedJobsTemplate,
                controller: 'FailedJobsController',
                resolve: {
                  jobs: function () {
                    return data;
                  }
                }
              });
            } else {
              window.location = '/';
            }
          });
        }
      }).error(function (response) {
        $scope.error = response;
      });
    };
  }
]).run([
  '$templateCache',
  function ($templateCache) {
    // template for comment modal
    $templateCache.put('template/jenkins/failedjobs.html', '<div class="modal-header"><h3 class="modal-title">Oh, oh!  we have failed jenkins jobs    <i class="fa fa-frown-o"></i></h3></div>' + '<div class="modal-body">' + '<p> Hey there, nice to see you again. It seems that we have some failing jenkins jobs</p>' + '<span> Please show some love <i class="fa fa-heart"></i> and see if you can help: </span>' + '<p></p>' + '<span data-ng-repeat="job in jobs"><a href="{{\xa0job.url }}" target="_blank"> {{ job.job }}</a><span>' + '</div>' + '<div class="modal-footer">' + '<button class="btn btn-primary" data-ng-click="ok()">Ok, I know</button>' + '</div>' + '</div>');
  }
]);'use strict';
angular.module('users').controller('AvatarController', [
  '$scope',
  '$http',
  '$location',
  'Authentication',
  'socket',
  function ($scope, $http, $location, Authentication, socket) {
    $scope.account = Authentication.account;
    $scope.image = 'modules/users/img/avatars/' + $scope.account.avatar + '?' + window.moment().unix();
    $scope.avatarChanged = false;
    $scope.avatars = [
      'america.png',
      'barret.png',
      'blanka.png',
      'chunli.png',
      'clone.png',
      'cloud.png',
      'colossus.png',
      'darth-vader.png',
      'default.png',
      'dhalsim.png',
      'doll.png',
      'evilred.png',
      'hellboy.png',
      'ironman.png',
      'ironman2.png',
      'johny.png',
      'joker.png',
      'logan.png',
      'magneto.png',
      'mummy.png',
      'ood.png',
      'ryu.png',
      'scream.png',
      'silver.png',
      'slasher.png',
      'spiderwoman.png',
      'superman.png',
      'thor.png',
      'tigger.png',
      'smith.png',
      'universe.png',
      'venon.png',
      'war1.png',
      'war2.png',
      'war3.png',
      'woody.png',
      'zangief.png'
    ];
    $scope.setAvatar = function (avatar) {
      $scope.image = 'modules/users/img/avatars/' + avatar;
      $scope.account.avatar = avatar;
      $scope.avatarChanged = true;
    };
    $scope.firstLogin = function () {
      // either the user has changed the avatar or he has droped a file on the drop area
      if ($scope.avatarChanged || $scope.image.includes('data:image/png;base64')) {
        var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' } };
        var data = window.$.param({ img: $scope.image });
        $http.post('/settings/firstLogin', data, config).success(function (response) {
          $scope.account.avatar = $scope.account.userName + '.png';
          socket.emit('user:avatarUpdate', { user: $scope.account });
          $scope.account.firstLogin = false;
        }).error(function (response) {
          $scope.error = response;
        });
      } else {
        $scope.account.firstLogin = false;
      }
    };
  }
]);'use strict';
angular.module('users').controller('FailedJobsController', [
  '$scope',
  'jobs',
  function ($scope, jobs) {
    $scope.jobs = jobs;
    $scope.ok = function () {
      window.location = '/';
    };
  }
]);'use strict';
angular.module('users').directive('fileDropzone', function () {
  return {
    require: '^?form',
    restrict: 'A',
    scope: {
      file: '=',
      fileName: '=',
      dropzoneHoverClass: '@',
      avatarChanged: '='
    },
    link: function (scope, element, attrs, form) {
      var checkSize, getDataTransfer, isTypeValid, processDragOverOrEnter, validMimeTypes;
      getDataTransfer = function (event) {
        var dataTransfer;
        dataTransfer = event.dataTransfer || event.originalEvent.dataTransfer;
        return dataTransfer;
      };
      processDragOverOrEnter = function (event) {
        if (event) {
          element.addClass(scope.dropzoneHoverClass);
          if (event.preventDefault) {
            event.preventDefault();
          }
          if (event.stopPropagation) {
            return false;
          }
        }
        getDataTransfer(event).effectAllowed = 'copy';
        return false;
      };
      validMimeTypes = attrs.fileDropzone;
      checkSize = function (size) {
        var _ref;
        if ((_ref = attrs.maxFileSize) === void 0 || _ref === '' || size / 1024 / 1024 < attrs.maxFileSize) {
          return true;
        } else {
          alert('File must be smaller than ' + attrs.maxFileSize + ' MB');
          return false;
        }
      };
      isTypeValid = function (type) {
        if (validMimeTypes === void 0 || validMimeTypes === '' || validMimeTypes.indexOf(type) > -1) {
          return true;
        } else {
          alert('Invalid file type.  File must be one of following types ' + validMimeTypes);
          return false;
        }
      };
      element.bind('dragover', processDragOverOrEnter);
      element.bind('dragenter', processDragOverOrEnter);
      element.bind('dragleave', function () {
        return element.removeClass(scope.dropzoneHoverClass);
      });
      return element.bind('drop', function (event) {
        var file, name, reader, size, type;
        if (event !== null) {
          event.preventDefault();
        }
        element.removeClass(scope.dropzoneHoverClass);
        reader = new FileReader();
        reader.onload = function (evt) {
          if (checkSize(size) && isTypeValid(type)) {
            scope.$apply(function () {
              scope.file = evt.target.result;
              scope.avatarChanged = true;
              if (angular.isString(scope.fileName)) {
                scope.fileName = name;
                return scope.fileName;
              }
            });
            if (form) {
              form.$setDirty();
            }
            return scope.$emit('file-dropzone-drop-event', {
              file: scope.file,
              type: type,
              name: name,
              size: size
            });
          }
        };
        file = getDataTransfer(event).files[0];
        name = file.name;
        type = file.type;
        size = file.size;
        reader.readAsDataURL(file);
        return false;
      });
    }
  };
});'use strict';
// Authentication service for user variables
angular.module('users').factory('Authentication', [function () {
    var _this = this;
    if (typeof _this._data === 'undefined') {
      _this._data = { account: window.account };
      window.account = '';  // erase user data to avoid manipulations
    }
    return _this._data;
  }]);'use strict';
// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', [
  '$resource',
  function ($resource) {
    return $resource('users', {}, { update: { method: 'PUT' } });
  }
]);/** Global variables. If you change any of these vars, don't forget 
 * to change the values in the less files!
 */
var left_side_width = 220;
// Sidebar width in pixels
$(function () {
  'use strict';
  // Add hover support for touch devices
  $('.btn').bind('touchstart', function () {
    $(this).addClass('hover');
  }).bind('touchend', function () {
    $(this).removeClass('hover');
  });
  // Activate tooltips
  $('[data-toggle=\'tooltip\']').tooltip();
  /*
     * Add collapse and remove events to boxes
     */
  $('[data-widget=\'collapse\']').click(function () {
    // Find the box parent
    var box = $(this).parents('.box').first();
    // Find the body and the footer
    var bf = box.find('.box-body, .box-footer');
    if (!box.hasClass('collapsed-box')) {
      box.addClass('collapsed-box');
      // Convert minus into plus
      $(this).children('.fa-minus').removeClass('fa-minus').addClass('fa-plus');
      bf.slideUp();
    } else {
      box.removeClass('collapsed-box');
      // Convert plus into minus
      $(this).children('.fa-plus').removeClass('fa-plus').addClass('fa-minus');
      bf.slideDown();
    }
  });
  /*
     * INITIALIZE BUTTON TOGGLE ------------------------
     */
  $('.btn-group[data-toggle="btn-toggle"]').each(function () {
    var group = $(this);
    $(this).find('.btn').click(function (e) {
      group.find('.btn.active').removeClass('active');
      $(this).addClass('active');
      e.preventDefault();
    });
  });
  $('[data-widget=\'remove\']').click(function () {
    // Find the box parent
    var box = $(this).parents('.box').first();
    box.slideUp();
  });
  /*
     * Make sure that the sidebar is streched full height
     * --------------------------------------------- We are gonna assign a
     * min-height value every time the wrapper gets resized and upon page load.
     * We will use Ben Alman's method for detecting the resize event.
     * 
     */
  function _fix() {
    // Get window height and the wrapper height
    var height = $(window).height() - $('body > .header').height() - ($('body > .footer').outerHeight() || 0);
    $('.wrapper').css('min-height', height + 'px');
    var content = $('.wrapper').height();
    // If the wrapper height is greater than the window
    if (content > height)
      // then set sidebar height to the wrapper
      $('.left-side, html, body').css('min-height', content + 'px');
    else {
      // Otherwise, set the sidebar to the height of the window
      $('.left-side, html, body').css('min-height', height + 'px');
    }
  }
  // Fire upon load
  _fix();
  // Fire when wrapper is resized
  $('.wrapper').resize(function () {
    _fix();
    fix_sidebar();
  });
  // Fix the fixed layout sidebar scroll bug
  fix_sidebar();  /*
     * We are gonna initialize all checkbox and radio inputs to iCheck plugin
     * in. You can find the documentation at http://fronteed.com/iCheck/
     */
                  /*$("input[type='checkbox']:not(.simple), input[type='radio']:not(.simple)").iCheck({
        checkboxClass: 'icheckbox_minimal',
        radioClass: 'iradio_minimal'
    });*/
});
function fix_sidebar() {
  // Make sure the body tag has the .fixed class
  if (!$('body').hasClass('fixed')) {
    return;
  }
  // Add slimscroll
  $('.sidebar').slimscroll({
    height: $(window).height() - $('.header').height() + 'px',
    color: 'rgba(0,0,0,0.2)'
  });
}
/* END DEMO */
$(window).load(function () {
  /* ! pace 0.4.17 */
  (function () {
    var a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V = [].slice, W = {}.hasOwnProperty, X = function (a, b) {
        function c() {
          this.constructor = a;
        }
        for (var d in b)
          W.call(b, d) && (a[d] = b[d]);
        return c.prototype = b.prototype, a.prototype = new c(), a.__super__ = b.prototype, a;
      }, Y = [].indexOf || function (a) {
        for (var b = 0, c = this.length; c > b; b++)
          if (b in this && this[b] === a)
            return b;
        return -1;
      };
    for (t = {
        catchupTime: 500,
        initialRate: 0.03,
        minTime: 500,
        ghostTime: 500,
        maxProgressPerFrame: 10,
        easeFactor: 1.25,
        startOnPageLoad: !0,
        restartOnPushState: !0,
        restartOnRequestAfter: 500,
        target: 'body',
        elements: {
          checkInterval: 100,
          selectors: ['body']
        },
        eventLag: {
          minSamples: 10,
          sampleCount: 3,
          lagThreshold: 3
        },
        ajax: {
          trackMethods: ['GET'],
          trackWebSockets: !1
        }
      }, B = function () {
        var a;
        return null != (a = 'undefined' != typeof performance && null !== performance ? 'function' == typeof performance.now ? performance.now() : void 0 : void 0) ? a : +new Date();
      }, D = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame, s = window.cancelAnimationFrame || window.mozCancelAnimationFrame, null == D && (D = function (a) {
        return setTimeout(a, 50);
      }, s = function (a) {
        return clearTimeout(a);
      }), F = function (a) {
        var b, c;
        return b = B(), (c = function () {
          var d;
          return d = B() - b, d >= 33 ? (b = B(), a(d, function () {
            return D(c);
          })) : setTimeout(c, 33 - d);
        })();
      }, E = function () {
        var a, b, c;
        return c = arguments[0], b = arguments[1], a = 3 <= arguments.length ? V.call(arguments, 2) : [], 'function' == typeof c[b] ? c[b].apply(c, a) : c[b];
      }, u = function () {
        var a, b, c, d, e, f, g;
        for (b = arguments[0], d = 2 <= arguments.length ? V.call(arguments, 1) : [], f = 0, g = d.length; g > f; f++)
          if (c = d[f])
            for (a in c)
              W.call(c, a) && (e = c[a], null != b[a] && 'object' == typeof b[a] && null != e && 'object' == typeof e ? u(b[a], e) : b[a] = e);
        return b;
      }, p = function (a) {
        var b, c, d, e, f;
        for (c = b = 0, e = 0, f = a.length; f > e; e++)
          d = a[e], c += Math.abs(d), b++;
        return c / b;
      }, w = function (a, b) {
        var c, d, e;
        if (null == a && (a = 'options'), null == b && (b = !0), e = document.querySelector('[data-pace-' + a + ']')) {
          if (c = e.getAttribute('data-pace-' + a), !b)
            return c;
          try {
            return JSON.parse(c);
          } catch (f) {
            return d = f, 'undefined' != typeof console && null !== console ? console.error('Error parsing inline pace options', d) : void 0;
          }
        }
      }, g = function () {
        function a() {
        }
        return a.prototype.on = function (a, b, c, d) {
          var e;
          return null == d && (d = !1), null == this.bindings && (this.bindings = {}), null == (e = this.bindings)[a] && (e[a] = []), this.bindings[a].push({
            handler: b,
            ctx: c,
            once: d
          });
        }, a.prototype.once = function (a, b, c) {
          return this.on(a, b, c, !0);
        }, a.prototype.off = function (a, b) {
          var c, d, e;
          if (null != (null != (d = this.bindings) ? d[a] : void 0)) {
            if (null == b)
              return delete this.bindings[a];
            for (c = 0, e = []; c < this.bindings[a].length;)
              this.bindings[a][c].handler === b ? e.push(this.bindings[a].splice(c, 1)) : e.push(c++);
            return e;
          }
        }, a.prototype.trigger = function () {
          var a, b, c, d, e, f, g, h, i;
          if (c = arguments[0], a = 2 <= arguments.length ? V.call(arguments, 1) : [], null != (g = this.bindings) ? g[c] : void 0) {
            for (e = 0, i = []; e < this.bindings[c].length;)
              h = this.bindings[c][e], d = h.handler, b = h.ctx, f = h.once, d.apply(null != b ? b : this, a), f ? i.push(this.bindings[c].splice(e, 1)) : i.push(e++);
            return i;
          }
        }, a;
      }(), null == window.Pace && (window.Pace = {}), u(Pace, g.prototype), C = Pace.options = u({}, t, window.paceOptions, w()), S = [
        'ajax',
        'document',
        'eventLag',
        'elements'
      ], O = 0, Q = S.length; Q > O; O++)
      I = S[O], C[I] === !0 && (C[I] = t[I]);
    i = function (a) {
      function b() {
        return T = b.__super__.constructor.apply(this, arguments);
      }
      return X(b, a), b;
    }(Error), b = function () {
      function a() {
        this.progress = 0;
      }
      return a.prototype.getElement = function () {
        var a;
        if (null == this.el) {
          if (a = document.querySelector(C.target), !a)
            throw new i();
          this.el = document.createElement('div'), this.el.className = 'pace pace-active', document.body.className = document.body.className.replace('pace-done', ''), document.body.className += ' pace-running', this.el.innerHTML = '<div class="pace-progress">\n  <div class="pace-progress-inner"></div>\n</div>\n<div class="pace-activity"></div>', null != a.firstChild ? a.insertBefore(this.el, a.firstChild) : a.appendChild(this.el);
        }
        return this.el;
      }, a.prototype.finish = function () {
        var a;
        return a = this.getElement(), a.className = a.className.replace('pace-active', ''), a.className += ' pace-inactive', document.body.className = document.body.className.replace('pace-running', ''), document.body.className += ' pace-done';
      }, a.prototype.update = function (a) {
        return this.progress = a, this.render();
      }, a.prototype.destroy = function () {
        try {
          this.getElement().parentNode.removeChild(this.getElement());
        } catch (a) {
          i = a;
        }
        return this.el = void 0;
      }, a.prototype.render = function () {
        var a, b;
        return null == document.querySelector(C.target) ? !1 : (a = this.getElement(), a.children[0].style.width = '' + this.progress + '%', (!this.lastRenderedProgress || this.lastRenderedProgress | 0 !== this.progress | 0) && (a.children[0].setAttribute('data-progress-text', '' + (0 | this.progress) + '%'), this.progress >= 100 ? b = '99' : (b = this.progress < 10 ? '0' : '', b += 0 | this.progress), a.children[0].setAttribute('data-progress', '' + b)), this.lastRenderedProgress = this.progress);
      }, a.prototype.done = function () {
        return this.progress >= 100;
      }, a;
    }(), h = function () {
      function a() {
        this.bindings = {};
      }
      return a.prototype.trigger = function (a, b) {
        var c, d, e, f, g;
        if (null != this.bindings[a]) {
          for (f = this.bindings[a], g = [], d = 0, e = f.length; e > d; d++)
            c = f[d], g.push(c.call(this, b));
          return g;
        }
      }, a.prototype.on = function (a, b) {
        var c;
        return null == (c = this.bindings)[a] && (c[a] = []), this.bindings[a].push(b);
      }, a;
    }(), N = window.XMLHttpRequest, M = window.XDomainRequest, L = window.WebSocket, v = function (a, b) {
      var c, d, e, f;
      f = [];
      for (d in b.prototype)
        try {
          e = b.prototype[d], null == a[d] && 'function' != typeof e ? f.push(a[d] = e) : f.push(void 0);
        } catch (g) {
          c = g;
        }
      return f;
    }, z = [], Pace.ignore = function () {
      var a, b, c;
      return b = arguments[0], a = 2 <= arguments.length ? V.call(arguments, 1) : [], z.unshift('ignore'), c = b.apply(null, a), z.shift(), c;
    }, Pace.track = function () {
      var a, b, c;
      return b = arguments[0], a = 2 <= arguments.length ? V.call(arguments, 1) : [], z.unshift('track'), c = b.apply(null, a), z.shift(), c;
    }, H = function (a) {
      var b;
      if (null == a && (a = 'GET'), 'track' === z[0])
        return 'force';
      if (!z.length && C.ajax) {
        if ('socket' === a && C.ajax.trackWebSockets)
          return !0;
        if (b = a.toUpperCase(), Y.call(C.ajax.trackMethods, b) >= 0)
          return !0;
      }
      return !1;
    }, j = function (a) {
      function b() {
        var a, c = this;
        b.__super__.constructor.apply(this, arguments), a = function (a) {
          var b;
          return b = a.open, a.open = function (d, e) {
            return H(d) && c.trigger('request', {
              type: d,
              url: e,
              request: a
            }), b.apply(a, arguments);
          };
        }, window.XMLHttpRequest = function (b) {
          var c;
          return c = new N(b), a(c), c;
        }, v(window.XMLHttpRequest, N), null != M && (window.XDomainRequest = function () {
          var b;
          return b = new M(), a(b), b;
        }, v(window.XDomainRequest, M)), null != L && C.ajax.trackWebSockets && (window.WebSocket = function (a, b) {
          var d;
          return d = new L(a, b), H('socket') && c.trigger('request', {
            type: 'socket',
            url: a,
            protocols: b,
            request: d
          }), d;
        }, v(window.WebSocket, L));
      }
      return X(b, a), b;
    }(h), P = null, x = function () {
      return null == P && (P = new j()), P;
    }, x().on('request', function (b) {
      var c, d, e, f;
      return f = b.type, e = b.request, Pace.running || C.restartOnRequestAfter === !1 && 'force' !== H(f) ? void 0 : (d = arguments, c = C.restartOnRequestAfter || 0, 'boolean' == typeof c && (c = 0), setTimeout(function () {
        var b, c, g, h, i, j;
        if (b = 'socket' === f ? e.readyState < 2 : 0 < (h = e.readyState) && 4 > h) {
          for (Pace.restart(), i = Pace.sources, j = [], c = 0, g = i.length; g > c; c++) {
            if (I = i[c], I instanceof a) {
              I.watch.apply(I, d);
              break;
            }
            j.push(void 0);
          }
          return j;
        }
      }, c));
    }), a = function () {
      function a() {
        var a = this;
        this.elements = [], x().on('request', function () {
          return a.watch.apply(a, arguments);
        });
      }
      return a.prototype.watch = function (a) {
        var b, c, d;
        return d = a.type, b = a.request, c = 'socket' === d ? new m(b) : new n(b), this.elements.push(c);
      }, a;
    }(), n = function () {
      function a(a) {
        var b, c, d, e, f, g, h = this;
        if (this.progress = 0, null != window.ProgressEvent)
          for (c = null, a.addEventListener('progress', function (a) {
              return h.progress = a.lengthComputable ? 100 * a.loaded / a.total : h.progress + (100 - h.progress) / 2;
            }), g = [
              'load',
              'abort',
              'timeout',
              'error'
            ], d = 0, e = g.length; e > d; d++)
            b = g[d], a.addEventListener(b, function () {
              return h.progress = 100;
            });
        else
          f = a.onreadystatechange, a.onreadystatechange = function () {
            var b;
            return 0 === (b = a.readyState) || 4 === b ? h.progress = 100 : 3 === a.readyState && (h.progress = 50), 'function' == typeof f ? f.apply(null, arguments) : void 0;
          };
      }
      return a;
    }(), m = function () {
      function a(a) {
        var b, c, d, e, f = this;
        for (this.progress = 0, e = [
            'error',
            'open'
          ], c = 0, d = e.length; d > c; c++)
          b = e[c], a.addEventListener(b, function () {
            return f.progress = 100;
          });
      }
      return a;
    }(), d = function () {
      function a(a) {
        var b, c, d, f;
        for (null == a && (a = {}), this.elements = [], null == a.selectors && (a.selectors = []), f = a.selectors, c = 0, d = f.length; d > c; c++)
          b = f[c], this.elements.push(new e(b));
      }
      return a;
    }(), e = function () {
      function a(a) {
        this.selector = a, this.progress = 0, this.check();
      }
      return a.prototype.check = function () {
        var a = this;
        return document.querySelector(this.selector) ? this.done() : setTimeout(function () {
          return a.check();
        }, C.elements.checkInterval);
      }, a.prototype.done = function () {
        return this.progress = 100;
      }, a;
    }(), c = function () {
      function a() {
        var a, b, c = this;
        this.progress = null != (b = this.states[document.readyState]) ? b : 100, a = document.onreadystatechange, document.onreadystatechange = function () {
          return null != c.states[document.readyState] && (c.progress = c.states[document.readyState]), 'function' == typeof a ? a.apply(null, arguments) : void 0;
        };
      }
      return a.prototype.states = {
        loading: 0,
        interactive: 50,
        complete: 100
      }, a;
    }(), f = function () {
      function a() {
        var a, b, c, d, e, f = this;
        this.progress = 0, a = 0, e = [], d = 0, c = B(), b = setInterval(function () {
          var g;
          return g = B() - c - 50, c = B(), e.push(g), e.length > C.eventLag.sampleCount && e.shift(), a = p(e), ++d >= C.eventLag.minSamples && a < C.eventLag.lagThreshold ? (f.progress = 100, clearInterval(b)) : f.progress = 100 * (3 / (a + 3));
        }, 50);
      }
      return a;
    }(), l = function () {
      function a(a) {
        this.source = a, this.last = this.sinceLastUpdate = 0, this.rate = C.initialRate, this.catchup = 0, this.progress = this.lastProgress = 0, null != this.source && (this.progress = E(this.source, 'progress'));
      }
      return a.prototype.tick = function (a, b) {
        var c;
        return null == b && (b = E(this.source, 'progress')), b >= 100 && (this.done = !0), b === this.last ? this.sinceLastUpdate += a : (this.sinceLastUpdate && (this.rate = (b - this.last) / this.sinceLastUpdate), this.catchup = (b - this.progress) / C.catchupTime, this.sinceLastUpdate = 0, this.last = b), b > this.progress && (this.progress += this.catchup * a), c = 1 - Math.pow(this.progress / 100, C.easeFactor), this.progress += c * this.rate * a, this.progress = Math.min(this.lastProgress + C.maxProgressPerFrame, this.progress), this.progress = Math.max(0, this.progress), this.progress = Math.min(100, this.progress), this.lastProgress = this.progress, this.progress;
      }, a;
    }(), J = null, G = null, q = null, K = null, o = null, r = null, Pace.running = !1, y = function () {
      return C.restartOnPushState ? Pace.restart() : void 0;
    }, null != window.history.pushState && (R = window.history.pushState, window.history.pushState = function () {
      return y(), R.apply(window.history, arguments);
    }), null != window.history.replaceState && (U = window.history.replaceState, window.history.replaceState = function () {
      return y(), U.apply(window.history, arguments);
    }), k = {
      ajax: a,
      elements: d,
      document: c,
      eventLag: f
    }, (A = function () {
      var a, c, d, e, f, g, h, i;
      for (Pace.sources = J = [], g = [
          'ajax',
          'elements',
          'document',
          'eventLag'
        ], c = 0, e = g.length; e > c; c++)
        a = g[c], C[a] !== !1 && J.push(new k[a](C[a]));
      for (i = null != (h = C.extraSources) ? h : [], d = 0, f = i.length; f > d; d++)
        I = i[d], J.push(new I(C));
      return Pace.bar = q = new b(), G = [], K = new l();
    })(), Pace.stop = function () {
      return Pace.trigger('stop'), Pace.running = !1, q.destroy(), r = !0, null != o && ('function' == typeof s && s(o), o = null), A();
    }, Pace.restart = function () {
      return Pace.trigger('restart'), Pace.stop(), Pace.start();
    }, Pace.go = function () {
      return Pace.running = !0, q.render(), r = !1, o = F(function (a, b) {
        var c, d, e, f, g, h, i, j, k, m, n, o, p, s, t, u, v;
        for (j = 100 - q.progress, d = o = 0, e = !0, h = p = 0, t = J.length; t > p; h = ++p)
          for (I = J[h], m = null != G[h] ? G[h] : G[h] = [], g = null != (v = I.elements) ? v : [I], i = s = 0, u = g.length; u > s; i = ++s)
            f = g[i], k = null != m[i] ? m[i] : m[i] = new l(f), e &= k.done, k.done || (d++, o += k.tick(a));
        return c = o / d, q.update(K.tick(a, c)), n = B(), q.done() || e || r ? (q.update(100), Pace.trigger('done'), setTimeout(function () {
          return q.finish(), Pace.running = !1, Pace.trigger('hide');
        }, Math.max(C.ghostTime, Math.min(C.minTime, B() - n)))) : b();
      });
    }, Pace.start = function (a) {
      u(C, a), Pace.running = !0;
      try {
        q.render();
      } catch (b) {
        i = b;
      }
      return document.querySelector('.pace') ? (Pace.trigger('start'), Pace.go()) : setTimeout(Pace.start, 50);
    }, 'function' == typeof define && define.amd ? define('theme-app', [], function () {
      return Pace;
    }) : 'object' == typeof exports ? module.exports = Pace : C.startOnPageLoad && Pace.start();
  }.call(this));
});
/*
 * BOX REFRESH BUTTON ------------------ This is a custom plugin to use with the
 * compenet BOX. It allows you to add a refresh button to the box. It converts
 * the box's state to a loading state.
 * 
 * USAGE: $("#box-widget").boxRefresh( options );
 */
(function ($) {
  'use strict';
  $.fn.boxRefresh = function (options) {
    // Render options
    var settings = $.extend({
        trigger: '.refresh-btn',
        source: '',
        onLoadStart: function (box) {
        },
        onLoadDone: function (box) {
        }  // When the source has been loaded
      }, options);
    // The overlay
    var overlay = $('<div class="overlay"></div><div class="loading-img"></div>');
    return this.each(function () {
      // if a source is specified
      if (settings.source === '') {
        if (console) {
          console.log('Please specify a source first - boxRefresh()');
        }
        return;
      }
      // the box
      var box = $(this);
      // the button
      var rBtn = box.find(settings.trigger).first();
      // On trigger click
      rBtn.click(function (e) {
        e.preventDefault();
        // Add loading overlay
        start(box);
        // Perform ajax call
        box.find('.box-body').load(settings.source, function () {
          done(box);
        });
      });
    });
    function start(box) {
      // Add overlay and loading img
      box.append(overlay);
      settings.onLoadStart.call(box);
    }
    function done(box) {
      // Remove overlay and loading img
      box.find(overlay).remove();
      settings.onLoadDone.call(box);
    }
  };
}(jQuery));
/*
 * TODO LIST CUSTOM PLUGIN ----------------------- This plugin depends on iCheck
 * plugin for checkbox and radio inputs
 */
(function ($) {
  'use strict';
  $.fn.todolist = function (options) {
    // Render options
    var settings = $.extend({
        onCheck: function (ele) {
        },
        onUncheck: function (ele) {
        }
      }, options);
    return this.each(function () {
      $('input', this).on('ifChecked', function (event) {
        var ele = $(this).parents('li').first();
        ele.toggleClass('done');
        settings.onCheck.call(ele);
      });
      $('input', this).on('ifUnchecked', function (event) {
        var ele = $(this).parents('li').first();
        ele.toggleClass('done');
        settings.onUncheck.call(ele);
      });
    });
  };
}(jQuery));
/* CENTER ELEMENTS */
(function ($) {
  'use strict';
  jQuery.fn.center = function (parent) {
    if (parent) {
      parent = this.parent();
    } else {
      parent = window;
    }
    this.css({
      'position': 'absolute',
      'top': ($(parent).height() - this.outerHeight()) / 2 + $(parent).scrollTop() + 'px',
      'left': ($(parent).width() - this.outerWidth()) / 2 + $(parent).scrollLeft() + 'px'
    });
    return this;
  };
}(jQuery));
/*
 * jQuery resize event - v1.1 - 3/14/2010
 * http://benalman.com/projects/jquery-resize-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman Dual licensed under the MIT and GPL
 * licenses. http://benalman.com/about/license/
 */
(function ($, h, c) {
  var a = $([]), e = $.resize = $.extend($.resize, {}), i, k = 'setTimeout', j = 'resize', d = j + '-special-event', b = 'delay', f = 'throttleWindow';
  e[b] = 250;
  e[f] = true;
  $.event.special[j] = {
    setup: function () {
      if (!e[f] && this[k]) {
        return false;
      }
      var l = $(this);
      a = a.add(l);
      $.data(this, d, {
        w: l.width(),
        h: l.height()
      });
      if (a.length === 1) {
        g();
      }
    },
    teardown: function () {
      if (!e[f] && this[k]) {
        return false;
      }
      var l = $(this);
      a = a.not(l);
      l.removeData(d);
      if (!a.length) {
        clearTimeout(i);
      }
    },
    add: function (l) {
      if (!e[f] && this[k]) {
        return false;
      }
      var n;
      function m(s, o, p) {
        var q = $(this), r = $.data(this, d);
        r.w = o !== c ? o : q.width();
        r.h = p !== c ? p : q.height();
        n.apply(this, arguments);
      }
      if ($.isFunction(l)) {
        n = l;
        return m;
      } else {
        n = l.handler;
        l.handler = m;
      }
    }
  };
  function g() {
    i = h[k](function () {
      a.each(function () {
        var n = $(this), m = n.width(), l = n.height(), o = $.data(this, d);
        if (m !== o.w || l !== o.h) {
          n.trigger(j, [
            o.w = m,
            o.h = l
          ]);
        }
      });
      g();
    }, e[b]);
  }
}(jQuery, this));