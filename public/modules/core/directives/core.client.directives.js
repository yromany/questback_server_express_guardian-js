'use strict';

angular.module('core').directive('sidebarToggle', ['$http', function ($http) {
    return {
        link: function (scope, element, attrs) {
            // Enable sidebar toggle
            element.click(function (e) {
                e.preventDefault();
                // If window is small enough, enable sidebar push menu
                if (angular.element(window).width() <= 992) {
                    angular.element('.row-offcanvas').toggleClass('active');
                    angular.element('.left-side').removeClass('collapse-left');
                    angular.element('.right-side').removeClass('strech');
                    angular.element('.row-offcanvas').toggleClass('relative');
                } else {
                    angular.element('.left-side').toggleClass('collapse-left');
                    angular.element('.right-side').toggleClass('strech');
                }
            });
        }
    };
}]).directive('slimScroll', function () {
    return {
        link: function (scope, element, attrs) {
            element.slimscroll({
                height: attrs.h,
                alwaysVisible: false,
                size: attrs.s
            }).css('width', '100%');
        }
    };
}).directive('sidebarTree', function () {
    return {
        link: function (scope, element, attrs) {

            var btn = element.children('a').first();
            var menu = element.children('.treeview-menu').first();
            var isActive = element.hasClass('active');

            // initialize already active menus
            if (isActive) {
                menu.show();
                btn.children('.fa-angle-left').first().removeClass('fa-angle-left').addClass('fa-angle-down');
            }

            // Slide open or close the menu on link click
            btn.click(function (e) {
                e.preventDefault();
                if (isActive) {
                    // Slide up to close menu
                    menu.slideUp('fast');
                    isActive = false;
                    btn.children('.fa-angle-down').first().removeClass('fa-angle-down').addClass('fa-angle-left');
                    btn.parent('li').removeClass('active');
                } else {
                    // Slide down to open menu
                    menu.slideDown('fast');
                    isActive = true;
                    btn.children('.fa-angle-left').first().removeClass('fa-angle-left').addClass('fa-angle-down');
                    btn.parent('li').addClass('active');
                }
            });

            /* Add margins to submenu elements to give it a tree look */
            menu.find('li > a').each(function () {
                var pad = parseInt(window.jQuery(this).css('margin-left')) + 10;
                window.jQuery(this).css({'margin-left': pad + 'px'});
            });

        }
    };
});
