'use strict';

angular.module('core').controller('SideBarController', ['$scope', '$location', 'socket', 'Authentication', 'System', '$splash', 'growl', '$modal', '$http',
    function ($scope, $location, socket, Authentication, System, $splash, growl, $modal, $http) {

        $scope.account = Authentication.account;

        $scope.maintenanceTemplate = 'template/configuration/maintenance.html';

        $scope.avatarLastState = window.moment().unix();

        socket.on('user:init', function (data) {
            $scope.user = data.user;
            $scope.users = data.users;
        });

        socket.on('user:refresh', function (data) {
            $scope.users = data.users;
        });

        socket.on('user:left', function (data) {
            $scope.users = data.users;
        });

        socket.on('config:refresh', function (data) {
            System.setConf(data.conf);
        });

        $scope.toggleStatus = function (user) {
            socket.emit('user:togglestatus', {
                user: user
            });
        };

        socket.on('user:update', function (data) {
            $scope.user = data.user;
            $scope.users = data.users;
        });

        socket.on('user:avatarUpdate', function () {
            $scope.avatarLastState = window.moment().unix();
        });

        $scope.changeAvatar = function () {
            $splash.open({}, {
                templateUrl: 'modules/users/views/settings/first-login.client.view.html'
            });
        };

        socket.on('artifact:refresh', function (data) {
            growl.info('Artifact <strong>' + data.name + '</strong> has been checked in.', {});
        });

        var openMaintenanceModal = function (message) {
            $modal.open({
                templateUrl: $scope.maintenanceTemplate,
                controller: 'MaintenanceController',
                size: 'lg',
                resolve: {
                    message: function () {
                        return message;
                    }
                }
            });
        };

        if (!$scope.account.announcementRead) {
            $http.get('/maintenance/message').success(function (data) {
                openMaintenanceModal(data.message);
            });
        }

        socket.on('maintenance:new', function (data) {
            openMaintenanceModal(data.message);
        });

        socket.on('jenkins:failed', function (data) {
            growl.error('Job <strong> <a href="' + data.url + '" target="_blank">' + data.job + '</strong></a> has failed.', {ttl: -1});
        });

        socket.on('jenkins:fixed', function (data) {
            growl.success('Job <strong> <a href="' + data.url + ' " target="_blank">' + data.job + '</strong></a> is fixed.', {ttl: -1});
        });

    }
]).run(['$templateCache', function ($templateCache) { // template for maintenance message
    $templateCache.put('template/configuration/maintenance.html',
        '<div class="modal-header"><h3 class="modal-title">Maintenance Announcement</h3></div>' +
        '<div class="modal-body">' +
        '<pre>{{ message }}</pre>' +
        '</div>' +
        '<div class="modal-footer">' +
        '<button class="btn btn-primary" data-ng-click="ok()">Ok, understood!</button>' +
        '</div>' +
        '</div>');
}]);
