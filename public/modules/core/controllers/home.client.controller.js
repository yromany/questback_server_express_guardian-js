'use strict';

angular.module('core').controller('HomeController', ['$scope', 'Authentication', '$splash', '$http',
    function ($scope, Authentication, $splash, $http) {

        $scope.account = Authentication.account;
        $scope.widgets = [];
        $scope.widgetSections = $scope.account.widgetSections;
        $scope.layoutConfig = false;

        // user widgets positioning
        $scope.widgets = $scope.account.widgets; // widgets positioning is a user specific setting

        $scope.init = function () {
            if ($scope.widgetSections > 2) {
                $scope.widgetsClasses = ['col-lg-6','col-lg-3','col-lg-3'];
                $scope.widgetsLayout = 'layout-aaa';
            } else {
                $scope.widgetsClasses = ['col-lg-7','col-lg-5'];
                $scope.widgetsLayout = 'layout-ab';
            }
        };

        $scope.init();

        // widgets options
        $scope.widgetOptions = {
            placeholder: 'sort-highlight',
            connectWith: '.connectedSortable',
            forcePlaceholderSize: true,
            zIndex: 999999,
            // update widget settings on update
            update: function (e, ui) {
                $http.put('settings/widgets', $scope.widgets).success(function(data) {
                });
            }
        };

        // first login handler
        if ($scope.account.firstLogin === true) {
            $splash.open({},{
                templateUrl: 'modules/users/views/settings/first-login.client.view.html',
                windowTemplateUrl: 'splash/index.html'
            });
        }

        $scope.layout = function () {
            $scope.layoutConfig = true;
        };

        $scope.changeLayout = function (layout) {
            if (layout !== $scope.widgetsLayout) {
                if (layout === 'layout-ab') {
                    angular.forEach($scope.widgets[2], function(ob) {
                        $scope.widgets[1].unshift(ob);
                    });
                    $scope.widgets[2] = [];
                    $scope.widgetSections = 2;

                } else {
                    $scope.widgetSections = 3;
                }
                var widgetSections = { 'sections' : $scope.widgetSections};
                $http.put('settings/widgets/sections', widgetSections).success(function(data) {
                    $http.put('settings/widgets', $scope.widgets).success(function(data) {
                        window.location.reload();
                    });
                });
            }

            $scope.layoutConfig = false;

        };


    }
]);
