'use strict';

angular.module('core').controller('HeaderController', ['$scope', 'Authentication', '$http', 'System',
    function ($scope, Authentication, $http, System) {
        $scope.conf = System.conf();
        $scope.account = Authentication.account;

        var initJiras = function () {
            $http.get('jira/tasks').success(function (data) {
                $scope.tasks = data;
            });
        };

        $scope.init = function () {
            initJiras();
        };

        $scope.init();

    }
]);
