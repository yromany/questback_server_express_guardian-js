'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        // Redirect to home view when route not found
        $urlRouterProvider.otherwise('/home');

        // Home state routing
        $stateProvider.state('index', {
            views: {
                'header': {
                    templateUrl: 'modules/core/views/header.client.view.html'
                },
                'wrapper': {
                    templateUrl: 'modules/core/views/wrapper.client.view.html'
                }
            }
        }).state('index.home', {
            url: '/home',
            views: {
                'content': {
                    templateUrl: 'modules/core/views/home.client.view.html'
                },
                // widgets
                'chat@content': {
                    templateUrl: 'modules/chat/views/chat.client.view.html',
                    controller: 'ChatController'
                },
                'todo@content': {
                    templateUrl: 'modules/jira/views/todo.client.view.html',
                    controller: 'TodoController'
                },
                'stream@content': {
                    templateUrl: 'modules/jira/views/stream.client.view.html',
                    controller: 'StreamController'
                },
                'webdriver@content': {
                    templateUrl: 'modules/jenkins/views/webdriver.client.view.html',
                    controller: 'TestReportController'
                },
                'build@content': {
                    templateUrl: 'modules/jenkins/views/build.client.view.html',
                    controller: 'BuildController'
                }
            }
        }).state('index.features', {
            resolve: {
                features: function ($http) {
                    return $http.get('/features');
                }
            },
            url: '/features',
            views: {
                'content': {
                    templateUrl: 'modules/feature/views/feature.client.view.html',
                    controller: function ($scope, features, Editor) {
                        Editor.init();
                        $scope.filteredSearch = [features.data];
                        $scope.features = [features.data];
                        $scope.featureLoaded = true;
                        $scope.tree_loading = false;
                    }
                }
            }
        }).state('index.university', {
            url: '/university',
            views: {
                'content': {
                    templateUrl: 'modules/university/views/university.client.view.html'
                }
            }
        }).state('index.jira', {
            url: '/jira',
            views: {
                'content': {
                    templateUrl: 'modules/jira/views/boxes.client.view.html'
                }
            }
        }).state('index.config', {
            resolve: {
                hosts: function ($http) {
                    return $http.get('/configuration/hosts');
                },
                users: function ($http) {
                    return $http.get('/configuration/users');
                }
            },
            url: '/config',
            views: {
                'content': {
                    templateUrl: 'modules/configuration/views/configuration.client.view.html',
                    controller: function ($scope, hosts, users) {
                        $scope.hosts = hosts.data;
                        $scope.users = users.data;
                    }
                }
            }
        }).state('index.artifact', {
            url: '/artifact',
            views: {
                'content': {
                    templateUrl: 'modules/deployment/views/artifacts.client.view.html'
                }
            }
        }).state('index.deployment', {
            resolve: {
                hosts: function ($http) {
                    return $http.get('/configuration/hosts');
                }
            },
            url: '/deployment',
            views: {
                'content': {
                    templateUrl: 'modules/deployment/views/deployment.client.view.html',
                    controller: function ($scope, hosts) {
                        $scope.hosts = hosts.data;
                    }
                }
            }
        }).state('index.changelog', {
            url: '/changelog',
            views: {
                'content': {
                    templateUrl: 'modules/changelog/views/changelog.client.view.html'
                }
            }
        }).state('index.coverage', {
            resolve: {
                coverage: function ($http) {
                    return $http.get('/coverage');
                }
            },
            url: '/coverage',
            views: {
                'content': {
                    templateUrl: 'modules/coverage/views/coverage.client.view.html',
                    controller: function ($scope, coverage) {
                        $scope.coverage = coverage.data.coverage;
                    }
                }
            }
        });
    }
]);
