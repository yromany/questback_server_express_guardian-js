'use strict';

angular.module('core').factory('socket', function (socketFactory) {
	return socketFactory();
});