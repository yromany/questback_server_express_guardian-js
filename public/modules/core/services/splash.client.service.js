'use strict';

angular.module('core').service('$splash', ['$modal', '$rootScope',
    function ($modal, $rootScope) {
        return {
            open: function (attrs, opts) {
                var scope = $rootScope.$new();
                angular.extend(scope, attrs);
                opts = angular.extend(opts || {}, {
                    //keyboard: false,
                    backdrop: false,
                    scope: scope,
                    windowTemplateUrl: 'modules/core/views/splash.client.view.html'
                });
                return $modal.open(opts);
            }
        };
    }
]);