'use strict';

angular.module('coverage').controller('CoverageController', ['$scope',
    function ($scope) {


        var initCoverage = function() {

            var data = google.visualization.arrayToDataTable($scope.coverage),
                tree = new google.visualization.TreeMap(document.getElementById('coverage'));

            tree.draw(data, {
                minColor: '#f56954',
                midColor: '#ffcc73',
                maxColor: '#1acd70',
                headerHeight: 15,
                fontColor: 'black',
                showScale: true,
                maxColorValue: 100,
                useWeightedAverageForAggregation: true
            });
        };

        $scope.init = function() {
            initCoverage();
        };

        $scope.init();

    }
]);