'use strict';

angular.module('coverage').directive('coverage', [
    function () {
        return {
            restrict: 'E',
            template: '<div></div>',
            replace: true,
            scope: {
                data: '='
            },
            link: function(scope, element, attrs) {

                var data = google.visualization.arrayToDataTable(scope.data),
                tree = new google.visualization.TreeMap(document.getElementById('coverage'));

                tree.draw(data, {
                    minColor: '#ff0000',
                    midColor: '#FF7722',
                    maxColor: '#00ff00',
                    headerHeight: 15,
                    fontColor: 'black',
                    showScale: true,
                    maxColorValue: 100,
                    useWeightedAverageForAggregation: true
                });

            }
        };
    }
]);