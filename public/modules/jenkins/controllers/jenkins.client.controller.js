'use strict';

angular.module('core').controller('BuildController', ['$scope', '$http',
  function($scope, $http) { 
    
    $scope.build = {};
    $scope.build.title = 'Commit Stage';
    $scope.build.version = 'DEV';
    
    // initialize build status
    var initBuilds = function() {
      $scope.buildsStarted = false;
      $http.get('jenkins/builds').success(function(data) {
        $scope.buildStatus = data;
        $scope.buildsStarted = true;
      });
    };
    
    // get test report from a given jenkins job
    var initTestReport = function() {
      $scope.testReportStarted = false;
      $http.get('jenkins/testreport?job=EFS_DEV_AcceptanceStage_WebDriver').success(function(data) {
        $scope.testreport = data;
        $scope.testReportStarted = true;
      });
    };
    
    $scope.init = function() {
      initBuilds();
      initTestReport();
    };
    
    $scope.init();
    
    // draggable build options
    $scope.buildStatusOptions = {
      // update status position on drop and dom change
      stop: function(e, ui) {
        for (var index in $scope.buildStatus) {
          $scope.buildStatus[index].position = index;
        }
        $http.put('jenkins/build/position', {builds : $scope.buildStatus});
      }
    };
    
    // add build start
    $scope.addBuildStatus = function() {
      $scope.addingBuildStatus = true;
      $http.get('jenkins/build/stages').success(function(data) {
        $scope.BuildStages = data;
      });
    };
    
    // add build confirm
    $scope.addBuildStatusConfirm = function(status) {
      status.stage = status.title.replace(/ /g,'');
      $http.post('jenkins/build', {status : status}).success(function(data) {
        $scope.addingBuildStatus = false;
        initBuilds();
      });
    };
    
    // remove build status
    $scope.deleteBuildStatus = function(id) {
      $http.delete('jenkins/build/'+id).success(function(data) {
        initBuilds();
      });
    };
    
    
  }
]).controller('TestReportController', ['$scope', '$http',
  function($scope, $http) { 
    
    // get test report from a given jenkins job
    var initTestReport = function() {
      $scope.testReportStarted = false;
      $http.get('jenkins/testreport?job=EFS_DEV_AcceptanceStage_WebDriver').success(function(data) {
        
        $scope.testReport = {
          options: {
            credits: {
              enabled: false
            },
            chart: {
              type: 'pie'
            },
            plotOptions: {
              pie: {
                colors: ['#1acd70', '#ff876c'],
                dataLabels: {
                  format: '<strong>{point.percentage:.0f} </strong> %',  
                },
                showInLegend: true
              }
            },
            legend: {
              enabled: true,
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          },
          series: [{
            data: [
              ['Passed',   data.passCount],
              {
                name: 'Failed',
                y: data.failCount,
                sliced: true,
                selected: true
              }
            ]
          }],
          title: {
            style: {
              display: 'none'
            }
          },
          loading: false
        };
        
        $scope.testReportStarted = true;
      });
    };
    
    $scope.init = function() {
      initTestReport();
    };
    
    $scope.init();
    
  }
]);