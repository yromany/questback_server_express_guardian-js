'use strict';

angular.module('chat').controller('ChatController', ['$scope', 'Authentication', 'socket', '$http', 
  function($scope, Authentication, socket, $http) {
    $scope.account = Authentication.account;
    
    $scope.chatStarted = false;
    $scope.configLimit = false;
    $scope.messages = [];
    
    $scope.chatLimit = $scope.account.chat.chatLimit; 
    $scope.chatFrom = $scope.account.chat.chatFrom;
    
    // initialize chat
    var initChat = function() {
      $scope.message = '';
      $http.get('chat/messages').success(function(data) {
        $scope.messages = data;
        $scope.chatStarted = true;
      });
    };
    
    $scope.setLimit = function(limit) {
      $scope.chatStarted = false;
      $scope.chatLimit = limit;
      var data = {'limit': $scope.chatLimit};
      $http.put('settings/chat/limit', data).success(function(data) {
        initChat();
        $scope.configLimit = false;
      });
    };
    
    $scope.setFrom = function(from) {
      $scope.chatStarted = false;
      $scope.chatFrom = from;
      var data = {'from': $scope.chatFrom};
      $http.put('settings/chat/from', data).success(function(data) {
        initChat();
      });
    };
    
    $scope.init = function() {
      initChat();
    };
    
    $scope.init();
    
    socket.on('got:message', function (data) {
      $scope.messages.unshift({
        user: data.user,
        message: data.message,
        when: data.when
      });
    });

    $scope.sendMessage = function () {
      if ($scope.message.trim() !== '') {
        if($scope.message === '/limit') {
          $scope.configLimit = true;
        } else {
          socket.emit('send:message', {
            user: $scope.account,
            message: $scope.message
          });
        }
        $scope.lastMessage = $scope.message;
        $scope.message = '';  
      }
    };
    
	}
]);