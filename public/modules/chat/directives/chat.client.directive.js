(function () {

  'use strict';

  var
    main = 'smile',
    smilies = ['biggrin', 'confused', 'cool', 'cry', 'eek', 'evil', 'like', 'lol', 'love', 'mad', 'mrgreen', 'neutral', 'question', 'razz', 'redface', 'rolleyes', 'sad', 'smile', 'surprised', 'thumbdown', 'thumbup', 'twisted', 'wink'],
    shorts = {
      ':D': 'biggrin',
      ':-D': 'biggrin',
      ':S': 'confused',
      ':-S': 'confused',
      ';(': 'cry',
      ';-(': 'cry',
      'OO': 'eek',
      '<3': 'like',
      '&lt;3': 'like',
      '^^': 'lol',
      ':|': 'neutral',
      ':-|': 'neutral',
      ':P': 'razz',
      ':-P': 'razz',
      ':(': 'sad',
      ':-(': 'sad',
      ':)': 'smile',
      ':-)': 'smile',
      ':O': 'surprised',
      ':-O': 'surprised',
      ';)': 'wink',
      ';-)': 'wink'
    },

    regex = new RegExp(':(' + smilies.join('|') + '):', 'g'),
    template = '<i class="smiley-$1" title="$1"></i>',

    escapeRegExp = function (str) {
      return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
    },

    regExpForShort = function (str) {
      if (/^[a-z]+$/i.test(str)) { // use word boundaries if emoji is letters only
        return new RegExp('\\b' + str + '\\b', 'gi');
      }
      else {
        return new RegExp(escapeRegExp(str), 'gi');
      }
    },

    templateForSmiley = function (str) {
      return template.replace('$1', str);
    },

    apply = function (input) {
      if (!input) return '';

      var output = input.replace(regex, template);

      for (var sm in shorts) {
        if (shorts.hasOwnProperty(sm)) {
          output = output.replace(regExpForShort(sm), templateForSmiley(shorts[sm]));
        }
      }

      return output;
    };
  
  angular.module('chat')
  /* smilies parser filter */
    .filter('smilies', function () {
      return apply;
    })
    /* smilies parser attribute */
    .directive('smilies', function () {
      return {
        restrict: 'A',
        scope: {
          source: '=smilies'
        },
        link: function ($scope, el) {
          el.html(apply($scope.source));
        }
      };
    })
    /* smilies selector directive */
    .directive('smiliesSelector', ['$timeout', function ($timeout) {
      var templateUrl;
      try {
        angular.module('ui.bootstrap.popover');
        templateUrl = 'template/smilies/button-a.html';
      }
      catch (e) {
        console.error('No Popover module found');
        return {};
      }
      return {
        restrict: 'A',
        templateUrl: templateUrl,
        scope: {
          source: '=smiliesSelector',
          placement: '@smiliesPlacement',
          title: '@smiliesTitle'
        },
        link: function ($scope, el) {
          $scope.smilies = smilies;

          $scope.append = function (smiley) {

            if ($scope.source === undefined) {
              $scope.source = '';
            }

            $scope.source += ' :' + smiley + ': ';

            $timeout(function () {
              el.children('i').click(); // close the popover
            });
          };
        }
      };

    }])
    /* helper directive for input focusing on choosing smiley */
    .directive('focusOnChange', function () {
      return {
        restrict: 'A',
        link: function ($scope, el, attrs) {
          $scope.$watch(attrs.focusOnChange, function () {
            el[0].focus();
          });
        }
      };
    })
    /* data range picker directive */
    .directive('dateRangePicker', function () {
      return {
        link: function (scope, element, attrs, form) {
          element.daterangepicker(
            {
              opens: 'right',
              ranges: {
                'Today': [window.moment(), window.moment()],
                'Last Day': [window.moment().subtract(1, 'days'), window.moment()],
                'Last 7 Days': [window.moment().subtract(6, 'days'), window.moment()],
                'Last 30 Days': [window.moment().subtract(29, 'days'), window.moment()],
                'Last Quarter': [window.moment().subtract(3, 'month').startOf('month'), window.moment()],
                'Last Year': [window.moment().subtract(1, 'year').startOf('month'), window.moment()]
              },
              startDate: window.moment().subtract(6, 'days'),
              endDate: window.moment()
            },
            function (start, end) {
              scope.setFrom(start);
              angular.element('#chatrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
          );
        }
      };
    })
    /* send message on pressing return directive */
    .directive('ngEnter', function () {
      return function (scope, element, attrs) {
        element.bind('keydown keypress', function (event) {
          if (event.which === 13) {
            scope.$apply(function () {
              scope.$eval(attrs.ngEnter);
            });
            event.preventDefault();
          }
        });
      };
    })
    .directive('chatLimitRange', function () {
      return {
        link: function (scope, element, attrs, form) {
          element.ionRangeSlider({
            from: scope.chatLimit, onFinish: function (obj) {
              scope.setLimit(obj.fromNumber);
            }
          });
        }
      };
    })
    .directive('chatRange', function () {
      return {
        link: function (scope, element, attrs, form) {
          element.html(window.moment(scope.chatFrom).format('MMMM D, YYYY') + ' - ' + window.moment().format('MMMM D, YYYY'));
        }
      };
    })
    /* popover template */
    .run(['$templateCache', function ($templateCache) {
      // use ng-init because popover-template only accept a variable
      $templateCache.put('template/smilies/button-a.html',
        '<i class="smiley-' + main + ' smilies-selector" ' +
        'ng-init="smiliesTemplate = \'template/smilies/popover-a.html\'" ' +
        'popover-template="smiliesTemplate" ' +
        'popover-placement="{{!placement && \'left\' || placement}}" ' +
        'popover-title="{{title}}"</i>'
      );
      $templateCache.put('template/smilies/popover-a.html',
        '<div ng-model="smilies" class="smilies-selector-content">' +
        '<i class="smiley-{{smiley}}" ng-repeat="smiley in smilies" ng-click="append(smiley)"></i>' +
        '</div>'
      );
      $templateCache.put('template/smilies/popover-b.html',
        '<div class="popover" tabindex="-1">' +
        '<div class="arrow"></div>' +
        '<h3 class="popover-title" ng-bind-html="title" ng-show="title"></h3>' +
        '<div class="popover-content">' +
        $templateCache.get('template/smilies/popover-a.html') +
        '</div>' +
        '</div>'
      );
    }]);
}());