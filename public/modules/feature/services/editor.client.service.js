'use strict';

angular.module('feature').service('Editor', function() {
  var editor = null;
  var last_selected_feature = '';
 
  this.createEditor = function() {
    editor = window.ace.edit('web-editor');
    editor.renderer.setShowGutter(false);
    editor.renderer.setShowPrintMargin(false);
    editor.setTheme('ace/theme/monokai');
    editor.getSession().setMode('ace/mode/gherkin');
    editor.getSession().setTabSize(2);
    editor.setReadOnly(true);
    editor.setOptions({
      maxLines: 1000
    });
    return editor;
  };
  
  this.init = function() {
    editor = null;
  };
  
  this.getEditor = function() {
    return editor;
  };
  
  this.getLastSelected = function() {
    return last_selected_feature;
  };
  
  this.setLastSelected = function(feature) {
    last_selected_feature = feature;
  };
  
});