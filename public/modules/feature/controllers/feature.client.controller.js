'use strict';

angular.module('feature').controller('FeatureController', ['$scope', 'Editor', '$http', '$anchorScroll',
    function ($scope, Editor, $http, $anchorScroll) {

        var tree;
        $scope.ftree = tree = {};
        $scope.last_selected_feature = Editor.getLastSelected();

        $scope.browsers = [
            {label: 'Chrome (latest)', name: 'chrome_cloud', os: 'Windows 8.1'},
            {label: 'Firefox (latest)', name: 'firefox_cloud', os: 'Windows 8.1'},
            {label: 'Chrome (latest)', name: 'chrome_cloud', os: 'Windows 7'},
            {label: 'Firefox (latest)', name: 'firefox_cloud', os: 'Windows 7'}
        ];

        $scope.installations = [
            {label: 'http://dev-aat-tka.qb-feedback.com', environment: 'Testkanal'},
            {label: 'http://dev-integration.qb-feedback.com', environment: 'Automation Farm'}
        ];

        $scope.pie = {
            options: {
                chart: {
                    type: 'pie',
                    height: 150,
                    margin: [0, 0, 0, 0]
                },
                plotOptions: {
                    pie: {
                        size: '80%',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                legend: {
                    enabled: false,
                    layout: 'vertical',
                    align: 'left',
                    verticalAlign: 'middle'
                }
            },
            series: [{
                data: [
                    ['Firefox', 45.0],
                    ['IE', 26.8],
                    ['Chrome', 12.8],
                    ['Safari', 8.5],
                    ['Opera', 6.2],
                ]
            }],
            title: {
                text: 'Feature statistics',
                style: {
                    display: 'none'
                }
            },
            loading: false
        };

        $scope.column = {
            options: {
                chart: {
                    type: 'column',
                    height: 150,
                    margin: [0, 0, 0, 0]
                },
                plotOptions: {
                    pie: {
                        size: '80%',
                        dataLabels: {
                            enabled: false
                        }
                    }
                },
                legend: {
                    enabled: false
                }
            },
            series: [{
                data: [
                    ['Firefox', 45.0],
                    ['IE', 26.8],
                    ['Chrome', 12.8],
                    ['Safari', 8.5],
                    ['Opera', 6.2],
                ]
            }],
            title: {
                text: 'Feature statistics',
                style: {
                    display: 'none'
                }
            },
            loading: false
        };

        $scope.browser = $scope.browsers[0];
        $scope.installation = $scope.installations[0];

        var search = function (features, found) {
            $scope.folder_loaded = false;
            if (features.data.type === 'folder') {
                if (features.children.length > 0) {
                    angular.forEach(features.children, function (value, key) {
                        if (value.data.type === 'folder') {
                            if (value.children.length > 0) {
                                search(value, found);
                            }
                        } else {
                            if (value.label.match($scope.featureSearch)) found.push(value);
                        }
                    });
                }
            }
            return found;
        };

        $scope.$watch('featureSearch', function () {
            if ($scope.featureSearch) {
                $scope.feature_loaded = false;
                var found = [];
                $scope.filteredSearch = $scope.features;
                $scope.filteredSearch = search($scope.features[0], found);
            }
            else {
                $scope.filteredSearch = $scope.features;
            }
        });

        $scope.features_handler = function (branch) {
            Editor.setLastSelected(branch.label);
            if (branch.data.type === 'folder') {
                $scope.feature_loaded = false;
                $scope.folder_loaded = true;
                if (branch.selected) {
                    $scope.child_features = [];
                    var getChildren = function (branch) {
                        for (var index in branch.children) {
                            if (branch.children[index].label.split('.').pop() === 'feature') {
                                //branch.children[index].group = branch.data.label;
                                $scope.child_features.push(branch.children[index]);
                            }
                            getChildren(branch.children[index]);
                        }
                    };
                    getChildren(branch);
                }

            } else {
                $scope.feature_loaded = true;
                $scope.folder_loaded = false;
                var path = {
                    path: branch.data.path
                };
                $http.post('/feature', path).success(function (response) {
                    var editor = (Editor.getEditor() === null) ? Editor.createEditor() : Editor.getEditor();
                    editor.getSession().setValue(response + '\n\n\n');
                    $anchorScroll();
                }).error(function (response) {
                    $scope.error = response;
                });
            }
        };

    }
]);