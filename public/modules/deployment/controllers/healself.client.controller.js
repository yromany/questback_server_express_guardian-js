'use strict';

angular.module('deployment').controller('HealselfController', ['$scope', 'healself', 'installation',
    function ($scope, healself, installation) {
        $scope.healself = healself.data.result;
        $scope.installation = installation;
    }
]);