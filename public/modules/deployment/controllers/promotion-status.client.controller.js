'use strict';

angular.module('deployment').controller('PromotionStatusController', ['$scope', '$http', 'Authentication',
    function ($scope, $http, Authentication) {

        $scope.promotionHost = 'http://old-servicegw.tka.globalpark.com:84'; //TODO muss eine config variable sein
        $scope.distributionPercentage = 0;
        $scope.distributionStatus = 'Initializing...';
        $scope.distributionType = 'default';
        $scope.distributionBarStatus = 'progress-striped active';
        $scope.distributionFailed = false;
        $scope.promotionStarted = false;
        $scope.alreadyPromoting = false;

        $scope.account = Authentication.account;

        // handle artifact page navigation
        $scope.searchArtifacts = function () {
            $scope.artifact.searched = false;
            var start = 0;
            if ($scope.currentArtifactPage > 0) {
                start = 5 * $scope.currentArtifactPage - 5;
            }
            $http.get('artifacts?start=' + start + '&version=' + $scope.selectedVersions + '&max=5').success(function (data) {
                $scope.artifact.artifacts = data.artifacts;
                $scope.artifact.searched = true;
            });
        };

        var getPromotionStatus = {
            'id': 1,
            'service': 'status',
            'method': 'getPromotionStatus',
            'params': ['EFS']
        };

        /**
         * Wait for deployment to be completed
         */
        function waitForPromotionCompleted() {
            setTimeout(function(){}, 2000); // wait 2 seconds before start
            var progress = setInterval(function () {
                $http.post($scope.promotionHost, getPromotionStatus).success(function (data) {
                    if (data.result[0] === -1) {
                        clearInterval(progress);
                        if (!$scope.distributionFailed) {
                            $scope.distributionPercentage = 100;
                            $scope.distributionStatus = 'Artifact was successfully promoted';
                            $scope.distributionType = 'success';
                            $scope.distributionBarStatus = '';
                            $scope.artifactToPromote.promoted = true;
                            $scope.artifactToPromote.promotedBy = $scope.account.displayName;
                            $scope.searchArtifacts();
                        }
                    } else {
                        if (typeof data.error !== 'undefined') {
                            $scope.distributionPercentage = 100;
                            $scope.distributionStatus = data.error.message;
                            $scope.distributionType = 'danger';
                            $scope.distributionBarStatus = '';
                        } else {
                            $scope.distributionPercentage = data.result.progress;
                            $scope.distributionStatus = data.result.message;
                        }
                    }
                });
            }, 800);
        }

        $scope.init = function () {

            var promotion = {
                'product': $scope.artifactToPromote.product,
                'version': $scope.artifactToPromote.version,
                'name': $scope.artifactToPromote.name,
                'artifactory': $scope.artifactToPromote.artifactory
            };

            $http.post($scope.promotionHost, getPromotionStatus).success(function (data) {
                if (data.result[0] !== -1) {
                    $scope.alreadyPromoting = true;
                    $scope.searchArtifacts();
                } else {
                    $scope.promotionStarted = true;
                    $http.put('/artifact/promote', promotion).success(function (data) {
                        if (data.result === false) {
                            $scope.distributionFailed = true;
                            $scope.distributionPercentage = 100;
                            $scope.distributionStatus = data.error.message;
                            $scope.distributionType = 'danger';
                            $scope.distributionBarStatus = '';
                        }
                    });
                    waitForPromotionCompleted();
                }
            });
        };

        $scope.init();

    }
]);
