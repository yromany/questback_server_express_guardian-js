'use strict';

angular.module('deployment').controller('CommentController', ['$scope', 'instanceArtifact', '$modalInstance', '$http',
	function ($scope, instanceArtifact, $modalInstance, $http) {
		$scope.artifact = instanceArtifact;
		$scope.comment = $scope.artifact.comment;
		$scope.ok = function () {
			var data = {'name': $scope.artifact.name, 'comment': $scope.comment};
			$http.put('/artifact/comment', data).success(function (data) {
				$scope.artifact.comment = $scope.comment;
				$modalInstance.close();
			});
		};

	}
]);