'use strict';

angular.module('deployment').controller('DeploymentStatusController', ['$scope', '$http', '$modal',
    function ($scope, $http, $modal) {

        $scope.distributionPercentage = 0;
        $scope.distributionStatus = 'Initializing...';
        $scope.distributionType = 'default';
        $scope.distributionBarStatus = 'progress-striped active';
        $scope.healselfTemplate = 'template/deployment/healself.html';

        var getBuildUploadStatus = {
            'id': 1,
            'service': 'status',
            'method': 'getBuildUploadStatus',
            'params': [$scope.artifact.name]
        };

        var getDeployStatus = function (installation) {
            return {
                'id': 1,
                'service': 'status',
                'method': 'getDeployStatus',
                'params': [installation]
            };
        };

        var isDeploying = function (installation) {
            return {'id': 1, 'service': 'status', 'method': 'isDeploying', 'params': [installation]};
        };

        var deployBuild = function (installation) {
            return {
                'id': 1,
                'service': 'deploy',
                'method': 'deployBuild',
                'params': [installation, $scope.artifact.name, true]
            };
        };

        /**
         * Wait for deployment to be completed
         */
        function waitForDeploymentCompleted(installation, index) {
            var progress = setInterval(function () {
                $http.post($scope.host.hostURL, getDeployStatus(installation.name)).success(function (data) {
                    if (data.result[0] === -1) {
                        clearInterval(progress);
                        if (!$scope.installations[index].deploymentFailed) {
                            $scope.installations[index].deploymentPercentage = 100;
                            $scope.installations[index].deploymentStatus = 'Build was successfully deployed';
                            $scope.installations[index].deploymentType = 'success';
                            $scope.installations[index].deploymentBarStatus = '';
                            $scope.installations[index].deploymentFinnished = true;
                        }
                    } else {
                        $scope.installations[index].deploymentPercentage = data.result.progress;
                        $scope.installations[index].deploymentStatus = data.result.message;
                    }
                });
            }, 800);
        }

        /**
         * Initialize build deployment
         */
        function deploy() {
            $scope.installations.forEach(function (installation, index) {
                $scope.installations[index].deploymentStarted = true;
                $scope.installations[index].deploymentPercentage = 0;
                $scope.installations[index].deploymentStatus = 'Initializing...';
                $scope.installations[index].deploymentType = 'default';
                $scope.installations[index].deploymentBarStatus = 'progress-striped active';
                $http.post($scope.host.hostURL, isDeploying(installation.name)).success(function (data) {
                    if (data.result === false) {
                        $http.post($scope.host.hostURL, deployBuild(installation.name)).success(function (data) {
                            if (typeof data.error !== 'undefined') {
                                $scope.installations[index].deploymentPercentage = 100;
                                $scope.installations[index].deploymentStatus = data.error.message;
                                $scope.installations[index].deploymentType = 'danger';
                                $scope.installations[index].deploymentBarStatus = '';
                                $scope.installations[index].deploymentFailed = true;
                                $scope.installations[index].deploymentFinnished = true;
                            }
                        });
                        waitForDeploymentCompleted(installation, index);
                    }
                    else {
                        $scope.installations[index].deploymentPercentage = 100;
                        $scope.installations[index].deploymentStatus = 'Deployment not possible. Already deploying';
                        $scope.installations[index].deploymentType = 'warning';
                        $scope.installations[index].deploymentBarStatus = '';
                        $scope.installations[index].deploymentFailed = true;
                        $scope.installations[index].deploymentFinnished = true;
                    }
                });

            });
        }

        /**
         * Initialize upload progress bar
         */
        function initUploadProgressBar() {
            var progress = setInterval(function () {
                $http.post($scope.host.hostURL, getBuildUploadStatus).success(function (data) {
                    $scope.distributionPercentage = data.result.progress;
                    $scope.distributionStatus = data.result.message;
                    if (data.result[0] === -1) {
                        clearInterval(progress);
                        $scope.distributionPercentage = 100;
                        $scope.distributionStatus = 'Build was successfully distributed';
                        $scope.distributionType = 'success';
                        $scope.distributionBarStatus = '';
                        deploy();
                    }
                });
            }, 800);
        }

        /**
         * Wait for upload process to start
         */
        function waitForUploadProcess() {
            //the artifact needs to be distributed (we want to check the upload status)
            var timeout = 0;
            var waitForUpload = setInterval(function () {
                //we use this timeout to be sure that the upload process has started
                if (timeout >= 10) {
                    alert('Something went wrong when getting the distribution process');
                    $scope.distributionPercentage = 100;
                    $scope.distributionStatus = 'Build could not be distributed';
                    $scope.distributionType = 'danger';
                    $scope.distributionBarStatus = '';
                    clearInterval(waitForUpload);
                } else {
                    timeout = timeout + 1;
                }
                /**
                 * wait until upload process is started
                 */
                $http.post($scope.host.hostURL, getBuildUploadStatus).success(function (data) {
                    if (data.result[0] !== -1) {
                        // OK we have an upload process
                        clearInterval(waitForUpload);
                        initUploadProgressBar();
                    }
                });
            }, 1000);
        }

        $scope.init = function () {
            $scope.installations.forEach(function (installation, index) {
                $scope.installations[index].deploymentStarted = true;
                $scope.installations[index].deploymentPercentage = 0;
                $scope.installations[index].deploymentStatus = 'Initializing...';
                $scope.installations[index].deploymentType = 'default';
                $scope.installations[index].deploymentBarStatus = 'progress-striped active';
                $scope.installations[index].deploymentFinnished = false;
                $scope.installations[index].deploymentFailed = false;
            });
            var data = {'id': 1, 'service': 'deploy', 'method': 'buildExists', 'params': [$scope.artifact.name]};
            $http.post($scope.host.hostURL, data).success(function (data) {
                if (typeof data.error !== 'undefined') {
                    initUploadProgressBar();
                } else {
                    if (data.result === true) {
                        initUploadProgressBar();
                    } else {
                        var body = {'artifact': $scope.artifact.artifactory};
                        $http.put($scope.host.hostURL + '/?service=deploy&build=' + $scope.artifact.name + '&artifactory=true', body);
                        waitForUploadProcess();
                    }
                }
            });
        };

        $scope.init();

        // healself modal
        $scope.healself = function (installation) {
            $modal.open({
                templateUrl: $scope.healselfTemplate,
                controller: 'HealselfController',
                size: 'lg',
                resolve: {
                    healself: function ($http) {
                        var getHealselfLog = {
                            'id': 1,
                            'service': 'status',
                            'method': 'getHealselfLog',
                            'params': [installation, true]
                        };
                        return $http.post($scope.host.hostURL, getHealselfLog);
                    },
                    installation: function () {
                        return installation;
                    }
                }
            });
        };
    }
]).run(['$templateCache', function ($templateCache) { // template for comment modal
    $templateCache.put(
        'template/deployment/healself.html',
        '<div class="modal-header"><h3 class="modal-title">Healself log</h3></div>' +
        '<div class="modal-body" slim-scroll h="600px" s="5px">' +
        '<p class="lead">{{ installation }}</p>' +
        '<pre data-ng-bind-html="healself"></pre>' +
        '</div>' +
        '<div class="modal-footer">' +
        '<button class="btn btn-primary" data-ng-click="$close()">Ok</button>' +
        '</div>' +
        '</div>'
    );
}]);