'use strict';

angular.module('deployment').controller('DeploymentController', ['$scope', '$http', '$modal', 'Authentication', '$splash', 'socket', 'growl',
    function ($scope, $http, $modal, Authentication, $splash, socket, growl) {

        $scope.account = Authentication.account;

        $scope.selectedHost = null;
        $scope.installations = [];
        $scope.selectedInstallations = [];
        $scope.selectedVersions = [];
        $scope.selectedVersionsModel = [];
        $scope.artifact = {};
        $scope.selectedInstallationsDetails = [];
        $scope.modulesTemplate = 'template/deployment/modules.html';
        $scope.customModulesTemplate = 'template/deployment/custom.html';
        $scope.changesetsTemplate = 'template/deployment/changeset.html';
        $scope.commentTemplate = 'template/deployment/comment.html';
        $scope.currentArtifactPage = 0;
        $scope.favoriteInst = $scope.account.favoriteInst;
        $scope.myFavoriteInst = [];
        $scope.allVersionsShowed = false;

        // for artifact management
        $scope.product = {};
        $scope.versions = [];

        var max_artifacts = 5,
            all_artifacts = false;

        // help function to remove element from array
        function remove(arr, what) {
            var found = arr.indexOf(what);
            while (found !== -1) {
                arr.splice(found, 1);
                found = arr.indexOf(what);
            }
        }

        // initialize deployment page
        var initDeployment = function () {
            $http.get('user/favorite/installations').success(function (data) {
                $scope.myFavoriteInst = data;
            });
        };

        $scope.init = function () {
            initDeployment();
        };

        $scope.init();

        // search for artifacts
        $scope.searchArtifacts = function () {
            if ($scope.selectedVersionsModel.length > 0) {
                $scope.artifact.searched = false;
                var start = 0;
                if ($scope.currentArtifactPage > 0) {
                    start = max_artifacts * $scope.currentArtifactPage - max_artifacts;
                }
                $http.get('artifacts?start=' + start + '&version=' + $scope.selectedVersionsModel + '&max=' + max_artifacts + '&all=' + all_artifacts).success(function (data) {
                    $scope.artifact.artifacts = data.artifacts;
                    $scope.artifact.total = data.total;
                    $scope.artifact.searched = true;
                });
            }
        };

        // handle selection of installations
        $scope.selectInstallations = function () {
            $scope.selectedInstallationsDetails = [];
            $scope.selectedVersions = [];
            $scope.selectedVersionsModel = [];
            $scope.artifact = {};
            $scope.artifact.searched = false;
            $scope.currentArtifactPage = 0;
            $scope.allVersionsShowed = false;
            angular.forEach($scope.selectedInstallations, function (value, key) {
                $scope.selectedInstallationsDetails[key] = ({name: value});
                if (window.$.inArray(value, $scope.myFavoriteInst) > -1) {
                    $scope.selectedInstallationsDetails[key].favorite = 'favorite';
                }
            });
            var count = 0;
            angular.forEach($scope.selectedInstallations, function (value, key) {
                var data = {id: 1, service: 'installations', method: 'getInstallationInfo', params: [value]};
                $http.post($scope.selectedHost.hostURL, data).success(function (cb) {
                    count++;
                    if (typeof cb.error !== 'undefined') {
                        growl.error(cb.error.message, {});
                        if ($scope.selectedVersions.indexOf('DEV') === -1) {
                            $scope.selectedVersions.push('DEV');
                            $scope.selectedVersionsModel.push('DEV');
                        }
                    } else {
                        $scope.selectedInstallationsDetails[key].info = cb.result;
                        if ($scope.selectedVersions.indexOf(cb.result['EFS Version']) === -1) {
                            $scope.selectedVersions.push(cb.result['EFS Version'].toUpperCase());
                            $scope.selectedVersionsModel.push(cb.result['EFS Version'].toUpperCase());
                        }
                    }
                    if (count === $scope.selectedInstallations.length) {
                        $scope.searchArtifacts();
                    }
                });
            });
        };

        // get installations from given given host
        $scope.getInstallations = function () {
            $scope.installations = [];
            $scope.selectedInstallations = [];
            $scope.selectedInstallationsDetails = [];
            $scope.selectedVersions = [];
            $scope.selectedVersionsModel = [];
            var data = {'id': 1, 'service': 'status', 'method': 'getInstallations', 'params': []};
            $http.post($scope.selectedHost.hostURL, data).success(function (cb) {
                cb.result.sort();
                var installations = cb.result;
                if ($scope.favoriteInst) {
                    installations.forEach(function (inst) {
                        if (window.$.inArray(inst, $scope.myFavoriteInst) > -1) {
                            $scope.installations.push(inst);
                        }
                    });
                } else $scope.installations = cb.result;
            });
        };

        // handle artifact page navigation
        $scope.pageArtifactChanged = function () {
            $scope.artifact.searched = false;
            var start = max_artifacts * $scope.currentArtifactPage - max_artifacts;
            $http.get('artifacts?start=' + start + '&version=' + $scope.selectedVersionsModel + '&max=' + max_artifacts + '&all=' + all_artifacts).success(function (data) {
                $scope.artifact.artifacts = data.artifacts;
                $scope.artifact.searched = true;
            });
        };

        // changesets modal
        $scope.open = function (artifactName) {
            $modal.open({
                templateUrl: 'modules/deployment/views/deployment-changeset.client.view.html',
                controller: 'ChangeSetController',
                size: 'lg',
                resolve: {
                    artifactName: function () {
                        return artifactName;
                    },
                    artifact: function () {
                        return $scope.artifact;
                    },
                    version: function () {
                        return $scope.selectedVersionsModel;
                    },
                    host: function () {
                        return $scope.selectedHost;
                    },
                    installations: function () {
                        return $scope.selectedInstallationsDetails;
                    },
                    max_artifacts: function () {
                        return max_artifacts;
                    },
                    all_artifacts: function () {
                        return all_artifacts;
                    }
                }
            });
        };

        // comments modal
        $scope.comment = function (artifact) {
            $modal.open({
                templateUrl: $scope.commentTemplate,
                controller: 'CommentController',
                resolve: {
                    instanceArtifact: function () {
                        return artifact;
                    }
                }
            });
        };

        // toggle show only favorite installations
        $scope.toggleFavoriteInst = function () {
            var data = {'favoriteInst': $scope.favoriteInst};
            $http.put('/settings/favorite/inst', data).success(function (data) {
                if ($scope.selectedHost) $scope.getInstallations();
            });
        };

        // add or remove installation to favorite list
        $scope.handleFavoriteInstallation = function (installation) {
            var data = {'installation': installation.name};
            if (installation.favorite === 'favorite') {
                $http.delete('/user/favorite/installation/' + installation.name, data).success(function (data) {
                    installation.favorite = '';
                    remove($scope.myFavoriteInst, installation.name);
                });
            } else {
                $http.post('/user/favorite/installation', data).success(function (data) {
                    installation.favorite = 'favorite';
                    $scope.myFavoriteInst.push(installation.name);
                });
            }
        };

        // approve artifact
        $scope.approve = function (artifact) {
            $scope.artifact.searched = false;
            var data = {'name': artifact.name};
            $http.put('/artifact/approve', data).success(function (data) {
                artifact.approved = true;
                artifact.approvedBy = $scope.account.displayName;
                $scope.artifact.searched = true;
            });
        };

        // decline artifact
        $scope.decline = function (artifact) {
            $scope.artifact.searched = false;
            var data = {'name': artifact.name};
            $http.put('/artifact/decline', data).success(function (data) {
                artifact.declined = true;
                artifact.declinedBy = $scope.account.displayName;
                $scope.artifact.searched = true;
            });
        };

        // revoke artifact decline
        $scope.revoke = function (artifact) {
            $scope.artifact.searched = false;
            var data = {'name': artifact.name};
            $http.put('/artifact/revoke', data).success(function (data) {
                artifact.declined = false;
                artifact.declinedBy = '';
                $scope.artifact.searched = true;
            });
        };

        // start artifact deployment
        $scope.deploy = function (artifact) {
            var answer = window.confirm('Sure?');
            if (answer) {
                $splash.open({
                    artifact: artifact,
                    host: $scope.selectedHost,
                    installations: $scope.selectedInstallationsDetails
                }, {
                    templateUrl: 'modules/deployment/views/deployment-splash.client.view.html'
                });
            }
        };

        // start artifact deployment
        $scope.promote = function (artifact) {
            var answer = window.confirm('Sure?');
            if (answer) {
                $splash.open({
                    artifactToPromote: artifact,
                    artifact: $scope.artifact,
                    currentArtifactPage: $scope.currentArtifactPage,
                    selectedVersions: $scope.selectedVersions
                }, {
                    templateUrl: 'modules/deployment/views/promotion-splash.client.view.html'
                });
            }
        };

        // show all versions (only available 4 QA)
        $scope.showAllVersions = function () {
            $scope.artifact.searched = false;
            $http.get('artifacts/versions?product=EFS').success(function (data) {
                $scope.selectedVersions = [];
                $scope.selectedVersionsModel = [];
                data.forEach(function (version) {
                    $scope.selectedVersions.push(version);
                    $scope.selectedVersionsModel.push(version);
                    $scope.allVersionsShowed = true;
                });
                $scope.searchArtifacts();
            });
        };

        // reload artifact list when new artifact has been checked in
        socket.on('artifact:refresh', function (data) {
            $scope.searchArtifacts();
        });

        $scope.getVersions = function () {
            max_artifacts = 12;
            all_artifacts = true;
            $scope.selectedVersionsModel = [];
            $http.get('artifacts/versions?product='+$scope.product).success(function (data) {
                $scope.versions = data;
            });
        };

        $scope.deleteVersions = function () {
            var answer = window.confirm('This will purge the selected versions from system. Sure?');
            if (answer) {
                $http.delete('artifacts?product=' +$scope.product + '&version=' + $scope.selectedVersionsModel).success(function (data) {
                    $scope.selectedVersionsModel = [];
                    $scope.product = {};
                    $scope.versions = [];
                });
            }
        };

    }
]).run(['$templateCache', function ($templateCache) { // template for modules popover
    $templateCache.put('template/deployment/modules.html',
        '<span class="modules-label label-danger" ng-repeat="module in installation.info[\'Modules\']">{{ module }} </span>'
    );
}]).run(['$templateCache', function ($templateCache) { // template for custom modules popover
    $templateCache.put('template/deployment/custom.html',
        '<span class="modules-label label-warning" ng-repeat="module in installation.info[\'Custom Modules\']">{{ module }} </span>'
    );
}]).run(['$templateCache', function ($templateCache) { // template for comment modal
    $templateCache.put('template/deployment/comment.html',
        '<div class="modal-header"><h3 class="modal-title">Comment artifact {{ artifact.name }}</h3></div>' +
        '<div class="modal-body">' +
        '<textarea rows="6" style="width: 100%" data-ng-model="comment"></textarea>' +
        '</div>' +
        '<div class="modal-footer">' +
        '<button class="btn btn-primary" data-ng-click="ok()">Ok</button>' +
        '</div>' +
        '</div>');
}]);
