'use strict';

angular.module('deployment').controller('ChangeSetController', ['$scope', 'artifactName', 'artifact', 'version', 'host', 'installations', 'max_artifacts', 'all_artifacts', '$http', 'Authentication', 'growl', '$splash',
    function ($scope, artifactName, artifact, version, host, installations, max_artifacts, all_artifacts, $http, Authentication, growl, $splash) {

        $scope.version = version;
        $scope.artifact = {};
        $scope.artifactPage = artifact;
        $scope.index = 0;
        $scope.artifactCount = 0;
        $scope.artifactsCommits = [];

        $scope.hasprev = true;
        $scope.hasnext = true;

        $scope.account = Authentication.account;

        var count = function () {
            $scope.artifactCount = 0;
            $scope.artifactsCommits = [];
            if ($scope.artifact.artifacts[$scope.index].changesets.length > 0) {
                $scope.artifact.artifacts[$scope.index].changesets.forEach(function (changeset, changesetIndex) {
                    $scope.artifactCount = $scope.artifactCount + changeset.commitCount;
                    Array.prototype.push.apply($scope.artifactsCommits, changeset.commits);
                });
            }
            $scope.hasnext = (typeof $scope.artifact.artifacts[$scope.index + 1] !== 'undefined');
            $scope.hasprev = (typeof $scope.artifact.artifacts[$scope.index - 1] !== 'undefined');
        };

        $scope.next = function () {
            $scope.index++;
            count();
        };

        $scope.prev = function () {
            $scope.index--;
            count();
        };

        // return
        $scope.updateJiraStatus = function () {
            var data = {'commits': $scope.artifactsCommits};
            $http.post('/jira/issue/status', data).success(function (cb) {
                $scope.artifactsCommits = cb;
            }).error(function (error) {
                growl.error(error);
            });
        };

        // approve artifact
        $scope.approve = function (artifact) {
            $scope.artifactPage.searched = false;
            var artifactPageIndex = $scope.artifactPage.artifacts.findIndex(function (x) {
                return x.name === artifact.name;
            });
            var data = {'name': artifact.name};
            $http.put('/artifact/approve', data).success(function (data) {
                artifact.approved = true;
                artifact.approvedBy = $scope.account.displayName;
                if (typeof artifactPageIndex !== 'undefined') {
                    $scope.artifactPage.artifacts[artifactPageIndex].approved = true;
                    $scope.artifactPage.artifacts[artifactPageIndex].approvedBy = $scope.account.displayName;
                    $scope.artifactPage.searched = true;
                }
            });
        };

        // start artifact deployment
        $scope.deploy = function (artifactName) {

            var answer = window.confirm('Sure?');
            if (answer) {
                $splash.open({
                    artifact: $scope.artifact.artifacts[$scope.index],
                    host: host,
                    installations: installations
                }, {
                    templateUrl: 'modules/deployment/views/deployment-splash.client.view.html'
                });
            }
        };

        $scope.init = function () {
            $http.get('artifacts?start=0&version=' + $scope.version + '&max=0&all=' + all_artifacts).success(function (data) {
                $scope.artifact.artifacts = data.artifacts;
                $scope.artifactCount = data.total;
                $scope.index = $scope.artifact.artifacts.findIndex(function (x) {
                    return x.name === artifactName;
                });
                count();
            });
        };

        $scope.init();

    }
]);
