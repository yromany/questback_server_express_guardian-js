'use strict';

function replaceAll(txt, replace, with_this) {
    return txt.replace(new RegExp(replace, 'g'), with_this);
}

angular.module('deployment').filter('nodomain', [
    function () {
        return function (input) {
            input = input.replace('.qb-feedback.com', '');
            input = input.replace('.3uu.de', '');
            return input;
        };
    }
]).filter('trust', ['$sce', function($sce) {
    return function(htmlCode){
        return $sce.trustAsHtml(htmlCode);
    };
}]);