'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.
		state('index.profile', {
		  url: '/settings/profile',
      views: {
        'content': {
          templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
        }
      }
		}).
		state('index.first-login', {
      url: '/settings/first-login',
      views: {
        'content': {
          templateUrl: 'modules/users/views/settings/first-login.client.view.html'
        }
      }
    });
	}
]);