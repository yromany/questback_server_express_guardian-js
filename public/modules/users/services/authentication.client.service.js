'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', [
    function () {
        var _this = this;

        if ((typeof(_this._data) === 'undefined')) {
            _this._data = {
                account: window.account
            };
            window.account = ''; // erase user data to avoid manipulations
        }

        return _this._data;
    }
]);