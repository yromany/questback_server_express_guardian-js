'use strict';

angular.module('users').controller('FailedJobsController', ['$scope', 'jobs',
    function ($scope, jobs) {

        $scope.jobs = jobs;

        $scope.ok = function () {
            window.location = '/';
        };

    }
]);
