'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$modal',
    function ($scope, $http, $modal) {

        $scope.failedJobsTemplate = 'template/jenkins/failedjobs.html';

        $scope.signin = function () {
            $http.post('/auth/signin', $scope.credentials).success(function (response) {
                $scope.credentials = null;

                // just check if there are failing jenkins jobs if the user is a QA member
                if (!response.isQA) {
                    window.location = '/';
                } else {
                    $http.get('/jenkins/build/failed').success(function (data) {
                        if (data.length > 0) {
                            $modal.open({
                                templateUrl: $scope.failedJobsTemplate,
                                controller: 'FailedJobsController',
                                resolve: {
                                    jobs: function () {
                                        return data;
                                    }
                                }
                            });
                        } else {
                            window.location = '/';
                        }
                    });
                }

            }).error(function (response) {
                $scope.error = response;
            });
        };
    }
]).run(['$templateCache', function ($templateCache) { // template for comment modal
    $templateCache.put('template/jenkins/failedjobs.html',
        '<div class="modal-header"><h3 class="modal-title">Oh, oh!  we have failed jenkins jobs    <i class="fa fa-frown-o"></i></h3></div>' +
        '<div class="modal-body">' +
        '<p> Hey there, nice to see you again. It seems that we have some failing jenkins jobs</p>' +
        '<span> Please show some love <i class="fa fa-heart"></i> and see if you can help: </span>' +
        '<p></p>' +
        '<span data-ng-repeat="job in jobs"><a href="{{ job.url }}" target="_blank"> {{ job.job }}</a><span>' +
        '</div>' +
        '<div class="modal-footer">' +
        '<button class="btn btn-primary" data-ng-click="ok()">Ok, I know</button>' +
        '</div>' +
        '</div>');
}]);
