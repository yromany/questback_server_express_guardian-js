'use strict';

angular.module('users').controller('AvatarController', ['$scope', '$http', '$location', 'Authentication', 'socket',
    function ($scope, $http, $location, Authentication, socket) {

        $scope.account = Authentication.account;
        $scope.image = 'modules/users/img/avatars/' + $scope.account.avatar + '?' + window.moment().unix();
        $scope.avatarChanged = false;

        $scope.avatars = ['america.png', 'barret.png', 'blanka.png', 'chunli.png', 'clone.png', 'cloud.png', 'colossus.png',
            'darth-vader.png', 'default.png', 'dhalsim.png', 'doll.png', 'evilred.png', 'hellboy.png', 'ironman.png', 'ironman2.png',
            'johny.png', 'joker.png', 'logan.png', 'magneto.png', 'mummy.png', 'ood.png', 'ryu.png', 'scream.png', 'silver.png',
            'slasher.png', 'spiderwoman.png', 'superman.png', 'thor.png', 'tigger.png', 'smith.png', 'universe.png', 'venon.png',
            'war1.png', 'war2.png', 'war3.png', 'woody.png', 'zangief.png'];

        $scope.setAvatar = function (avatar) {
            $scope.image = 'modules/users/img/avatars/' + avatar;
            $scope.account.avatar = avatar;
            $scope.avatarChanged = true;
        };

        $scope.firstLogin = function () {
            // either the user has changed the avatar or he has droped a file on the drop area
            if ($scope.avatarChanged || $scope.image.includes('data:image/png;base64')) {
                var config = {
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                };
                var data = window.$.param({img: $scope.image});
                $http.post('/settings/firstLogin', data, config).success(function (response) {
                    $scope.account.avatar = $scope.account.userName + '.png';
                    socket.emit('user:avatarUpdate', {
                        user: $scope.account
                    });
                    $scope.account.firstLogin = false;
                }).error(function (response) {
                    $scope.error = response;
                });
            } else {
                $scope.account.firstLogin = false;
            }
        };
    }
]);
