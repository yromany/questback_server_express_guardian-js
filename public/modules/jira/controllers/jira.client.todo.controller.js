'use strict';

angular.module('core').controller('TodoController', ['$scope', '$http', 'System', 'socket',
    function ($scope, $http, System, socket) {

        $scope.conf = System.conf();

        $scope.todosStarted = false;
        $scope.todos = {};
        $scope.perPageTodos = 6;
        $scope.totalTodos = 0;
        $scope.currentTodoPage = 0;

        // initialize qa todo list
        var initTodos = function () {
            $scope.todosStarted = false;
            $scope.currentTodoPage = 0;
            $http.get('jira/todos?start=0&max=6').success(function (data) {
                $scope.todos = data.issues;
                $scope.totalTodos = data.total;
                $scope.todosStarted = true;
            });
        };

        $scope.init = function () {
            initTodos();
        };

        $scope.init();

        $scope.pageTodoChanged = function () {
            $scope.todosStarted = false;
            var start = $scope.perPageTodos * $scope.currentTodoPage - $scope.perPageTodos;
            $http.get('jira/todos?start=' + start + '&max=6').success(function (data) {
                $scope.todos = data.issues;
                $scope.todosStarted = true;
            });
        };

        socket.on('config:refresh', function (data) {
            initTodos();
        });

    }
]);
