'use strict';

angular.module('jira').controller('JiraConfigController', ['$scope', 'Authentication', '$http',
  function($scope, Authentication, $http) {
    $scope.jira = {};
    $scope.jira.task = {};
    
    $scope.account = Authentication.account; 
    
    var types = ['warning', 'info', 'success', 'danger'];
    
    // initialize build status
    var initJiras = function() {
      $http.get('jira/block/config').success(function(data) {
        if (data.length === 4) {
          angular.forEach(data, function(value, key) {
            $scope.jira[value.type] = value;
            $scope.jira[value.type].loaded = true;
          });
        } else {
          angular.forEach(types, function(value, key) {
            $scope.jira[value] = {};
            $scope.jira[value].title = 'Not defined';
            $scope.jira[value].jql = 'Not defined';
            $scope.jira[value].loaded = true;
          });
          angular.forEach(data, function(value, key) {
            $scope.jira[value.type] = value;
            $scope.jira[value.type].loaded = true;
          });        
        }
      });
      
      $http.get('jira/task/config').success(function(data) {
        $scope.jira.task.jql = 'Not defined';
        if (data.jql)  {
          $scope.jira.task.jql = data.jql;
        } 
        $scope.jira.task.loaded = true;
      });
    };
     
    $scope.init = function() {
      initJiras();
    };
    
    $scope.init();
    
    $scope.updateBox = function(type) {
      $scope.jira[type].loaded = false;
      if ($scope.jira[type].jql.length > 0) {
        $http.put('jira/block/update', {jira : $scope.jira[type], type : type}).success(function(data) {
          $scope.jira[type].loaded = true;
        }).error(function(data){
          $scope.jira[type].loaded = true;
          alert(data);
        });
      } else {
        alert('No JQL? Who are you? A tester?');
        $scope.jira[type].loaded = true;
      }
    };
    
    $scope.updateTask = function(type) {
      $scope.jira.task.loaded = false;
      if ($scope.jira.task.jql.length > 0) {
        $http.put('jira/task/update', {jql : $scope.jira.task.jql}).success(function(data) {
          $scope.jira.task.loaded = true;
        }).error(function(data){
          $scope.jira.task.loaded = true;
          alert(data);
        });
      } else {
        alert('No JQL? Who are you? A tester?');
        $scope.jira.task.loaded = true;
      }
    };
    
    $scope.setDefaultBoxes = function() {
      $scope.jira.warning.title = 'Open Support Tickets';
      $scope.jira.warning.jql = 'project = "K@nban" AND status != Closed AND "Support Request" = yes and assignee = gp-schnitte';
      $scope.jira.success.title = 'QA Closed Tickets in This Year';
      $scope.jira.success.jql = 'category = "QA Germany" AND status = Closed AND assignee in membersOf("QA Germany") and updated <= endOfYear() AND updated >= startOfYear()';
      $scope.jira.info.title = 'Kanban Ready For Test';
      $scope.jira.info.jql = 'project = KANBAN AND status = Resolved AND assignee in membersOf("QA Germany")';
      $scope.jira.danger.title = 'Critical Bugs';
      $scope.jira.danger.jql = 'project = "K@nban" AND status != Closed AND priority = "Priority 1"';
      
      angular.forEach(types, function(value, key) {
        $scope.updateBox(value);
      });
      
    };
    
    $scope.setDefaultTask = function() {
      $scope.jira.task.jql = 'category = "QA Germany" AND status != Closed AND assignee = ' + $scope.account.userName;
      $scope.updateTask();
    };
    
    
  }
]);