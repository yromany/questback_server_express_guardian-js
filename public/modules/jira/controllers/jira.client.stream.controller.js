'use strict';

angular.module('core').controller('StreamController', ['$scope', '$http', 'socket',
    function ($scope, $http, socket) {

        $scope.streamStarted = false;
        $scope.stream = {};
        $scope.currentStreamMax = 5;
        $scope.showContent = true;
        $scope.showLessButton = false;
        $scope.showMoreButton = false;
        $scope.streamConfig = false;

        $scope.availableProjects = [];
        $scope.myProjects = {};
        $scope.myProjects.projects = [];

        $scope.streamConfigStart = function () {
            $scope.streamConfig = !($scope.streamConfig);
        };

        $scope.streamConfigSave = function () {
            console.log($scope.showContent);
            $scope.streamConfig = false;
            $http.put('jira/stream/update', {
                projects: $scope.myProjects.projects,
                limit: $scope.currentStreamMax,
                showContent: $scope.showContent
            }).success(function (data) {
                initStream();
            });
        };

        // initialize activity stream
        var initStream = function () {
            $scope.streamStarted = false;
            $scope.showLessButton = false;
            $scope.showMoreButton = false;
            $scope.stream = [];
            $http.get('jira/stream/config').success(function (data) {
                $scope.availableProjects = data.availableprojects;
                $scope.myProjects.projects = data.myprojects;
                $scope.currentStreamMax = data.mylimit;
                $scope.showContent = data.showcontent;
                $http.get('jira/stream?max=' + $scope.currentStreamMax + '&projects=' + $scope.myProjects.projects).success(function (data) {
                    $scope.stream = data;
                    if ($scope.stream.length > 0) {
                        $scope.showMoreButton = true;
                    }
                    if ($scope.stream.length > 5) {
                        $scope.showLessButton = true;
                    }
                    $scope.streamStarted = true;
                });
            });
        };

        $scope.init = function () {
            initStream();
        };

        $scope.init();

        $scope.streamMore = function () {
            $scope.streamStarted = false;
            $scope.currentStreamMax = $scope.currentStreamMax + 5;
            $http.get('jira/stream?max=' + $scope.currentStreamMax + '&projects=' + $scope.myProjects.projects).success(function (data) {
                $scope.stream = data;
                $scope.streamStarted = true;
                $scope.showLessButton = true;
            });
        };

        $scope.streamLess = function () {
            $scope.streamStarted = false;
            $scope.currentStreamMax = $scope.currentStreamMax - 5;
            $http.get('jira/stream?max=' + $scope.currentStreamMax + '&projects=' + $scope.myProjects.projects).success(function (data) {
                $scope.stream = data;
                $scope.streamStarted = true;
                if ($scope.currentStreamMax === 5) {
                    $scope.showLessButton = false;
                }
            });
        };

        socket.on('stream:refresh', function (data) {
            initStream();
        });

        socket.on('config:refresh', function (data) {
            initStream();
        });

    }
]);
