'use strict';

angular.module('jira').controller('JiraHomeController', ['$scope', '$http', 'socket',
    function ($scope, $http, socket) {

        var types = ['warning', 'info', 'success', 'danger'];

        $scope.jira = {};

        // initialize jira blocks
        var initJiras = function () {
            angular.forEach(types, function (value, key) {
                $scope.jira[value] = {};
                $scope.jira[value].title = 'Loading...';
                $scope.jira[value].count = 'Loading...';
            });

            $http.get('jira/blocks').success(function (data) {
                if (data.length === 4) {
                    angular.forEach(data, function (value, key) {
                        $scope.jira[value.type] = value;
                        $scope.jira[value.type].loaded = true;
                    });
                } else {
                    angular.forEach(types, function (value, key) {
                        $scope.jira[value] = {};
                        $scope.jira[value].title = 'Not defined';
                        $scope.jira[value].count = 'Not defined';
                        $scope.jira[value].loaded = true;
                    });
                    angular.forEach(data, function (value, key) {
                        $scope.jira[value.type] = value;
                        $scope.jira[value.type].loaded = true;
                    });
                }
            });
        };

        $scope.init = function () {
            initJiras();
        };

        $scope.init();

        socket.on('config:refresh', function (data) {
            initJiras();
        });


    }
]);