'use strict';

angular.module('jira').directive('todoText', function() {
  return {
    template: '{{todo.fields.summary.substring(0,70)}}'
  };
});