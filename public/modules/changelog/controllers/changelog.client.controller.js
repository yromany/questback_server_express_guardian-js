'use strict';

angular.module('changelog').controller('ChangelogController', ['$scope', '$http',
    function ($scope, $http) {
        $scope.product = {};
        $scope.versions = [];
        $scope.changelogs = [];

        $scope.getVersions = function () {
            $scope.changelogs = [];
            $http.get('artifacts/versions?product='+$scope.product).success(function (data) {
                $scope.versions = data;
            });
        };

        $scope.getChangelog = function (version) {
            $http.get('changelogs/'+$scope.product+'/'+version).success(function (data) {
                $scope.changelogs = data;
            });
        };
    }
]);