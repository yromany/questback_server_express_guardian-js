'use strict';

angular.module('configuration').controller('MaintenanceController', ['$scope', 'message', '$modalInstance', '$http',
    function ($scope, message, $modalInstance, $http) {

        $scope.message = message;

        $scope.ok = function () {
            $http.put('/settings/announcement/read').success(function (data) {
                $modalInstance.close();
            });
        };

    }
]);