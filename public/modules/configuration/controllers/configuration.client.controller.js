'use strict';

angular.module('configuration').controller('ConfigurationController', ['$scope', '$http', 'System', '$location', 'Authentication',
    function ($scope, $http, System, $location, Authentication) {

        $scope.account = Authentication.account;

        var helper = (JSON.parse(JSON.stringify(System.conf())));
        delete helper._id;
        delete helper.__v;
        helper.jiraProjectUserPass = '*****';
        helper.jiraGlobalUserPass = '*****';
        helper.jenkins2password = '*****';
        helper.artifactoryPassword = '*****';
        $scope.conf = helper;
        $scope.btnStatus = 'disabled';
        $scope.btnStatusLabel = 'Update feature definition';
        $scope.btnCoverageStatus = 'disabled';
        $scope.statusResult = 'Hey ho! Please check the version status first.';
        $scope.coverageStatusResult = 'Hey ho! Please select the coverage file.';
        $scope.coverage = '';
        $scope.maintenance = {
            message : ''
        };

        //########### COVERAGE ##############

        // handle on load coverage file
        $scope.handler = function (e, files) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $scope.coverage = reader.result;
                $scope.btnCoverageStatus = '';
                $scope.coverageStatusResult = 'Ok. Lets update the coverage.';
                $scope.$apply();
            };
            reader.readAsText(files[0]);
        };

        // update coverage collection
        $scope.updateCoverage = function () {
            $scope.coverageStatusResult = 'Updating coverage...';
            return $http.post('/configuration/coverage/update', {coverage: $scope.coverage}).success(function (data) {
                $scope.coverageStatusResult = 'Yep. Everything should be up to date now.';
            });
        };

        //########### FEATURES ##############

        // check for version status
        $scope.checkFeatureStatus = function () {
            $scope.statusResult = 'Checking status...';
            $scope.btnStatus = 'disabled';
            return $http.get('/configuration/features/status').success(function (data) {
                $scope.statusResult = data;
                if (data.indexOf('no changes found') === -1) {
                    $scope.btnStatus = '';
                    if (data.indexOf('check out') > 0) {
                        $scope.btnStatusLabel =  'Checkout webdriver repository';
                    }
                } else {
                    $scope.btnStatusLabel =  'Update feature definition';
                }
            });
        };

        // update features
        $scope.updateFeatures = function () {
            $scope.statusResult = 'Updating features...';
            $scope.btnStatus = 'disabled';
            return $http.post('/configuration/features/update').success(function (data) {
                $scope.statusResult = 'Horray! Feature files are up to date.';
                $scope.btnStatusLabel =  'Update feature definition';
            });
        };

        //########### GLOBAL CONFIG ##############

        // update a single gloabal variable
        $scope.updateConfig = function (item, value) {
            var data = {};
            data[item] = value;
            return $http.post('/configuration/system/update', {conf: data, id: System.conf()._id});
        };

        //########### DEPLOYMENT HOSTS ##############

        // get the list of deployment hosts
        var getHosts = function() {
            $http.get('/configuration/hosts').success(function (data) {
                $scope.hosts = data;
            });
        };

        // create a new deployment host
        $scope.newDeployHost = function () {
            $http.post('/configuration/hosts').success(function (data) {
                getHosts();
            });
        };

        // update a deployment host
        $scope.updateHost = function (host) {
            $http.put('/configuration/hosts/'+host._id, host).success(function (data) {
            });
        };

        // delete a deployment host
        $scope.deleteHost = function (host) {
            $http.delete('/configuration/hosts/'+host._id).success(function (data) {
                getHosts();
            });
        };

        //########### USER MANAGEMENT ##############

        // get the list of deployment hosts
        var getUsers = function() {
            $http.get('/configuration/users').success(function (data) {
                $scope.users = data;
            });
        };

        // update a user
        $scope.updateUser = function (user) {
            $http.put('/configuration/users/'+user._id, user).success(function (data) {
            });
        };

        // delete a user
        $scope.deleteUser = function (user) {
            $http.delete('/configuration/users/'+user._id).success(function (data) {
                getUsers();
            });
        };

        // send maintenance message
        $scope.sendMaintenanceMessage = function () {
            var data = {message: $scope.maintenance.message};
            if (data.message !== '') {
                $http.post('/configuration/maintenance', data).success(function (data) {
                });
            } else return 'Maintenance message with no message? :-/';
        };

    }
]);
