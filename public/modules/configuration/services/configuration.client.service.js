'use strict';

// Authentication service for user variables
angular.module('configuration').service('System', function() {
    var conf = window.conf;
    this.conf = function () {
        return conf;
    };
    this.setConf = function (newConfig) {
        conf = newConfig;
    };
});

