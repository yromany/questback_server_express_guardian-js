'use strict';

module.exports = function(app, io) {
    var users = require('../../app/controllers/users'),
        artifact = require('../../app/controllers/artifacts');

    // register new build artifact
    app.route('/artifact').post(artifact.register(io));

    // approve build artifact
    app.route('/artifact/approve').put(users.ensureLoggedIn, artifact.approve);

    // decline build artifact
    app.route('/artifact/decline').put(users.ensureLoggedIn, artifact.decline);

    // comment build artifact
    app.route('/artifact/revoke').put(users.ensureLoggedIn, artifact.revoke);

    // comment build artifact
    app.route('/artifact/comment').put(users.ensureLoggedIn, artifact.comment);

    // update build artifact
    app.route('/artifacts').get(users.ensureLoggedIn, artifact.artifacts);

    // update build artifact
    app.route('/artifacts/versions').get(users.ensureLoggedIn, artifact.versions);

    // promote build artifact
    app.route('/artifact/promote').put(users.ensureLoggedIn, artifact.promote(io));

    // archive old artifacts
    app.route('/artifacts/archive').put(artifact.archive);

    // archive old artifacts
    app.route('/artifacts').delete(artifact.deleteVersions);

    //######## ARTIFACTORY ############

    // discard old artifactory artifacts
    app.route('/artifacts/artifactory/discard').put(artifact.discard);


};
