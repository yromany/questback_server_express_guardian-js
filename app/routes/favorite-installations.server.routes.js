'use strict';

module.exports = function(app) {

    var users = require('../../app/controllers/users'),
        favinst = require('../../app/controllers/favorite-installations');

    // add installation to favorites
    app.route('/user/favorite/installation').post(users.ensureLoggedIn, favinst.update);

    // return favorite installations
    app.route('/user/favorite/installations').get(users.ensureLoggedIn, favinst.list);

    // delete favorite installation
    app.route('/user/favorite/installation/:name').delete(users.ensureLoggedIn, favinst.delete);

};