'use strict';

module.exports = function (app, io) {
    var users = require('../../app/controllers/users'),
        jira = require('../../app/controllers/jira');

    // jira personal blocks
    app.route('/jira/block/update').put(users.ensureLoggedIn, jira.updateBlock);
    app.route('/jira/block/config').get(users.ensureLoggedIn, jira.blockJqls);
    app.route('/jira/blocks').get(users.ensureLoggedIn, jira.blocks);

    // jira my taks
    app.route('/jira/task/config').get(users.ensureLoggedIn, jira.taskJql);
    app.route('/jira/task/update').put(users.ensureLoggedIn, jira.updateTask);
    app.route('/jira/tasks').get(users.ensureLoggedIn, jira.tasks);

    // jira qa todos
    app.route('/jira/todos').get(users.ensureLoggedIn, jira.todos);

    // jira personal streams
    app.route('/jira/stream').get(users.ensureLoggedIn, jira.stream);
    app.route('/jira/stream/config').get(users.ensureLoggedIn, jira.streamConfig);
    app.route('/jira/stream/update').put(users.ensureLoggedIn, jira.streamConfigUpdate);
    app.route('/jira/stream/refresh').post(jira.streamRefresh(io));

    // jira get issue status
    app.route('/jira/issue/status').post(users.ensureLoggedIn,jira.getJiraStatus);

    // jira get issue status
    //app.route('/jira/webhook/closed').post(jira.webHookClosed);

};