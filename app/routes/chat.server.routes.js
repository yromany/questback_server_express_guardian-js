'use strict';

module.exports = function (app) {
    var users = require('../../app/controllers/users'),
        chat = require('../../app/controllers/chat/chat');

    // get chat messages
    app.route('/chat/messages').get(users.ensureLoggedIn, chat.messages);

};