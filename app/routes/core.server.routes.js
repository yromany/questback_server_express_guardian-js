'use strict';

module.exports = function (app) {
    // Root routing
    var users = require('../../app/controllers/users'),
        core = require('../../app/controllers/core'),
        feature = require('../../app/controllers/feature');

	// home
    app.route('/').get(users.ensureLoggedIn, core.index);

    // login
    app.route('/login').get(core.login);

};