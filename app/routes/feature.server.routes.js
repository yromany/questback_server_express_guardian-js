'use strict';

module.exports = function (app) {
    var users = require('../../app/controllers/users'),
        feature = require('../../app/controllers/feature');

    // read all features
    app.route('/features').get(users.ensureLoggedIn, feature.list);

    // read one feature
    app.route('/feature').post(users.ensureLoggedIn, feature.read);

};