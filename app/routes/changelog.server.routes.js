'use strict';

module.exports = function(app) {
    var users = require('../../app/controllers/users'),
        changelog = require('../../app/controllers/changelog');

    // register new build artifact
    app.route('/changelogs/:product/:version').get(changelog.list);

};