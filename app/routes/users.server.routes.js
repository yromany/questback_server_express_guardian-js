'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport');

module.exports = function (app) {
    // User Routes
    var users = require('../../app/controllers/users');

    // Setting up the users authentication api
    app.route('/auth/signin').post(users.signin);
    app.route('/auth/signout').get(users.signout);

    //### USER SETTINGS ###

    // set inital user avatar
    app.route('/settings/firstlogin').post(users.requiresLogin, users.firstLogin);

    // set chat messages limit
    app.route('/settings/chat/limit').put(users.ensureLoggedIn, users.chatLimit);

    // set chat from date
    app.route('/settings/chat/from').put(users.ensureLoggedIn, users.chatFrom);

    // set widgets positioning
    app.route('/settings/widgets').put(users.ensureLoggedIn, users.widgets);

    // set widgets layout
    app.route('/settings/widgets/sections').put(users.ensureLoggedIn, users.widgetSections);
    // set sidebar (on/off)
    app.route('/settings/sidebar').put(users.ensureLoggedIn, users.sidebar);

    // set sidebar (on/off)
    app.route('/settings/favorite/inst').put(users.ensureLoggedIn, users.favoriteInst);

    // set announcement as readed
    app.route('/settings/announcement/read').put(users.ensureLoggedIn, users.readAnnouncement);

    // Finish by binding the user middleware
    app.param('userId', users.userByID);
};
