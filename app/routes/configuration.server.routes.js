'use strict';

module.exports = function(app, io) {
    var users = require('../../app/controllers/users'),
        configuration = require('../../app/controllers/configuration'),
        deployHost = require('../../app/controllers/deploy-hosts'),
        maintenance = require('../../app/controllers/maintenance');

    //########## CONFIG SYSTEM GLOBAL VARIABLES #############

    // update global system configuration variables
    app.route('/configuration/system/update').post(users.ensureLoggedIn,users.ensureIsAdmin, configuration.update(io));

    //########## CONFIG FEATURES ###########

    // check version status from feature files
    app.route('/configuration/features/status').get(users.ensureLoggedIn,users.ensureIsAdmin, configuration.statusFeature);
    // update feature file definition
    app.route('/configuration/features/update').post(users.ensureLoggedIn,users.ensureIsAdmin, configuration.updateFeature);

    //######## CONFIG COVERAGE ############

    // update coverage collection
    app.route('/configuration/coverage/update').post(users.ensureLoggedIn,users.ensureIsAdmin, configuration.updateCoverage);

    //######## CONFIG DEPLOY HOSTS ############

    //create a new host
    app.route('/configuration/hosts').post(users.ensureLoggedIn,users.ensureIsAdmin,deployHost.create);

    // update a host
    app.route('/configuration/hosts/:id').put(users.ensureLoggedIn,users.ensureIsAdmin,deployHost.update);

    // delete a host
    app.route('/configuration/hosts/:id').delete(users.ensureLoggedIn,users.ensureIsAdmin,deployHost.delete);

    // get hosts
    app.route('/configuration/hosts').get(users.ensureLoggedIn,deployHost.list);

    //######## USER MANAGEMENT ############

    // get users
    app.route('/configuration/users').get(users.ensureLoggedIn,users.ensureIsAdmin,users.listUsers);

    // update a user
    app.route('/configuration/users/:id').put(users.ensureLoggedIn,users.ensureIsAdmin,users.update);

    // update a user
    app.route('/configuration/users/:id').delete(users.ensureLoggedIn,users.ensureIsAdmin,users.delete);

    //######## MAINTENANCE MESSAGE ############

    // send maintenance message to all users
    app.route('/configuration/maintenance').post(users.ensureLoggedIn,users.ensureIsAdmin, maintenance.send(io));

};
