'use strict';

module.exports = function (app) {
    var users = require('../../app/controllers/users'),
        coverage = require('../../app/controllers/coverage');

    // get chat messages
    app.route('/coverage').get(coverage.read);
};