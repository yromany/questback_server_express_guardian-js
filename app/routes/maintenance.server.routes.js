'use strict';

module.exports = function(app) {
    var users = require('../../app/controllers/users'),
        maintenance = require('../../app/controllers/maintenance');

    // get maintenance message
    app.route('/maintenance/message').get(users.ensureLoggedIn, maintenance.read);
};