'use strict';

module.exports = function(app) {
    var changeset = require('../../app/controllers/changeset');

    // register new build artifact
    app.route('/changeset').post(changeset.create);
};