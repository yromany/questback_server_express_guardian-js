'use strict';

module.exports = function (app, io) {
    var users = require('../../app/controllers/users'),
        jenkins = require('../../app/controllers/jenkins');

    // get defined jenkins builds
    app.route('/jenkins/builds').get(users.ensureLoggedIn, jenkins.list);

    // create jenkins build
    app.route('/jenkins/build').post(users.ensureLoggedIn, jenkins.create);

    // delete jenkins build
    app.route('/jenkins/build/:id').delete(users.ensureLoggedIn, jenkins.delete);

    // change builds position
    app.route('/jenkins/build/position').put(users.ensureLoggedIn, jenkins.update);

    // get pipeline stages
    app.route('/jenkins/build/stages').get(users.ensureLoggedIn, jenkins.stages);

    // get jenkins webdriver test report
    app.route('/jenkins/testreport').get(users.ensureLoggedIn, jenkins.testReport);

    // send message that a build failed to all logged users
    app.route('/jenkins/build/failed').post(jenkins.failed(io));

    // send message that a build has been fixed to all logged users
    app.route('/jenkins/build/fixed').post(jenkins.fixed(io));

    // send message that a build has been fixed to all logged users
    app.route('/jenkins/build/failed').get(jenkins.failedJobs);
};
