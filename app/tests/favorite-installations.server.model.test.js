'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	FavoriteInstallations = mongoose.model('FavoriteInstallations');

/**
 * Globals
 */
var user, favoriteInstallations;

/**
 * Unit tests
 */
describe('Favorite installations Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});

		user.save(function() { 
			favoriteInstallations = new FavoriteInstallations({
				// Add model fields
				// ...
			});

			done();
		});
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return favoriteInstallations.save(function(err) {
				should.not.exist(err);
				done();
			});
		});
	});

	afterEach(function(done) { 
		FavoriteInstallations.remove().exec();
		User.remove().exec();

		done();
	});
});