'use strict';

/**
 * Module dependencies.
 */
var DeployHost = require('mongoose').model('DeployHosts'),
    _ = require('lodash');

/**
 * Create a Deploy host
 */
exports.create = function(req, res) {

    var host = new DeployHost();
    host.hostName = 'new host';
    host.hostURL = 'http://new-host.invalid';

    host.save(function(err,cb) {
        if (err) {
            console.error('\x1b[31m', err);
            res.status(400).send(err);
        } else {
            res.send(cb);
        }
    });
};

/**
 * Show the current Deploy host
 */
exports.read = function(req, res) {

};

/**
 * Update a Deploy host
 */
exports.update = function(req, res) {
    DeployHost.update({_id: req.params.id}, req.body, function(err, cb) {
        if (err) {
            console.error('\x1b[31m', err);
            res.status(400).send(err);
        } else {
            res.send(cb);
        }
    });
};

/**
 * Delete an Deploy host
 */
exports.delete = function(req, res) {
    DeployHost.remove({_id: req.params.id}, function(err,cb) {
        if (err) {
            console.error('\x1b[31m', err);
            res.status(400).send(err);
        } else {
            res.send('ok');
        }
    });
};

/**
 * List of Deploy hosts
 */
exports.list = function(req, res) {
    DeployHost.find({}).exec(function (err, obj) {
        if (err) {
            console.error('\x1b[31m', err);
            res.status(400).send(err);
        } else {
           res.send(obj);
        }
    });
};