'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Changelog = mongoose.model('Changelog'),
    _ = require('lodash');

/**
 * List of Changelogs
 */
exports.list = function(req, res) {

    var product = req.params.product || 'EFS',
        version = req.params.version || 'DEV';

    Changelog.find({product: product, version: version}).sort({date: 'desc'}).exec(function (err, obj) {
        if (err) {
            console.error('\x1b[31m', err);
            res.status(400).send(err);
        } else {
            res.send(obj);
        }
    });

};