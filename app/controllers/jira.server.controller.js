'use strict';
/**
 * Module dependencies.
 */
var _ = require('lodash'),
    system = require('../../config/config'),
    JiraBox = require('mongoose').model('JiraBox'),
    JiraTask = require('mongoose').model('JiraTask'),
    JiraProject = require('mongoose').model('JiraProject'),
    FeedParser = require('feedparser'),
    Request = require('request');

/**
 * Update Jira block JQls
 */
exports.updateBlock = function (req, res) {
    system.privateConf.jira.searchJira(req.body.jira.jql, {}, function (error, issue) {
        if (error) {
            res.status(400).send(error);
        } else {
            JiraBox.update({
                userName: req.user.userName,
                type: req.body.type
            }, req.body.jira, {upsert: true}, function (err, cb) {
                res.send('ok');
            });
        }
    });
};

/**
 * Get activity stream
 * Exclude hgbot and testlink users from the feed
 */
exports.stream = function (req, res) {

    var projects = req.query.projects.replace(/,/g, '+');
    var request = new Request({
            url: 'https://' + system.conf().jiraURL + '/activity?maxResults=' + req.query.max + '&streams=key+IS+' + projects + '&streams=user+NOT+hgbot+&streams=user+NOT+testlink+&os_authType=basic',
            auth: {
                'user': system.conf().jiraGlobalUserName,
                'pass': system.privateConf.jiraGlobalUserPass,
                'sendImmediately': false
            },
            strictSSL: false
        }),
        feedparser = new FeedParser({addmeta: false});

    var activities = [];

    request.on('error', function (error) {
        res.status(400).send(error);
    });

    request.on('response', function (response) {
        var stream = this;
        if (response.statusCode !== 200) return this.emit('error', new Error('Bad status code'));
        stream.pipe(feedparser);
    });

    feedparser.on('error', function (error) {
        res.status(400).send(error);
    });

    feedparser.on('readable', function () {
        var stream = this, meta = this.meta, item;
        var obj = {};
        while ((item = stream.read()) !== null) {
            //only returns the relevant fields

            obj = {};
            obj.title = item['atom:title']['#'];

            // if the text is too long we always prefer the summary
            if (typeof item['atom:summary'] !== 'undefined') {
                obj.content = item['atom:summary']['#'];
            } else if (typeof item['atom:content'] !== 'undefined') {
                obj.content = item['atom:content']['#'];
            }

            obj.date = item.date;
            obj.avatar = item['atom:author'].link[0]['@'].href;
            activities.push(obj);
        }
    });

    feedparser.on('end', function () {
        res.send(activities);
    });

};

/**
 * Get activity steam config
 */
exports.streamConfig = function (req, res) {
    var obj = {};
    obj.myprojects = [];
    obj.availableprojects = [];
    system.privateConf.jiraCervus.listProjects(function (error, projects) {
        if (error) {
            console.error('\x1b[31m', error);
            res.status(400).send(error);
        } else {
            for (var index in projects) {
                obj.availableprojects.push(projects[index].key);
            }
            JiraProject.find({
                userName: req.user.userName
            }).exec(function (err, cb) {
                if (err) {
                    console.error('\x1b[31m', err);
                    res.status(400).send(err);
                }
                if (cb[0]) {
                    obj.myprojects = cb[0].projects;
                    obj.mylimit = cb[0].limit;
                    obj.showcontent = cb[0].showContent;
                }
                res.send(obj);
            });
        }
    });
};

/**
 * Update Stream Config
 */
exports.streamConfigUpdate = function (req, res) {
    JiraProject.update({userName: req.user.userName}, req.body, {upsert: true}, function (err, cb) {
        res.send('ok');
    });
};

/**
 * Update Task JQL
 */
exports.updateTask = function (req, res) {
    system.privateConf.jira.searchJira(req.body.jql, {}, function (error, issue) {
        if (error) {
            res.status(400).send(error);
        } else {
            JiraTask.update({userName: req.user.userName}, req.body, {upsert: true}, function (err, cb) {
                res.send('ok');
            });
        }
    });
};

/**
 * Get Jira blocks
 */
exports.blocks = function (req, res) {
    JiraBox.find({
        userName: req.user.userName
    }).exec(function (err, obj) {
        if (obj.length > 0) {
            var i = 0;
            for (var index in obj) {
                /* jshint -W083 */
                (function (index) { // async function inside a loop -> I needed a closure here so index is passed to the function itself
                    system.privateConf.jira.searchJira(obj[index].jql, {}, function (error, issue) {
                        obj[index].jql = system.privateConf.jiraJQLURL + encodeURI(obj[index].jql);
                        if (error) {
                            obj[index].count = 0;
                            i++;
                        } else {
                            obj[index].count = issue.total;
                            i++;
                        }
                        if (i === obj.length) {
                            res.send(obj);
                        }
                    });
                }(index));
            }
        } else {
            res.send(obj);
        }
    });
};

/**
 * Get Jira tasks
 */
exports.tasks = function (req, res) {
    JiraTask.find({userName: req.user.userName}).exec(function (err, obj) {
        if (obj.length > 0) {
            system.privateConf.jira.searchJira(obj[0].jql, {fields: ['summary', 'priority', 'issuetype']}, function (error, issue) {
                if (error) {
                    res.status(400).send(error);
                } else {
                    issue.jql = system.privateConf.jiraJQLURL + encodeURI(obj[0].jql);
                    res.send(issue);
                }
            });
        }
    });
};

/**
 * Get QA TODOs
 */
exports.todos = function (req, res) {
    system.privateConf.jira.searchJira(system.conf().jiraTodoJQL, {
        startAt: req.query.start,
        maxResults: req.query.max,
        fields: ['summary', 'priority', 'issuetype', 'project']
    }, function (error, issue) {
        if (error) {
            res.status(400).send(error);
        } else {
            issue.jql = system.privateConf.jiraJQLURL + encodeURI(system.conf().jiraTodoJQL);
            res.send(issue);
        }
    });
};

/**
 * Get Box jqls Config
 */
exports.blockJqls = function (req, res) {
    JiraBox.find({userName: req.user.userName}).exec(function (err, obj) {
        res.send(obj);
    });
};

/**
 * Get Task jql Config
 */
exports.taskJql = function (req, res) {
    JiraTask.find({userName: req.user.userName}).exec(function (err, obj) {
        res.send(obj[0]);
    });
};

/**
 * Get used to refresh the activity stream
 */
exports.streamRefresh = function (io) {
    return function (req, res) {
        //TODO set a refresh time limit (e.g. only after 5 minutes after last call).
        io.emit('stream:refresh');
        res.send('ok');
    };
};

/**
 * Return jira status for given jira keys
 */
exports.getJiraStatus = function (req, res) {

    var commits = req.body.commits,
        keys = 0,
        count = 0,
        failed = false;

    commits.forEach(function (commit, index) {
        if (commit.jiraKey) {
            keys++;
            system.privateConf.jira.findIssue(commit.jiraKey + '?fields=status', function (error, issue) {
                if (error) {
                    if (error === 'Invalid issue number.')  {
                        console.error('\x1b[31m', error);
                    } else {
                        failed = true;
                        console.error('\x1b[31m', error);
                        res.status(400).send(error);
                    }
                } else {
                    commits[index].jiraStatus = issue.fields.status.name;
                    commits[index].jiraStatusColor = issue.fields.status.statusCategory.colorName;
                }

                count++;

                if (count === keys && !failed) (
                    res.send(commits)
                );
            });
        }
    });

};
