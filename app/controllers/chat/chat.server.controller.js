'use strict';

/**
 * Module dependencies.
 */
var Chat = require('mongoose').model('Chat'),
    User = require('mongoose').model('User'),
    _ = require('lodash');

/**
 * Get last messages
 */
exports.messages = function (req, res) {
    Chat.find({when: {'$gte': req.user.chat.chatFrom}}).sort({when: 'desc'}).limit(req.user.chat.chatLimit).exec(function (err, obj) {
        if (err) {
            console.error('\x1b[31m', err);
            res.status(400).send(err);
        } else {
            res.send(obj);
        }
    });
};