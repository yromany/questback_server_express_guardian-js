'use strict';

/**
 * Module dependencies.
 */
var Chat = require('mongoose').model('Chat'),
    User = require('mongoose').model('User'),
    _ = require('lodash'),
    Utils = require('../utils');

// Chat room controller
var userNames = (function (socket) {
    var users = [];

    // register user on chat
    var registerUser = function (user) {
        if (!users[user.userName]) {
            users[user.userName] = true;
            users.push(user);
        }
    };

    // return chat users
    var getUsers = function () {
        return users;
    };

    // release user from online list
    var releaseUser = function (user, callback) {
        if (users[user.userName]) {
            delete users[user.userName];
            for (var i = 0; i < users.length; i++)
                if (users[i].userName === user.userName) {
                    users.splice(i, 1);
                    break;
                }
        }
        return callback(users);
    };

    // toggle user online status
    var toggleStatus = function (user, callback) {
        for (var i = 0; i < users.length; i++)
            if (users[i].userName === user.userName) {
                var status = users[i].status;
                if (status === 'online') {
                    status = 'offline';
                } else {
                    status = 'online';
                }
                users[i].status = status;
                /*jshint -W083 */
                User.update({userName: user.userName}, {status: status}, {upsert: true}, function (err, obj) {
                    if (err) {
                        console.error('\x1b[31m', err);
                        return;
                    }
                });
                break;
            }
        return callback(users);
    };

    // update user avatar
    var avatarUpdate = function (user, callback) {
        for (var i = 0; i < users.length; i++) {
            if (users[i].userName === user.userName) {
                users[i].avatar = user.userName + '.png';
                break;
            }
        }
        return callback(users);
    };

    return {
        registerUser: registerUser,
        getUsers: getUsers,
        releaseUser: releaseUser,
        toggleStatus: toggleStatus,
        avatarUpdate: avatarUpdate
    };

}());

// export function for listening to the socket when the users connects
module.exports = function (socket) {

    var session = socket.request.session,
        email = session.passport.user;

    User.findOne({email: email}, {
        displayName: 1,
        status: 1,
        userName: 1,
        name: 1,
        avatar: 1,
        isAdmin: 1,
        isQA: 1,
        _id: 0
    }, function (err, obj) {

        if (err || obj === null) {
            socket.emit('user:err', {});
            return;
        }

        var user = obj;

        userNames.registerUser(user);

        // send the new user their name and a list of users
        socket.emit('user:init', {
            user: user,
            users: userNames.getUsers()
        });

        // notify other clients that a new user has joined
        socket.broadcast.emit('user:refresh', {
            users: userNames.getUsers()
        });

        // chat user has toggled his status
        socket.on('user:togglestatus', function (data) {
            userNames.toggleStatus(user, function (users) {
                socket.emit('user:update', {
                    user: user,
                    users: users
                });
                socket.broadcast.emit('user:refresh', {
                    users: users
                });
            });
        });

        // chat message was sent
        socket.on('send:message', function (data) {

            var newMessage = new Chat();
            var when = Date.now();

            newMessage.user = user;
            newMessage.message = data.message;
            newMessage.when = when;

            // save message and broadcast it
            newMessage.save(function (merr, cb) {
                if (merr) {
                    console.error('\x1b[31m', merr);
                    return;
                }
                socket.emit('got:message', {
                    user: user,
                    message: data.message,
                    when: when
                });
                socket.broadcast.emit('got:message', {
                    user: user,
                    message: data.message,
                    when: when
                });
            });
        });

        // chat user has updated his avatar
        socket.on('user:avatarUpdate', function (data) {
            userNames.avatarUpdate(user, function (users) {
                socket.emit('user:update', {
                    user: user,
                    users: users
                });
                socket.broadcast.emit('user:refresh', {
                    users: users
                });
                socket.emit('user:avatarUpdate', {});
                socket.broadcast.emit('user:avatarUpdate', {});
            });
        });

        // clean up when a user leaves, and broadcast it to other users
        socket.on('disconnect', function () {
            userNames.releaseUser(user, function (users) {
                socket.broadcast.emit('user:left', {
                    users: users
                });
            });
        });
    });
};
