'use strict';

/**
 * Module dependencies.
 */
var Build = require('mongoose').model('Build'),
    FailedJob = require('mongoose').model('FailedJob'),
    _ = require('lodash'),
    system = require('../../config/config'),
    rest = require('restler'),
    S = require('string');

/**
 * Create a Build
 */
exports.create = function(req, res) {
  var build = new Build();
  build.userName = req.user.userName;
  build.title = req.body.status.title;
  build.stage = req.body.status.stage;
  build.version = req.body.status.version;
  build.position = 99;
  build.save(function(err,cb) {
    if (err) {
      console.error('\x1b[31m', err);
      res.status(400).send(err);
    } else {
      res.send(cb);
    } 
  });
};

/**
 * Update build positions
 */
exports.update = function(req, res) {
  for (var index in req.body.builds) {
    /*jshint -W083 */
    Build.update({_id: req.body.builds[index]._id}, {position: req.body.builds[index].position}, function(err, cb) {
      if (err) {
        console.error('\x1b[31m', err);
      } 
    });
  }
  res.send('ok');
};

/**
 * Delete an Build
 */
exports.delete = function(req, res) {
  Build.remove({_id: req.params.id}, function(err,cb) {
    if (err) {
      console.error('\x1b[31m', err);
      res.status(400).send(err);
    } else {
      res.send('ok');
    } 
  });
};

/**
 * List of Builds
 */
exports.list = function(req, res) {
  Build.find({userName: req.user.userName}).sort({position: 'asc'}).exec(function(err,obj) {
    if (err) {
      console.error('\x1b[31m', err);
      res.status(400).send(err);
    } else {
      res.send(obj);
    } 
  });
};

/**
 * List of pipeline stages
 */
exports.stages = function(req, res) {
  //TODO convert it into DB
  var stages = {'versions': [{'version':'DEV'},{'version':'10.4'},{'version':'DV-2.1'}], 'stages': [{'title':'Commit Stage'},{'title':'Build Stage'},{'title':'Acceptance Stage'}]};
  res.send(stages);
};

/**
 * Get job test report
 */
exports.testReport = function(req, res) {
  rest.get('http://' + system.conf().jenkinsURL + '/job/' + req.query.job + '/lastBuild/testReport/api/json?tree=duration,failCount,passCount').on('complete', function(data) {
    // handle not existing test reports
    if (S(data).contains('>Not found<')) {
      rest.get('http://' + system.conf().jenkinsURL + '/job/' + req.query.job + '/lastSuccessfulBuild/testReport/api/json?tree=duration,failCount,passCount').on('complete', function(data) {
        res.send(data);
      });
    } else {
      res.send(data);
    }
  });
};


/**
 * Send message that job failed to all users
 */
exports.failed = function (io) {
    return function (req, res) {

        var job = req.body.job,
        url = req.body.url;

        FailedJob.update({job: job, url: url},
            {
                job: job,
                url: url
            },
            {upsert: true},
            function (err, cb) {
                if (err) {
                    console.error('\x1b[31m', err);
                    res.status(400).send({result: false, message: err});
                } else {
                    io.emit('jenkins:failed', {
                        job: job,
                        url: url
                    });
                    res.send({result: true, message: 'success'});
                }
            }
        );
    };
};

/**
 * Send message that job has been fixed to all users
 */
exports.fixed = function (io) {
    return function (req, res) {

        var job = req.body.job,
            url = req.body.url;

        FailedJob.remove({job: job},
            function (err, cb) {
                if (err) {
                    console.error('\x1b[31m', err);
                    res.status(400).send({result: false, message: err});
                } else {
                    io.emit('jenkins:fixed', {
                        job: job,
                        url: url
                    });
                    res.send({result: true, message: 'success'});
                }
            }
        );

    };
};

/**
 * Get list of failed jobs
 */
exports.failedJobs = function (req, res) {
    FailedJob.find({}).exec(function (err, obj) {
        if (err) {
            console.error('\x1b[31m', err);
            res.status(400).send(err);
        } else {
            res.send(obj);
        }
    });
};
