'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

/**
 * Create a Feature
 */
exports.create = function (req, res) {

};

/**
 * Show the current Feature
 */
exports.read = function (req, res) {
    function readFeature(path) {
        var fs = require('fs'),
            feature = fs.readFileSync(path, 'utf8');
        return feature;
    }

    res.send(readFeature(req.body.path).toString('utf8'));
};

/**
 * Update a Feature
 */
exports.update = function (req, res) {

};

/**
 * Delete an Feature
 */
exports.delete = function (req, res) {

};

/**
 * List of Features
 */
exports.list = function (req, res) {

    function dirTree(filename) {
        var fs = require('fs'),
            path = require('path');
        var stats = fs.lstatSync(filename), info = {
            label: path.basename(filename)
        };
        if (stats.isDirectory() || stats.isSymbolicLink()) {
            info.data = {
                path: filename,
                type: 'folder'
            };
            info.children = fs.readdirSync(filename).map(function (child) {
                return dirTree(filename + '/' + child);
            });
        } else {
            info.data = {
                path: filename,
                type: 'feature'
            };
        }
        return info;
    }

    res.json(dirTree('features'));

};