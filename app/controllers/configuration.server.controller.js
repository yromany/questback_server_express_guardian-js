'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    _ = require('lodash'),
    system = require('../../config/config'),
    Configuration = mongoose.model('Configuration'),
    Coverage = mongoose.model('Coverage'),
    User = mongoose.model('User'),
    child_process = require('child_process'),
    fs = require('fs'),
    Baby = require('babyparse');

/**
 *
 * Show the current coverage
 */
exports.readCoverage = function (req, res) {
    Coverage.findOne({}, function(err, cb) {
        res.send(cb);
    });
};

/**
 * Update a Configuration
 */
exports.update = function (io) {
    return function (req, res) {
        // update config value
        Configuration.update({_id: req.body.id}, req.body.conf, function (err, cb) {
            if (err) {
                console.error('\x1b[31m',err);
                res.send(err);
            }
            else {
                // return new config
                Configuration.findOne({}, function (err, cb) {
                    var newConfig = cb;
                    system.setConf(newConfig);

                    io.emit('config:refresh', {
                        conf: newConfig
                    });
                    res.send('ok');
                });
            }
        });
    };
};

/**
 * List of Configurations
 */
exports.list = function (req, res) {
    res.send(system.conf());
};

/**
 * Update webdriver feature set from repository
 */
exports.updateFeature = function (req, res) {

    var sp, //spawn runner
        relevant_directories = ['panelwebsite', 'www', 'portal'], // directories to be sync
        spawn = child_process.spawn,
        isWindows = (process.platform === 'win32');

    // windows can not create symbolic links so just skip the update
    if (isWindows) {
        res.status(403).send('Sorry! You are not able to do that on windows');
    } else {
        // test if folder webdriver is already checked out
        if (fs.existsSync('webdriver')) {
            sp = spawn('hg', ['pull', '-u', '-R', 'webdriver']); // only pull new changes
        } else {
            sp = spawn('hg', ['clone', 'ssh://mercurial@mercurial.3uu.de/repository/webdriver/efs-dev/', 'webdriver']); //checkout
        }

        sp.on('exit', function (code) {
            //create features folder if not exists
            fs.mkdir('features', function (cb) {
                //delete symlinks if they exist and create new ones
                for (var index in relevant_directories) {
                    if (fs.existsSync('features/' + relevant_directories[index])) {
                        fs.unlinkSync('features/' + relevant_directories[index]);
                    }
                    fs.symlinkSync('../webdriver/features/' + relevant_directories[index], 'features/' + relevant_directories[index], 'dir');
                }
                res.send('ok');
            });
        });
    }
};

exports.updateCoverage = function (req, res) {

    // parse coverage csv
    var parsed = Baby.parse(req.body.coverage, {
        delimiter: ';',
        header: false,
        dynamicTyping: false,
        skipEmptyLines: false,
        preview: 0,
        step: undefined,
        encoding: 'default',
        worker: false,
        comments: 'default',
        download: false,
        complete: function (results) {
            results.data[1][1] = null;
            results.data[1][2] = null;
            results.data[1][3] = null;
            results.data[1][4] = null;

            for(var i = 2; i < results.data.length; i++)
            {
                results.data[i][2] = parseInt(results.data[i][2]);
                results.data[i][3] = parseInt(results.data[i][3]);
                results.data[i][4] = parseInt(results.data[i][4]);
            }

        }
    });

    // update coverage collection
    Coverage.update({}, {coverage: parsed.data},{upsert: true}, function(err, cb) {
        res.send('ok');
    });

};

exports.statusFeature = function (req, res) {
    var sp, //spawn runner
        stdout = '',
        spawn = child_process.spawn;

    // test if folder webdriver is already checked out
    if (fs.existsSync('webdriver')) {
        sp = spawn('hg', ['in', '-R', 'webdriver']); // check for incoming changes
    } else {
        res.send('Please check out the webdriver repository');
        return;
    }

    sp.stdout.on('data', function (data) {
        stdout = stdout + data;
    });

    sp.on('exit', function (code) {
        res.send(stdout);
    });
};
