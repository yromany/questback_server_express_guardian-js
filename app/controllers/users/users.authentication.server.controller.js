'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	errorHandler = require('../errors'),
	mongoose = require('mongoose'),
	passport = require('passport'),
	User = mongoose.model('User');

/**
 * Signin after passport authentication
 */
exports.signin = function(req, res, next) {
	passport.authenticate('ldapauth', function(err, user, info) {
	  if (err || !user) {
	    if (err) {
	      console.error('\x1b[31m', err.message);
	      res.status(400).send('Error on ldapauth. Are you on our network?');
	    }
	    else {
	      console.error('\x1b[31m', 'Bad error on login: ' + user);
	      if (info) res.status(400).send(info.message);
	      else res.status(400).send('WTF? Something very bad happens...please try again later');
	    }
		} else {
			// Remove sensitive data before login
			user.password = undefined;
			req.login(user, function(err) {
				if (err) {
					res.status(400).send(err);
				} else {
					res.jsonp(user);
				}
			});
		}
	})(req, res, next);
};

/**
 * Signout
 */
exports.signout = function(req, res) {
	req.logout();
	res.redirect('/login');
};

