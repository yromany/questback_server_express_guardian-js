'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    errorHandler = require('../errors'),
    utils = require('../utils'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    moment = require('moment'),
    User = mongoose.model('User');


exports.firstLogin = function (req, res) {
    var fs = require('fs'),
        S = require('string'),
        user = req.user,
        avatar = user.userName + '.png',
        img = req.body.img.replace('data:image/png;base64,', ''),
        newAvatar = __dirname + '/../../../public/' + img;

    // avatar image is base64 encoded and needs to be saved
    if (!S(img).startsWith('modules')) {
        newAvatar = new Buffer(img, 'base64');
        fs.writeFile('public/modules/users/img/avatars/' + avatar, newAvatar, function (averr) {
            if (averr) {
                console.error('\x1b[31m', averr);
                res.status(400).send(averr);
            } else {
                /* jshint -W083 */
                User.update({userName: user.userName}, {
                    firstLogin: false,
                    avatar: avatar
                }, {upsert: true}, function (dberr, obj) {
                    if (dberr) {
                        console.error('\x1b[31m', dberr);
                        res.status(400).send(dberr);
                    } else {
                        res.send({message: 'ok'});
                    }
                });
            }

        });
    } else {
        // avatar image is a link to existing avatar and just needs to be copied
        utils.copyFile(newAvatar, __dirname + '/../../../public/modules/users/img/avatars/' + avatar, function (averr) {
            if (averr) {
                console.error('\x1b[31m', averr);
                res.status(400).send(averr);
            } else {
                /* jshint -W083 */
                User.update({userName: user.userName}, {
                    firstLogin: false,
                    avatar: avatar
                }, {upsert: true}, function (dberr, obj) {
                    if (dberr) {
                        console.error('\x1b[31m', dberr);
                        res.status(400).send(dberr);
                    } else {
                        res.send({message: 'ok'});
                    }
                });
            }
        });
    }
};

exports.chatLimit = function (req, res) {
    if (req.body.limit === parseInt(req.body.limit)) {
        User.update({userName: req.user.userName}, {'chat.chatLimit': req.body.limit}).exec(function (dberr, obj) {
            if (dberr) {
                console.error('\x1b[31m', dberr);
                res.status(400).send(dberr);
            } else {
                res.send({message: 'ok'});
            }
        });
    } else {
        res.send({message: 'not valid limit value'});
    }
};

exports.chatFrom = function (req, res) {
    if (moment(req.body.from).isValid()) {
        User.update({userName: req.user.userName}, {'chat.chatFrom': req.body.from}).exec(function (dberr, obj) {
            if (dberr) {
                console.error('\x1b[31m', dberr);
                res.status(400).send(dberr);
            } else {
                res.send({message: 'ok'});
            }
        });
    } else {
        res.send({message: 'not valid date value'});
    }
};

exports.widgets = function (req, res) {
    User.update({userName: req.user.userName}, {'widgets': req.body}).exec(function (dberr, obj) {
        if (dberr) {
            console.error('\x1b[31m', dberr);
            res.status(400).send(dberr);
        } else {
            res.send({message: 'ok'});
        }
    });
};

exports.widgetSections = function (req, res) {
    User.update({userName: req.user.userName}, {'widgetSections': req.body.sections}).exec(function (dberr, obj) {
        if (dberr) {
            console.error('\x1b[31m', dberr);
            res.status(400).send(dberr);
        } else {
            res.send({message: 'ok'});
        }
    });
};

exports.sidebar = function (req, res) {
    User.update({userName: req.user.userName}, {'sidebar': req.body.sidebar}).exec(function (dberr, obj) {
        if (dberr) {
            console.error('\x1b[31m', dberr);
            res.status(400).send(dberr);
        } else {
            res.send({message: 'ok'});
        }
    });
};

exports.favoriteInst = function (req, res) {
    User.update({userName: req.user.userName}, {'favoriteInst': req.body.favoriteInst}).exec(function (dberr, obj) {
        if (dberr) {
            console.error('\x1b[31m', dberr);
            res.status(400).send(dberr);
        } else {
            res.send({message: 'ok'});
        }
    });
};

exports.listUsers = function (req, res) {
    User.find({}).exec(function (dberr, obj) {
        if (dberr) {
            console.error('\x1b[31m', dberr);
            res.status(400).send(dberr);
        } else {
            res.send(obj);
        }
    });
};

/**
 * Update a User
 */
exports.update = function(req, res) {
    User.update({_id: req.params.id}, req.body, function(err, cb) {
        if (err) {
            console.error('\x1b[31m', err);
            res.status(400).send(err);
        } else {
            res.send(cb);
        }
    });
};

exports.readAnnouncement = function (req, res) {
    User.update({userName: req.user.userName}, {$set : {'announcementRead': true}}).exec(function (dberr, obj) {
        if (dberr) {
            console.error('\x1b[31m', dberr);
            res.status(400).send(dberr);
        } else {
            res.send({message: 'ok'});
        }
    });
};

/**
 * Delete an User
 */
exports.delete = function(req, res) {
    User.findOne({
        _id: req.params.id
    }).exec(function (err, user) {
        if (user === null) {
            res.status(400).send('User not found');
            return;
        }
        if (err) {
            console.error('\x1b[31m', err);
            res.status(400).send(err);
        } else {
            if (user.userName === req.user.userName) {
                res.status(403).send('Nice try, but you are not able to delete your own user :-/');
            } else {
                User.remove({_id: req.params.id}, function(err,cb) {
                    if (err) {
                        console.error('\x1b[31m', err);
                        res.status(400).send(err);
                    } else {
                        res.send('ok');
                    }
                });
            }
        }
    });
};
