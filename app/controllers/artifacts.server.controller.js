'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    sys = require('sys'),
    exec = require('child_process').exec,
    Artifact = require('mongoose').model('Artifact'),
    ArtifactArchive = require('mongoose').model('ArtifactArchive'),
    Promotion = require('mongoose').model('Promotion'),
    Changeset = require('mongoose').model('Changeset'),
    Changelog = require('mongoose').model('Changelog'),
    Healselfhash = require('mongoose').model('Healselfhash'),
    _ = require('lodash'),
    system = require('../../config/config'),
    rest = require('restler'),
    S = require('string'),
    Request = require('request');

/**
 * Help function update/create Healselfhash for product/version
 */
function updateHealselfHash(product,version,healselfhash) {
    // create Healselfhash entry for the artifact
    Healselfhash.update({product: product, version: version},
        {
            $set: {hash: healselfhash, date: new Date()}
        },
        {upsert: true},
        function (err, cb) {
            if (err) {
                console.error('\x1b[31m', err.message);
            }
        });
}

/**
 * Create a Artifact
 */
exports.register = function (io) {
    return function (req, res) {
        try {
            var job = req.body.job,
                branch = req.body.branch || 'default',
                product = req.body.product || 'EFS',
                version = req.body.version,
                build = req.body.build,
                name = req.body.name,
                status = req.body.status || 'N/A',
                healselfhash = req.body.hash,
                healselfchanged = false,
                url = req.body.url,
                artifactory = '',
                date = new Date(),
                artifactoryBuildPattern = '(http:\/\/artifactory.*[0-9].zip)',
                jiraKeyPattern = '([a-zA-Z][a-zA-Z0-9_]+-[1-9][0-9]*)([^.]|\\.[^0-9]|\\.$|$)',
                jiraKeyPatternRegExp = new RegExp(jiraKeyPattern),
                jiraAuthorPatternRegExp = new RegExp(' <.+@.+..+>');

            // get artifactory URL
            rest.get(url + 'consoleText',
                {
                    username: system.conf().jenkins2user,
                    password: system.privateConf.jenkins2password
                }
            ).on('complete', function (data) {
                var artifact = data.match(artifactoryBuildPattern);
                if (artifact) {
                    artifactory = artifact[1];
                }

                // get changesets from build
                rest.get(url + 'wfapi/changesets',
                    {
                        username: 'admin',//system.conf().jenkins2user,
                        password: 'QA@Jenkins!'// system.privateConf.jenkins2password
                    }
                ).on('complete', function (data) {
                    var changesets = data;
                    if (changesets.length > 0) {

                        // iterate over the changesets
                        changesets.forEach(function (changeset, changesetIndex) {
                            // iterate over the commits to find the jira key on the comments
                            // if found add jira key and jira jira url into the commit object
                            changesets[changesetIndex].commits.forEach(function (commit, commitIndex) {

                                changesets[changesetIndex].commits[commitIndex].messageJira = commit.message.replace(jiraKeyPatternRegExp, '<a target="_blank" href="https://' + system.conf().jiraURL + '/browse/$1"></a>');
                                changesets[changesetIndex].commits[commitIndex].authorName = commit.authorJenkinsId.replace(jiraAuthorPatternRegExp, '');

                                var key = commit.message.match(jiraKeyPattern);
                                if (key) {
                                    changesets[changesetIndex].commits[commitIndex].jiraKey = key[1];
                                    changesets[changesetIndex].commits[commitIndex].jiraUrl = 'https://' + system.conf().jiraURL + '/browse/' + key[1];
                                }

                            });
                        });
                    } else {
                        changesets.push({commitCount: 0, commits: []});
                    }

                    // get changesets from previously failed builds and merge into new artifact
                    Changeset.find({job: job}).exec(function (err, builds) {
                        if (err) {
                            console.error('\x1b[31m', err);
                            res.status(400).send({result: false, message: err});
                        } else {
                            builds.forEach(function (build, buildIndex) {
                                if (typeof builds[buildIndex].changesets[0] !== 'undefined') {
                                    Array.prototype.push.apply(changesets, builds[buildIndex].changesets);
                                }
                                // delete previous changesets after collecting them for the current build
                                Changeset.remove({_id: builds[buildIndex]._id}).exec();
                            });

                            // get healselfhash from product and version
                            Healselfhash.findOne({product: product, version: version}).exec(function (err, hashObj) {
                                if (hashObj !== null) {
                                    if (hashObj.hash !== healselfhash) {
                                        healselfchanged = true;
                                        // healselfhash has changed for product, update hash value
                                        updateHealselfHash(product,version,healselfhash);
                                    }
                                } else {
                                    // create 1st hash value
                                    updateHealselfHash(product,version,healselfhash);
                                }

                                Artifact.update({name: name},
                                    {
                                        $set: {
                                            job: job,
                                            branch: branch,
                                            product: product,
                                            version: version,
                                            build: build,
                                            url: url,
                                            date: date,
                                            status: status,
                                            changesets: changesets,
                                            artifactory: artifactory,
                                            healselfhash: healselfhash,
                                            healselfchanged: healselfchanged
                                        }
                                    },
                                    {upsert: true},
                                    function (err, cb) {
                                        if (err) {
                                            console.error('\x1b[31m', err);
                                            res.status(400).send({result: false, message: err});
                                        } else {
                                            io.emit('artifact:refresh', {
                                                name: name
                                            });
                                            res.send({result: true, message: 'artifact has been successfully created'});
                                        }
                                    }
                                );
                            });

                        }
                    });
                    // Adding comments to Jira tickets for Git commits in builds
                    // HGBOT QBBOT
                    changesets.forEach(function (changeset, changesetIndex) {
                        // iterate over the commits
                        if(changeset.kind === 'git') {// changeset.kind kennt die IDE nicht, ist aber im Objekt
                            changeset.commits.forEach(function (commit, commitIndex) {
                                if(commit.jiraKey){
                                    var commitdate = new Date(commit.timestamp); // commit.timestamp kennt die IDE ebenfalls nicht
                                    var comment = '{panel:title=New changeset (' + commit.commitId + ') in build ' +
                                        name + ', comitted by ' + commit.authorName + ' }\r\n' +
                                        commit.message + '\r\n' +
                                        '----\r\n' +
                                        'Changeset: *[' + commit.commitId + '|' + commit.commitUrl + ']* on ' + commitdate + '\r\n' + // auch commitUrl kennt die IDE nicht
                                        '{panel}';
                                    system.privateConf.jira.addComment(commit.jiraKey, comment, function (error, issue) {
                                        if (error) {
                                            console.error('\x1b[31m', error);
                                        }
                                    });
                                }
                            });
                        }
                    });
                });
            });
        }
        catch (error) {
            console.error('\x1b[31m', error);
            res.status(400).send({result: false, message: error});
        }
    };
};

/**
 * Approve an Artifact
 */
exports.approve = function (req, res) {
    Artifact.update({name: req.body.name},
        {
            approved: true,
            approvedBy: req.user.displayName
        }, function (err, cb) {
            if (err) {
                console.error('\x1b[31m', err);
                res.status(400).send(err);
            } else {
                ArtifactArchive.update({name: req.body.name},
                    {
                        approved: true,
                        approvedBy: req.user.displayName
                    }, function (err, cb) {
                        if (err) {
                            console.error('\x1b[31m', err);
                            res.status(400).send(err);
                        } else {
                            res.send(cb);
                        }
                    }
                );
            }
        }
    );
};

/**
 * Decline an Artifact
 */
exports.decline = function (req, res) {
    Artifact.update({name: req.body.name},
        {
            declined: true,
            declinedBy: req.user.displayName
        }, function (err, cb) {
            if (err) {
                console.error('\x1b[31m', err);
                res.status(400).send(err);
            } else {
                ArtifactArchive.update({name: req.body.name},
                    {
                        declined: true,
                        declinedBy: req.user.displayName
                    }, function (err, cb) {
                        if (err) {
                            console.error('\x1b[31m', err);
                            res.status(400).send(err);
                        } else {
                            res.send(cb);
                        }
                    }
                );
            }
        }
    );
};

/**
 * Revoke declination of an Artifact
 */
exports.revoke = function (req, res) {
    Artifact.update({name: req.body.name},
        {
            declined: false,
            declinedBy: ''
        }, function (err, cb) {
            if (err) {
                console.error('\x1b[31m', err);
                res.status(400).send(err);
            } else {
                ArtifactArchive.update({name: req.body.name},
                    {
                        declined: false,
                        declinedBy: ''
                    }, function (err, cb) {
                        if (err) {
                            console.error('\x1b[31m', err);
                            res.status(400).send(err);
                        } else {
                            res.send(cb);
                        }
                    }
                );
            }
        }
    );
};

/**
 * Comment an Artifact
 */
exports.comment = function (req, res) {
    Artifact.update({name: req.body.name},
        {
            comment: req.body.comment
        }, function (err, cb) {
            if (err) {
                console.error('\x1b[31m', err);
                res.status(400).send(err);
            } else {
                ArtifactArchive.update({name: req.body.name},
                    {
                        comment: req.body.comment
                    }, function (err, cb) {
                        if (err) {
                            console.error('\x1b[31m', err);
                            res.status(400).send(err);
                        } else {
                            res.send(cb);
                        }
                    }
                );
            }
        }
    );
};

/**
 * Return a list from Artifacts
 */
exports.artifacts = function (req, res) {
    var versions = req.query.version.split(','),
        start = parseInt(req.query.start),
        max = parseInt(req.query.max),
        result = {total: 0, artifacts: []},
        s = start,
        m = max;

    Artifact.find({version: {$in: versions}}).count().exec(function (err, artifacts_count) {

        // if all artifact are required then ignore pagination
        if (req.query.all === 'true') {
            s = 0;
            m = 0;
        }
        Artifact.find({version: {$in: versions}}).skip(s).limit(m).sort({date: 'desc'}).exec(function (err, artifacts) {

            if (err) {
                console.error('\x1b[31m', err);
                res.status(400).send(err);
            } else {
                if (req.query.all === 'true') {
                    ArtifactArchive.find({version: {$in: versions}}).count().exec(function (err, archive_count) {
                        ArtifactArchive.find({version: {$in: versions}}).sort({date: 'desc'}).exec(function (err, archive) {
                            if (err) {
                                console.error('\x1b[31m', err);
                                res.status(400).send(err);
                            } else {
                                result.total = artifacts_count + archive_count;
                                Array.prototype.push.apply(artifacts,archive);
                                max = start + max;
                                if (max > 0) {
                                    result.artifacts = artifacts.slice(start, max);
                                } else {
                                    result.artifacts = artifacts;
                                }
                                res.send(result);
                            }
                        });
                    });
                } else {
                    result.total = artifacts_count;
                    result.artifacts = artifacts;
                    res.send(result);
                }
            }
        });
    });

};

/**
 * Return a list of distincted artifact versions
 */
exports.versions = function (req, res) {
    var product = req.query.product;

    Artifact.distinct('version', {product: product}).exec(function (err, versions) {
        if (err) {
            console.error('\x1b[31m', err);
            res.status(400).send({result: false, message: err});
        } else {
            res.send(versions);
        }
    });

};

/**
 * Promote EFS artifacts (ROM)
 */
exports.promote = function (io) {
    return function (req, res) {

        var product = req.body.product,
            version = req.body.version,
            artifact = req.body.name,
            artifactory = req.body.artifactory,
            promotionHost = 'http://old-servicegw.tka.globalpark.com:84'; //TODO needs to be a parameter

        var data = {
            'id': 1,
            'service': 'deploy',
            'method': 'promoteBuildToProduction',
            'params': [artifact, artifactory, product]
        };

        var promoteBuild = {
            data: JSON.stringify(data),
            headers: {'Content-Type': 'application/json'}
        };

        // promote artifact to (ROM)
        rest.post(promotionHost, promoteBuild).on('complete', function (data) {

            if (typeof data.error !== 'undefined') {
                console.error('\x1b[31m', data.error);
                res.status(400).send({result: false, message: data.error});
            } else {

                // find last promotion from product and version
                Promotion.findOne({product: product, version: version}).exec(function (err, promotionObj) {

                    var lastPromotion = '';

                    if (promotionObj !== null) {
                        lastPromotion = promotionObj.name;
                    }

                    // find all commits since last promotion
                    Artifact.find({
                        name: {$lte: artifact, $gt: lastPromotion},
                        product: product,
                        version: version
                    }).exec(function (err, artifacts) {
                        var changelog = [];

                        artifacts.forEach(function (artifact) {
                            artifact.changesets.forEach(function (changeset) {
                                changeset.commits.forEach(function (commit) {
                                    changelog.push(commit);
                                });
                            });
                        });

                        var artifactsToBlock = artifacts.map(function (artifact) {
                            return artifact._id;
                        });

                        // create changelog for artifact
                        Changelog.update({name: artifact},
                            {
                                product: product,
                                version: version,
                                name: artifact,
                                date: new Date(),
                                changelog: changelog
                            },
                            {upsert: true},
                            function (err, cb) {
                                if (err) {
                                    console.error('\x1b[31m', err);
                                    res.status(400).send({result: false, message: err});
                                } else {

                                    // create promotion entry for the artifact
                                    Promotion.update({product: product, version: version},
                                        {
                                            $set: {name: artifact, date: new Date()}
                                        },
                                        {upsert: true},
                                        function (err, cb) {
                                            if (err) {
                                                console.error('\x1b[31m', err);
                                                res.status(400).send({result: false, message: err});
                                            } else {

                                                // set artifact as promoted
                                                Artifact.update({name: artifact},
                                                    {
                                                        promoted: true,
                                                        promotedBy: req.user.displayName
                                                    }, function (err, cb) {
                                                        if (err) {
                                                            console.error('\x1b[31m', err);
                                                            res.status(400).send({result: false, message: err});
                                                        } else {

                                                            // block all artifacts between last promotion and promoted artifact
                                                            Artifact.update({_id: {$in: artifactsToBlock}}, {$set: {canPromote: false}}, {multi: true}).exec(function (err, obj) {
                                                                if (err) {
                                                                    console.error('\x1b[31m', err);
                                                                    res.status(400).send({result: false, message: err});
                                                                }
                                                                else {
                                                                    // interact over the commits and add a comment to the jira tickets
                                                                    changelog.forEach(function (commit) {
                                                                        if (typeof commit.jiraKey !== 'undefined') {
                                                                            system.privateConf.jira.addComment(commit.jiraKey, 'This fix has been promoted with Build ' + artifact + ' and it is ready for rollout. \n\n The QA Team.', function (error, issue) {});
                                                                        }
                                                                    });
                                                                    io.emit('artifact:promoted', {
                                                                        name: artifact
                                                                    });
                                                                    res.send({
                                                                        result: true,
                                                                        message: 'promotion of artifact completed'
                                                                    });
                                                                }
                                                            });

                                                        }
                                                    }
                                                );
                                            }
                                        }
                                    );
                                }
                            }
                        );
                    });
                });
            }
        }).on('fail', function (error) {
            console.error('\x1b[31m', error);
            res.status(400).send({result: false, message: error});
        });
    };
};

/**
 * Archive old artifacts
 */

function removeArchivedArtifacts(artifactsToArchive, jobCount, jobLength, res) {
    artifactsToArchive = artifactsToArchive.map(function (artifact) {
        return artifact._id;
    });
    Artifact.remove({_id: {$in: artifactsToArchive}}).exec(function (err, obj) {
        if (err) console.error('\x1b[31m', err);
        else {
            if (jobCount === jobLength) {
                res.send({result: true, message: 'archiving of artifacts completed'});
            }
        }
    });
}

exports.archive = function (req, res) {
    Artifact.distinct('job').exec(function (err, jobs) {
        var jobCount = 0;
        jobs.forEach(function (job) {
            var artifactCount = 0;
            Artifact.find({job: job}).skip(50).sort({date: -1}).exec(function (err, artifactsToArchive) {

                if (artifactsToArchive.length === 0) {
                    jobCount++;
                    removeArchivedArtifacts(artifactsToArchive, jobCount, jobs.length, res);
                }

                artifactsToArchive.forEach(function (artifactToArchive) {
                    ArtifactArchive.update({name: artifactToArchive.name}, artifactToArchive,
                        {upsert: true},
                        function (err, cb) {
                            artifactCount++;
                            if (artifactCount === artifactsToArchive.length) {
                                jobCount++;
                                removeArchivedArtifacts(artifactsToArchive, jobCount, jobs.length, res);
                            }
                        }
                    );
                });
            });
        });
    });
};

function setData(object) {
    return {
        type: 'junction',
        repoType: object.repoType,
        repoKey: object.repoKey,
        path: object.path,
        text: object.text,
        trashcan: false
    };
}

function setAuthentication(object) {
    return {
        username: system.conf().artifactoryUser,
        password: system.privateConf.artifactoryPassword,
        data: JSON.stringify(object),
        headers: {'Content-Type': 'application/json'}
    };
}

function emptyTrashCan(versionCount, versionsLength, repoCount, reposLength, res) {

    console.log('entering trash can');
    console.log('versionCount ' + versionCount);
    console.log('versionsLength ' + versionsLength);
    console.log('repoCount ' + repoCount);
    console.log('reposLength ' + reposLength);

    if (versionCount === versionsLength) {
        if (repoCount === reposLength) {

            console.log('emptying trash can');

            var data = {repoKey: 'auto-trashcan', path: ''};
            var authentication = setAuthentication(data);
            // empty trash can at the very end
            rest.post('http://' + system.conf().artifactoryURL + '/artifactory/ui/artifactactions/emptytrash',
                authentication
            ).on('complete', function (cb) {
                res.send({result: true, message: 'discarding of old artifacts completed'});
            });
        }
    }

}

/**
 * Discard old artifacts from repository manager
 */
exports.discard = function (req, res) {
    var data = {type: 'root'},
        authentication = setAuthentication(data);

    // get repositories
    rest.post('http://' + system.conf().artifactoryURL + '/artifactory/ui/treebrowser?compacted=true',
        authentication
    ).on('complete', function (repos) {
        var repoCount = 0;

        console.log('found ' + repos.length + ' repos');

        repos.forEach(function (repo) {
            data = setData(repo);
            authentication = setAuthentication(data);

            // skip trash repo type
            if (repo.repoType === 'trash') {
                console.log('ignoring trash can');
                repoCount++;
                return;
            }

            // get versions in repository
            rest.post('http://' + system.conf().artifactoryURL + '/artifactory/ui/treebrowser?compacted=true',
                authentication
            ).on('complete', function (versions) {

                console.log('found ' + versions.length + ' versions for repo ' + repo.repoKey);

                var versionCount = 0;

                if (versions.length === 0) {
                    versionCount++;
                    return;
                }

                versions.forEach(function (version) {
                    data = setData(version);
                    authentication = setAuthentication(data);

                    // get artifacts in version
                    rest.post('http://' + system.conf().artifactoryURL + '/artifactory/ui/treebrowser?compacted=true',
                        authentication
                    ).on('complete', function (artifacts) {

                        console.log('found ' + artifacts.length + ' artifacts for version ' + version.text + ' in repo ' + repo.repoKey);

                        var artifactCount = 0;
                        versionCount++;

                        artifacts = artifacts.reverse();
                        artifacts = artifacts.slice(system.conf().artifactoryRetention, artifacts.length);

                        if (artifacts.length === 0) {
                            console.log('no artifacts will be deleted');

                            if (versionCount === versions.length) {
                                repoCount++;
                            }

                            emptyTrashCan(versionCount, versions.length, repoCount, repos.length, res);
                            return;
                        }

                        console.log(artifacts.length + ' artifacts will be delete for version ' + version.text);

                        artifacts.forEach(function (artifact) {
                            data = {path: artifact.path, repoKey: artifact.repoKey};
                            authentication = setAuthentication(data);

                            // delete artifacts
                            rest.post('http://' + system.conf().artifactoryURL + '/artifactory/ui/artifactactions/delete',
                                authentication
                            ).on('complete', function (cb) {
                                artifactCount++;
                                if (artifactCount === artifacts.length) {

                                    if (versionCount === versions.length) {
                                        repoCount++;
                                    }

                                    emptyTrashCan(versionCount, versions.length, repoCount, repos.length, res);
                                }
                            });
                        });
                    });
                });
            });
        });
    });
};


exports.deleteVersions = function (req, res) {
    var versions = req.query.version.split(','),
        product = req.params.product || 'EFS';

    Artifact.remove({product: product, version: {$in: versions}}).exec(function (err, obj) {
        if (err) console.error('\x1b[31m', err);
        else {
            ArtifactArchive.remove({product: product, version: {$in: versions}}).exec(function (err, obj) {
                if (err) console.error('\x1b[31m', err);
                else {
                    res.send('ok');
                }
            });
        }
    });
};
