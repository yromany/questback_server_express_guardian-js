'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    FavInst = require('mongoose').model('FavoriteInstallations'),
    _ = require('lodash');

/**
 * Create a Favorite installation
 */
exports.create = function(req, res) {

};

/**
 * Show the current Favorite installation
 */
exports.read = function(req, res) {

};

/**
 * Update a Favorite installation
 */
exports.update = function(req, res) {
    FavInst.update({userName: req.user.userName, installation: req.body.installation},
        {
            userName: req.user.userName,
            installation: req.body.installation
        },
        {upsert: true},
        function (err, cb) {
            if (err) {
                console.error('\x1b[31m', err);
                res.status(400).send(err);
            } else {
                res.send(cb);
            }
        }
    );
};

/**
 * Delete an Favorite installation
 */
exports.delete = function(req, res) {
    FavInst.remove({userName: req.user.userName, installation: req.params.name}).exec(function (err, obj) {
        if (err) {
            console.error('\x1b[31m', err);
            res.status(400).send(err);
        } else {
           res.send(obj);
        }
    });
};

/**
 * List of Favorite installations
 */
exports.list = function(req, res) {
    var installations = [];
    FavInst.find({userName: req.user.userName}).exec(function (err, obj) {
        if (err) {
            console.error('\x1b[31m', err);
            res.status(400).send(err);
        } else {
            if (obj.length > 0) {
                obj.forEach(function (inst) {
                    installations.push(inst.installation);
                });
            }
            res.send(installations);
        }
    });
};