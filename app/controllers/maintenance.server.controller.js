'use strict';

/**
 * Module dependencies.
 */
var Maintenance = require('mongoose').model('Maintenance'),
    User =  require('mongoose').model('User'),
    moment = require('moment'),
    _ = require('lodash');

/**
 * Create a Maintenance Message
 */
exports.send = function (io) {
    return function(req, res) {
        Maintenance.update({name: 'maintenance'},
            {
                message: req.body.message,
                date: moment()
            },
            {upsert: true},
            function (err, cb) {
                if (err) {
                    console.error('\x1b[31m', err);
                    res.status(400).send(err);
                } else {
                    User.update({}, {$set : {'announcementRead': false}}, {multi: true}).exec(function (dberr, obj) {
                        if (dberr) {
                            console.error('\x1b[31m', dberr);
                            res.status(400).send(dberr);
                        } else {
                            io.emit('maintenance:new', {
                                    message: req.body.message
                                }
                            );
                            res.send(cb);
                        }
                    });
                }
            }
        );
    };
};

/**
 * Get maintanance message
 */
exports.read = function(req, res) {
    Maintenance.findOne({}).exec(function (err, obj) {
        if (err) {
            console.error('\x1b[31m', err);
            res.status(400).send(err);
        } else {
            res.send(obj);
        }
    });
};
