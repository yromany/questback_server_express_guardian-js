'use strict';

/**
 * Module dependencies.
 */

var _ = require('lodash'),
    system = require('../../config/config');

exports.index = function(req, res) {
	res.render('index', {
		account: req.user || null,
        conf: system.conf()
	});
};

exports.login = function(req, res) {
  if (req.isAuthenticated()) {
    res.render('index', {
      account: req.user || null,
        conf: system.conf()
    });
  } else {
    res.render('login', {
      account: req.user || null,
        conf: null
    });
  }
};
