'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    _ = require('lodash'),
    Coverage = mongoose.model('Coverage');

/**
 * Show the current Coverage
 */
exports.read = function (req, res) {
    // update coverage collection
    Coverage.findOne({}, function (err, cb) {
        res.send(cb);
    });
};
