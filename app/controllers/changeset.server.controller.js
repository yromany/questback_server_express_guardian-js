'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    _ = require('lodash'),
    Changeset = require('mongoose').model('Changeset'),
    system = require('../../config/config'),
    rest = require('restler');
/**
 * Create a Changeset
 */
exports.create = function (req, res) {
    try {
        var job = req.body.job,
            build = req.body.build,
            url = req.body.url,
            jiraKeyPattern = '([a-zA-Z][a-zA-Z0-9_]+-[1-9][0-9]*)([^.]|\\.[^0-9]|\\.$|$)',
            jiraKeyPatternRegExp = new RegExp(jiraKeyPattern),
            jiraAuthorPatternRegExp = new RegExp(' <.+@.+..+>');

        // get changesets from failed build
        rest.get(url + 'wfapi/changesets',
            {
                username: system.conf().jenkins2user,
                password: system.privateConf.jenkins2password
            }
        ).on('complete', function (data) {
            var changesets = data;

            if (changesets.length > 0) {

                // iterate over the changesets
                changesets.forEach(function (changeset, changesetIndex) {

                    // iterate over the commits to find the jira key on the comments
                    // if found add jira key and jira jira url into the commit object
                    changesets[changesetIndex].commits.forEach(function (commit, commitIndex) {

                        changesets[changesetIndex].commits[commitIndex].messageJira = commit.message.replace(jiraKeyPatternRegExp, '<a target="_blank" href="https://' + system.conf().jiraURL + '/browse/$1"></a>');
                        changesets[changesetIndex].commits[commitIndex].authorName = commit.authorJenkinsId.replace(jiraAuthorPatternRegExp, '');

                        var key = commit.message.match(jiraKeyPattern);
                        if (key) {
                            changesets[changesetIndex].commits[commitIndex].jiraKey = key[1];
                            changesets[changesetIndex].commits[commitIndex].jiraUrl = 'https://' + system.conf().jiraURL + '/browse/' + key[1];
                        }

                    });
                });
            } else {
                changesets.push({commitCount: 0, commits: []});
            }
            Changeset.update({job: job, build: build},
                {
                    job: job,
                    build: build,
                    changesets: changesets
                },
                {upsert: true},
                function (err, cb) {
                    if (err) {
                        console.error('\x1b[31m', err);
                        res.status(400).send({result: false, message: err});
                    } else {
                        res.send({result: true, message: 'changeset has been successfully created'});
                    }
                }
            );
        });
    }
    catch(error) {
        console.error('\x1b[31m', error);
        res.status(400).send({result: false, message: error});
    }
};