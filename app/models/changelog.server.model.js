'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    moment = require('moment');

/**
 * Changelog Schema
 */
var ChangelogSchema = new Schema({
    product: {
        type: String,
        required: true
    },
    version: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    changelog: {
        type: Object
    },
    date: {
        type: Date
    }
});

mongoose.model('Changelog', ChangelogSchema);