'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * DeployHosts Schema
 */
var DeployHostsSchema = new Schema({
	hostName: {
		type: String,
		required: true
	},
	hostURL: {
		type: String,
		required: true
	}
});

mongoose.model('DeployHosts', DeployHostsSchema);