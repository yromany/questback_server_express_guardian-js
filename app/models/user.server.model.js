'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    moment = require('moment'),
    Schema = mongoose.Schema;

/**
 * User Schema
 */
var UserSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: true,
        unique: true
    },
    userName: {
        type: String,
        trim: true,
        required: true,
        unique: true
    },
    displayName: {
        type: String,
        trim: true
    },
    avatar: {
        type: String,
        trim: true,
        default: 'default.png'
    },
    status: {
        type: String,
        trim: true,
        default: 'online'
    },
    email: {
        type: String,
        trim: true,
        required: true,
        unique: true
    },
    firstLogin: {
        type: Boolean,
        default: true
    },
    updated: {
        type: Date,
        default: moment()
    },
    created: {
        type: Date,
        default: moment()
    },
    chat: {
        chatLimit: {
            type: Number,
            default: 50
        },
        chatFrom: {
            type: Date,
            default: moment().subtract(6, 'days')
        }
    },
    widgetSections: {
        type: Number,
        default: 2
    },
    widgets: {
        type: Array,
        default: [
            [{
                view: 'chat@content'
            }, {
                view: 'todo@content'
            }],
            [{
                view: 'stream@content'
            }, {
                view: 'webdriver@content'
            }, {
                view: 'build@content'
            }],
            []
        ]
    },
    sidebar: {
        type: Boolean,
        default: true
    },
    favoriteInst: {
        type: Boolean,
        default: false
    },
    isQA: {
        type: Boolean,
        default: false
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    isLocked: {
        type: Boolean,
        default: false
    },
    announcementRead: {
        type: Boolean,
        default: true
    }
});

mongoose.model('User', UserSchema);
