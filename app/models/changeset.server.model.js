'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Changeset Schema
 */
var ChangesetSchema = new Schema({
	job: {
		type: String,
		required: false,
		trim: true
	},
	changesets: {
		type: Object
	}
});

mongoose.model('Changeset', ChangesetSchema);