'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Jira Blocks Schema
 */
var JiraBoxSchema = new Schema({
    userName: {
        type: String,
        trim: true,
        required: true
    },
    title: {
        type: String,
        trim: true,
        required: true
    },
    jql: {
        type: String,
        trim: true,
        required: true
    },
    type: {
        type: String,
        trim: true,
        required: true
    },
    count: {
        type: Number,
        default: 0
    }
});

/**
 * Jira Blocks Schema
 */
var JiraTaskSchema = new Schema({
    userName: {
        type: String,
        trim: true,
        required: true
    },
    jql: {
        type: String,
        trim: true,
        required: true
    },
    count: {
        type: Number,
        default: 0
    }
});

/**
 * Jira Projects Schema
 */
var JiraProjectSchema = new Schema({
    userName: {
        type: String,
        trim: true,
        required: true
    },
    projects: {
        type: Array,
        required: false
    },
    limit: {
        type: Number,
        default: 5,
        required: false
    },
    showContent: {
        type: Boolean,
        default: true
    }
});

mongoose.model('JiraBox', JiraBoxSchema);
mongoose.model('JiraTask', JiraTaskSchema);
mongoose.model('JiraProject', JiraProjectSchema);