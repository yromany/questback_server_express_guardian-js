'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Chat Schema
 */
var ChatSchema = new Schema({
  user: {
    type: Object,
    required: true,
  },
  message: {
    type: String,
    trim: true,
  },
  when: {
    type: Date,
    required: true
  }
});

mongoose.model('Chat', ChatSchema);