'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Healselfhash Schema
 */
var HealselfhashSchema = new Schema({
    product: {
        type: String,
        required: true
    },
    version: {
        type: String,
        required: true
    },
    hash: {
        type: String,
        required: true
    },
    date: {
        type: Date
    }
});

mongoose.model('Healselfhash', HealselfhashSchema);
