'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Jenkins Build Schema
 */
var BuildSchema = new Schema({
  userName: {
    type: String,
    trim: true,
    required: true
  },
  title: {
    type: String,
    trim: true,
    required: true,
    default: 'Commit Stage'
  },
  stage: {
    type: String,
    trim: true,
    required: true,
    default: 'CommitStage'
  },
  version: {
    type: String,
    trim: true,
    required: true,
    default: 'DEV'
  },
  position: {
    type: Number,
    required: true,
    default: 0
  }
});

/**
 * Jira Blocks Schema
 */
var FailedJobSchema = new Schema({
    job: {
        type: String,
        trim: true,
        unique: true
    },
    url: {
        type: String,
        trim: true,
        required: true
    }
});

mongoose.model('Build', BuildSchema);
mongoose.model('FailedJob', FailedJobSchema);
