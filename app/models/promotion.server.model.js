'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Promotions Schema
 */
var PromotionSchema = new Schema({
    product: {
        type: String,
        required: true
    },
    version: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    date: {
        type: Date
    }
});

mongoose.model('Promotion', PromotionSchema);