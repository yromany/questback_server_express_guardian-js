'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    moment = require('moment'),
    Schema = mongoose.Schema;

/**
 * Maintenance Schema
 */
var MaintenanceSchema = new Schema({
    name: {
        type: String,
        default: 'maintenance'
    },
    message: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: moment()
    }
});

mongoose.model('Maintenance', MaintenanceSchema);