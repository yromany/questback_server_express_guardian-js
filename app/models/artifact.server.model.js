'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Artifact Schema
 */
var ArtifactSchema = new Schema({
	job: {
		type: String,
		required: false,
        trim: true
	},
    branch: {
	    type: String,
        required: false,
        trim: true
    },
    product: {
        type: String,
        required: true,
        default: 'EFS'
    },
	version: {
		type: String,
        required: true,
	    trim: true
	},
    url: {
        type: String,
        required: false,
        trim: true
    },
    artifactory: {
        type: String,
        required: false,
        trim: true
    },
    healselfhash: {
        type: String,
        required: false,
        trim: true
    },
    healselfchanged: {
        type: Boolean,
        default: false
    },
	name: {
		type: String,
		required: true,
        trim: true,
        unique: true
	},
	date: {
        type: Date,
        required: true
    },
    status: {
        type: String,
        required: false,
        default: 'N/A'
    },
    changesets: {
        type: Object
    },
    approved: {
        type: Boolean,
        default: false
    },
    approvedBy: {
        type: String,
        required: false
    },
    promoted: {
        type: Boolean,
        default: false
    },
    promotedBy: {
        type: String,
        required: false
    },
    declined: {
        type: Boolean,
        default: false
    },
    declinedBy: {
        type: String,
        required: false
    },
    canPromote: {
        type: Boolean,
        default: true
    },
    comment: {
        type: String,
        required: false
    }
});

mongoose.model('Artifact', ArtifactSchema);
mongoose.model('ArtifactArchive', ArtifactSchema);
