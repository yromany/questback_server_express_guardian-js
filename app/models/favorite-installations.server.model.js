'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * FavoriteInstallations Schema
 */
var FavoriteInstallationsSchema = new Schema({
	userName: {
		type: String,
		required: true
	},
	installation: {
		type: String,
		required: true
	}
});

mongoose.model('FavoriteInstallations', FavoriteInstallationsSchema);