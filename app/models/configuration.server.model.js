'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Configuration Schema
 */
var ConfigurationSchema = new Schema({
    jenkinsURL: {
        type: String,
        trim: true,
        required: true,
        default: 'jenkins.globalpark.de'
    },
    jenkins2URL: {
        type: String,
        trim: true,
        required: true,
        default: 'jenkins.qb-feedback.com'
    },
    jenkins2user: {
        type: String,
        trim: true,
        required: true,
        default: 'admin'
    },
    jenkins2password: {
        type: String,
        trim: true,
        required: true,
        default: 'admin'
    },
    jiraURL: {
        type: String,
        trim: true,
        required: true,
        default: 'jira.3uu.de'
    },
    jiraGlobalUserName: {
        type: String,
        trim: true,
        required: true,
        default: 'testlink'
    },
    jiraGlobalUserPass: {
        type: String,
        trim: true,
        required: true,
        default: 'qa4dev!'
    },
    jiraProjectUserName: {
        type: String,
        trim: true,
        required: true,
        default: 'cervus'
    },
    jiraProjectUserPass: {
        type: String,
        trim: true,
        required: true,
        default: 'qa4dev!'
    },
    jiraTodoJQL: {
        type: String,
        trim: true,
        required: true,
        default: 'category = "QA Germany" AND status != Closed AND assignee = gp-qa'
    },
    scmConnectionString: {
        type: String,
        trim: true,
        required: true,
        default: 'ssh://mercurial@mercurial.3uu.de'
    },
    artifactoryURL: {
        type: String,
        trim: true,
        required: true,
        default: 'artifactory.qb-feedback.com'
    },
    artifactoryUser: {
        type: String,
        trim: true,
        required: true,
        default: 'admin'
    },
    artifactoryPassword: {
        type: String,
        trim: true,
        required: true,
        default: 'password'
    },
    artifactoryRetention: {
        type: Number,
        required: true,
        default: 100
    }
});

mongoose.model('Configuration', ConfigurationSchema);
