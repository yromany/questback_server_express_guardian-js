'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Coverage Schema
 */
var CoverageSchema = new Schema({
	coverage: {
		type: Array,
		required: true
	}
});

mongoose.model('Coverage', CoverageSchema);