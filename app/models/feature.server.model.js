'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Feature Schema
 */
var FeatureSchema = new Schema({
	// Feature model fields   
	// ...
});

mongoose.model('Feature', FeatureSchema);